---
title: About me
---

# About me

## What are you called?

I'm **+merlan \#flirora** \[ˈmeɹlan fliˈɹowa\], also known as **Uruwi**, **kozet**, or **ʻfelirovas-movanas**, and I program things as well as play games and create languages. Brief history about this site's name:

*   When I made an account at GitHub (where this site was originally hosted), I went by blue_bear_94 in calculator programming communities. Since GitHub didn't allow underscores in usernames, this got changed to bluebear94.
*   I chose to go by Kyarei here instead because this account was originally intended to upload stuff related to Wizard101 modding and I didn't want to reveal my identity. I no longer play Wizard101, let alone mod it, so I think it's safe to show who I really am now.

I also go by Fluffy8x (Reddit, originally my old Minecraft IGN).

## About this site

This site was first created to [rant about a certain Touhou character with exactly one wing](https://gitlab.com/Kyarei/kyarei.gitlab.io/commit/2fe5d0008bf5466480c53932119d3f425bddd2cd), and later expanded to [other](https://gitlab.com/Kyarei/kyarei.gitlab.io/commit/ec21e1e8eda19131b46fc5cf184bc784ed0c4c56) [Touhou](https://gitlab.com/Kyarei/kyarei.gitlab.io/commit/ced5c48c74955ded7f2fba3f4a826c6127337537) [stuff](https://gitlab.com/Kyarei/kyarei.gitlab.io/commit/8dccb8f3282bdb1d4bab5a3056a075da0cff6a96). It was hosted on GitHub Pages.

Two years later, I started editing the site again, adding content unrelated to Touhou. For instance, I added a page about [Wizard101](https://gitlab.com/Kyarei/kyarei.gitlab.io/commit/ed58dff77a99e61bbdb10d370dbf843e3eaaa7e6) and [documentation for an old conlang](https://gitlab.com/Kyarei/kyarei.gitlab.io/commit/bef2f309ff16e01a1811a62c648593ba68024348). Another page [added](https://gitlab.com/Kyarei/kyarei.gitlab.io/commit/7348edf009165c2d9cdf8f13921e39a41c5e1c35) early in the site's history was a page listing conlang orthography / romanisation fails.

Some time during that year, I became interested in [Arka](http://conlinguistics.org/arka/e_index.html). Since a [few](https://gitlab.com/Kyarei/kyarei.gitlab.io/commit/970e69aebc5eb823019af69fcd922bf103d5d0c6) [pages](https://gitlab.com/Kyarei/kyarei.gitlab.io/commit/6d6b6de997cc96b14c8bda53c8fd80ddcd8f456a) started using hacm as a result, a hacm toggler was added to the site in 2018\. Shortly after, the home page was revamped with a fancy animation.

On 2018/06/4, the site was moved to GitLab Pages. (The GitHub version is still available [here](https://bluebear94.github.io), though very outdated.) Meanwhile, I added more translations to this site, and more recently some stuff about SynthV as well.

On 2019/01/12, the site was migrated to be generated with nanoc, and the layout got a facelift. And then another facelift on 2021/01/13.

Most of the stuff on this site is dedicated to my hobbies outside programming, though there's a fair bit of programming involved in making this site. For programming stuff, check my GitLab profile. For politics salt (and conlanging stuff, I guess), check my Twitter account. For FGO videos (and conlanging stuff, I guess), check my YouTube channel.

Note that old pages that don't reflect the me of now, barring extenuating circumstances, stay here for posterity.

## Some more stuff!

Languages: Korean ("natively", yeah right), English (quasi-natively), Spanish (in middle and high school, but by the end of it, hated it so much that I stopped studying it after that), Japanese (basic), Welsh (basic), Irish (even basicker), [Arka](http://conlinguistics.org/arka/e_index.html) (high end of basic); not fluent in any of my own conlangs

Want to learn (if I have more time): Finnish, (moar) Japanese, maybe others

Pronouns: they > he >\> she > anything else >\> it

Holy wars: VSCode, Firefox, `void foo() {` and `} else {` on the same line, `int* p`, 2-space indents

Identifier preferences: `TypeNames`, `vars_and_functions`, `constants_too`, `namespace_names`, `PREPROCESSOR_MACROS` (formerly `varsAndFunctions`, `constantsToo`);  
for SQL, `TableNames`, `column_names`, `KEYWORDS`

Favorite phoneme: /ɬ/

Favorite Minecraft update: <s>the first update when I played Minecraft – 1.3</s> to be 1.17 if Microsoft doesn't take too much advantage of the upcoming account migration

big eng should look like N with hook

## Stuff used in this site

* The [kardinal](http://conlinguistics.org/arka/data_atom_1.html) font by Seren Arbazard
* ["Pecita Font"](https://www.fontspace.com/pecita-font-f10136) by, err... Pecita, licensed under the OFL
* [Inter](https://fonts.google.com/specimen/Inter) by Rasmus Andersson, also under the OFL
