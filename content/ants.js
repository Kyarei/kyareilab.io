var canvas;
var ctx;

function randIntBetween(a, b) {
  return Math.floor(a + (b - a) * Math.random());
}

function randFloatBetween(a, b) {
  return a + (b - a) * Math.random();
}

function getViewportDimensions() {
  let w =
      Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
  let h =
      Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
  return [w, h];
}

function getNextDelay() {
  return 25 + Math.floor(50 * Math.random());
}

function getRGBByHue(hue) {
  // Assumes saturation and value are 100%.
  // Hue is in [0, 360).
  let leg = Math.floor(hue / 60);
  let prog = (hue % 60) / 60;
  let tab = [1, 1 - prog, 0, 0, prog, 1];
  let r = tab[leg];
  let g = tab[(leg + 4) % 6];
  let b = tab[(leg + 2) % 6];
  return [
    Math.floor(255 * r),
    Math.floor(255 * g),
    Math.floor(255 * b),
  ];
}

function createAnts() {
  let ants = [];
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  let nAnts = Math.floor(canvas.width * canvas.height / 1500);
  ants = new Array(nAnts);
  let w = canvas.width;
  let h = canvas.height;
  for (let i = 0; i < nAnts; ++i) {
    let x = Math.floor(w * Math.random());
    let y = Math.floor(h * Math.random());
    let dir = Math.floor(4 * Math.random());
    let delay = getNextDelay();
    let hue = Math.floor(360 * Math.random());
    ants[i] = {x: x, y: y, dir: dir, delay: delay, hue: hue};
  }
  return ants;
}

function updateAnts(antFarm) {
  let w = canvas.width;
  let h = canvas.height;
  let vp = getViewportDimensions();
  if (w < vp[0] || h < vp[1]) {
    canvas.width = w = vp[0];
    canvas.height = h = vp[1];
    antFarm[0] = createAnts();
  }
  if (w == 0 || h == 0) return;
  let ants = antFarm[0];
  // Darken image by ~3% to avoid light being overcrowded
  ctx.globalCompositeOperation = 'destination-out';
  ctx.fillStyle = 'rgba(0, 0, 0, 0.03)';
  ctx.fillRect(0, 0, w, h);
  ctx.globalCompositeOperation = 'source-over';
  let nAnts = ants.length;
  for (let i = 0; i < nAnts; ++i) {
    let ant = ants[i];
    // Move
    switch (ant.dir) {
      case 0:
        ++ant.x;
        break;
      case 1:
        ++ant.y;
        break;
      case 2:
        --ant.x;
        break;
      case 3:
        --ant.y;
        break;
    }
    ant.x = (ant.x + w) % w;
    ant.y = (ant.y + h) % h;
    if (--ant.delay == 0) {
      ant.delay = getNextDelay();
      ant.dir = (ant.dir + 1 + 2 * Math.floor(2 * Math.random())) % 4;
    }
    let rgb = getRGBByHue(ant.hue++);
    ant.hue %= 360;
    ctx.fillStyle = 'rgb(' + rgb[0] + ',' + rgb[1] + ',' + rgb[2] + ')';
    ctx.fillRect(ant.x, ant.y, 1, 1);
    if (isNaN(ant.x) || isNaN(ant.y) || isNaN(ant.delay) || isNaN(ant.dir) ||
        isNaN(ant.hue)) {
      console.log('Ant #' + i + ' NaN\'ed out:');
      console.log(ant);
    }
  }
}

function startAnts() {
  let antFarm = [createAnts()];
  window.setInterval(() => updateAnts(antFarm), 50);
}

const LIGHTNING_SKY_COLOUR = '#2c1c4c';
const LIGHTNING_COLOUR = '#d9acf9';

function createLightning() {
  /*
  let ants = [];
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  let nAnts = Math.floor(canvas.width * canvas.height / 1500);
  ants = new Array(nAnts);
  let w = canvas.width;
  let h = canvas.height;
  for (let i = 0; i < nAnts; ++i) {
    let x = Math.floor(w * Math.random());
    let y = Math.floor(h * Math.random());
    let dir = Math.floor(4 * Math.random());
    let delay = getNextDelay();
    let hue = Math.floor(360 * Math.random());
    ants[i] = {x: x, y: y, dir: dir, delay: delay, hue: hue};
  }
  return ants;
  */
  ctx.fillStyle = LIGHTNING_SKY_COLOUR;
  ctx.fillRect(0, 0, canvas.width, canvas.height);
  return {
    nextStrikeDelay: 0,
    bolts: [],
  };
}

function forkDelayOnGeneration(num) {
  switch (num) {
    case 0:
      return randIntBetween(10, 40);
    case 1:
      return randIntBetween(20, 55);
    case 2:
      return randIntBetween(30, 70);
    case 3:
      return randIntBetween(40, 90);
    default:
      return randIntBetween(50, 110);
  }
}

function newBolt() {
  return {
    x: Math.floor(Math.random() * canvas.width),
    y: 0,
    dir: Math.floor(180 * Math.random()),
    generation: 0,
    forkDelay: forkDelayOnGeneration(0),
  };
}

function boltSpeed() {
  return 4 + Math.random() * 4;
}

function updateLightning(lightningFarm) {
  let w = canvas.width;
  let h = canvas.height;
  let vp = getViewportDimensions();
  if (w < vp[0] || h < vp[1]) {
    canvas.width = w = vp[0];
    canvas.height = h = vp[1];
    lightningFarm[0] = createLightning();
  }
  if (w == 0 || h == 0) return;
  let lightning = lightningFarm[0];
  // Darken image by ~3% to avoid light being overcrowded
  ctx.globalCompositeOperation = 'source-over';
  ctx.fillStyle = 'rgba(44, 28, 76, 0.02)';
  ctx.fillRect(0, 0, w, h);
  ctx.globalCompositeOperation = 'source-over';
  if (lightning.nextStrikeDelay <= 0 && lightning.bolts.length < 64) {
    lightning.bolts.push(newBolt());
    lightning.nextStrikeDelay = randIntBetween(30, 130);
  }
  --lightning.nextStrikeDelay;
  ctx.lineWidth = 1;
  ctx.strokeStyle = LIGHTNING_COLOUR;
  let len = lightning.bolts.length;
  ctx.beginPath();
  for (let i = 0; i < len; ++i) {
    // Update bolts
    let bolt = lightning.bolts[i];
    let speed = boltSpeed();
    let offsetRadius = randFloatBetween(2, 5);
    let offsetDirection = 2 * Math.PI * Math.random();
    let newX = bolt.x + speed * Math.cos(bolt.dir * Math.PI / 180) +
        offsetRadius * Math.cos(offsetDirection);
    let newY = bolt.y + speed * Math.sin(bolt.dir * Math.PI / 180) +
        offsetRadius * Math.sin(offsetDirection);
    ctx.moveTo(bolt.x, bolt.y);
    ctx.lineTo(newX, newY);
    bolt.x = newX;
    bolt.y = newY;
    bolt.dir += 6 * Math.random() - 3;
    bolt.dir = 0.995 * bolt.dir + 0.005 * 90;
    --bolt.forkDelay;
    if (bolt.forkDelay == 0) {
      // Fork the bolt
      let nextGen = bolt.generation + 1;
      lightning.bolts.push({
        x: bolt.x,
        y: bolt.y,
        dir: bolt.dir + 120 * Math.random() - 60,
        generation: nextGen,
        forkDelay: forkDelayOnGeneration(nextGen)
      });
      bolt.generation = nextGen;
      bolt.forkDelay = forkDelayOnGeneration(nextGen);
      bolt.dir += 120 * Math.random() - 60;
    }
  }
  ctx.closePath();
  ctx.stroke();
  lightning.bolts = lightning.bolts.filter(function(bolt) {
    return bolt.x >= 0 && bolt.x < w && bolt.y >= 0 && bolt.y < h;
  });
}

function startLightning() {
  let lightningFarm = [createLightning()];
  window.setInterval(() => updateLightning(lightningFarm), 50);
}

function startStatic(path) {
  let image = new Image();
  image.onload = function() {
    let cw = canvas.width;
    let ch = canvas.height;
    let iw = image.width;
    let ih = image.height;
    console.log([[cw, ch], [iw, ih]]);
    let x = Math.floor(cw / 2 - iw / 2);
    let y = Math.floor(ch / 2 - ih / 2);
    console.log([x, y]);
    ctx.drawImage(image, x, y);
    console.log('done');
  };
  image.src = path;
}

function startAnim() {
  canvas = document.getElementById('ants');
  ctx = canvas.getContext('2d');
  let dims = getViewportDimensions();
  canvas.width = dims[0];
  canvas.height = dims[1];
  let data = window.localStorage;
  let anim = data.getItem('animation') || 'ants';
  switch (anim) {
    case 'ants':
      startAnts();
      break;
    case 'lightning':
      startLightning();
      break;
    case 'static': {
      startStatic(data.getItem('imagePath') || '/image/toaster.png');
      break;
    }
    default: {
      console.error(anim + ': no such animation');
    }
  }
}
