---
title: Arka stuff
---

# kozet's Arka portal

## Starter resources

[The official Arka website](http://conlinguistics.org/arka/e_index.html) is no longer maintained (it seems), but it's a good resource for people who want to learn about Arka.

[The Vulgar Arka dictionary](http://mindsc.ape.jp/klel/) is still updated (albeit in Japanese); this is the dictionary you should be using.

[A work-in-progress translation](http://zpdic.ziphil.com/dictionary/arkarattoklel) of the above dictionary in English.

[Titlil Gas's portal.](http://titlilgas.blogspot.jp/2016/08/blog-post_3.html)

[Kakis Erl Sax's wiki](https://www33.atwiki.jp/kakis/pages/1.html) is a great place for historical Arka materials if you can read Japanese.

[<hacm>diakacte poten tier e trim</hacm>](https://discord.gg/gczVCVa8nN) – Discord server for speaking in Arka.

## What to watch out for

Also see [this page of common mistakes.](./mistakes.html)

Things that aren't mentioned in the official site, at least not well.

* <span class="hacm">o</span> is used before a word starting with a consonant. <hacm>ont</hacm> comes before a vowel-initial word.
* If you want to relativize an object of a preposition (here, we use "preposition" for what the official site calls "casers"), the preposition should be put at the end of the clause, as done in English while trying to piss prescriptivists off. _The city in which we live_ is translated, for instance, as <hacm>haim l'ans ra ka</hacm> (city <sc>rel</sc>=<sc>1pl</sc> live in). Omitting the preposition is also acceptable. See [this page](https://sites.google.com/site/faraspalt/levian/arka/vez/le) for more details.
* Speaking of which, relativizing genitives isn't so straightforward. The above site translates _a friend whose father is a teacher_ as <hacm>hacn le til kaan lex xaxan</hacm> (friend <sc>rel</sc> have father as teacher). What should you do if the clause uses some other verb? Beats me.
* You might be confused about ["turning conjunctions into casers"](http://conlinguistics.org/arka/e_study_malt_12.html). In short, a preposition (note: we're not including <hacm>e</hacm>, which is special) can modify only a verb by default. Attaching <hacm>-en</hacm> allows it to modify a noun instead.

## What happened to Seren Arbazard?

> <hacm>SEREN ARBAZARD SETES SAT XE, HAYU LU ES FOS. el sain sen tu kon fi oktfesk.</hacm>

_– [Nias Avelantis](https://twitter.com/xaiesk/status/337545528353099776)_

The relevant news article was taken down, but [here's an archive link](http://web.archive.org/web/20130602072611/http://sankei.jp.msn.com/affairs/news/130601/crm13060118010008-n1.htm) (Japanese). I'll leave it at that.
