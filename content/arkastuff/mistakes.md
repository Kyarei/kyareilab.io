---
title: Common Arka mistakes
---

# Common Arka mistakes

Most of the following mistakes are ones I've made myself. This is mostly for myself, but it might help to avoid the same mistakes I've made.

## Phonology

### Pronouncing <hacm>j</hacm> as \[dʒ\]

Please don't; it's pronounced \[ʒ\].

## Morphology

### Not using <hacm>-es</hacm>

New speakers might be confused by the purposes of the <hacm>-es</hacm> suffix, which indicates 'continuous aspect'. Beginners are likely to use <hacm>-or</hacm> or no suffix at all when they should be using <hacm>-es</hacm>. The correct usage is to use <hacm>-es</hacm> to describe a state and <hacm>-or</hacm> to describe a dynamic action ([this article](https://en.wikipedia.org/wiki/Continuous_and_progressive_aspects) might be helpful).

Note that even then, some verbs such as <hacm>sin</hacm> cannot take the continuous aspect.

### Ordering of tense and aspect affixes

One might be tempted to assume that the aspect affix comes closer to the verb than the tense affix, but the tense affix actually comes first: <hacm>rsiil-at-or</hacm> 'was ruling'.

### <hacm>o</hacm> vs <hacm>ont</hacm>

<span class="hacm">o</span> is used before a word starting with a consonant. <hacm>ont</hacm> comes before a vowel-initial word.

If you made this mistake, I don't blame you that much. Even [the Japanese web site](http://conlinguistics.org/arka/study_malt_9.html) fails to mention this; you have to look in the dictionary to find out the difference.

### Getting adverbs wrong

This is partly a morphological issue and partly a syntactic issue, but let's go!

* Modal adverbs (法副詞) are such things as <hacm>sen</hacm> ('can') or <hacm>sil</hacm> (future tense) that convey a grammatical meaning. Most of them come after a verb, but <hacm>en</hacm> ('not') and the various adverbs for imperatives or prohibitives are an exception.
* Free adverbs (遊離副詞) include adverbs such as <hacm>aluut</hacm> ('always') which are more contenty than modal adverbs but more adverby than regular adverbs.
* Regular adverbs (labelled ［副詞］ in the dictionary) comprise of everything else.

#### Rules for adverbs

| type → | modal | free | regular |
|-|-|-|-|
| placement | adjacent to the verb (pre/postfix depending on adverb) | anywhere | anywhere |
| needs <hacm>-el</hacm> suffix? | no | only if not adjacent to the verb and not modifying an adjective | yes unless modifying an adjective |

## Syntax

### Using <hacm>myul</hacm> (or <hacm>enk</hacm>) as a verb

These words can only act as nouns, adjective or prepositions.

### Modifying a noun with a prepositional phrase with an adverbial preposition

Any preposition labelled with ［格詞］ (such as <hacm>sa</hacm> 'before') in the dictionary can modify a verb or adjective only, while any preposition labelled with ［接続詞］ (such as <hacm>e</hacm>) can modify a noun only. The former kind of preposition can take the <hacm>-en</hacm> suffix in order to convert it to the latter (note: <hacm>a</hacm> → <hacm>alen</hacm>; <hacm>i</hacm> → <hacm>iten</hacm>).

## Semantics

### <hacm>ag</hacm> vs <hacm>haizen</hacm>

Note that <hacm>haizen</hacm> is used when one has a feeling of guilt. If there is no such feeling, <hacm>ag</hacm> should be used instead.

### <hacm>hot</hacm> vs <hacm>tis</hacm>

<span class="hacm">hot</span> means 'nothing or no one except'; <hacm>tis</hacm> means 'no more than'.

Ex: <hacm>an taut lin miik hot.</hacm> – I bought only five apples; I did not buy oranges or bananas

Ex: <hacm>an taut lin miik tis.</hacm> – I bought only five apples; I did not buy six or seven

### <hacm>fiina</hacm> vs <hacm>lana</hacm>

<span class="hacm">fiina</span> translates to 'on behalf of' and indicates that the target receives a benefit from the action: <hacm>an labat fiina xite</hacm> 'I worked for my family'.

<span class="hacm">lana</span> translates to 'in order to' and indicates a goal instead: <hacm>an felat lana tas</hacm> 'I studied for the exam'.

## Pragmatics

### <hacm>lala</hacm> vs <hacm>lulu</hacm>

They both translate roughly to 'oh my!', but the former is a reaction to something negative and the latter to something positive.

## Unorganised stuff

'here and there' ⇒ <hacm>kor dipit</hacm>

instead of <hacm>at esi</hacm>, use <hacm>emat esi</hacm> (?)

become aware of something: <hacm>xa, xan</hacm>

'what kind of' ⇒ <hacm>ak</hacm>

'to the degree that' ⇒ <hacm>ento</hacm>
