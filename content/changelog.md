---
title: Recent changes
---

# Recent changes

At the moment, changes before 26IV2019 are not recorded.

## 2021/10/02 <span class="idtag hacm">32 mik kun</span>

* 2 new translations: [Why there has not been any work like Arxidia until now](/estel/arka/study/3.html) and [A beautiful language](/estel/arka/study/163.html)

## 2021/05/31 <span class="idtag hacm">32 mel rav</span>

* Add a [new story](/langdocs/levian4/story.html) 

## 2021/01/31 <span class="idtag hacm">32 lis mel</span>

* Some minor updates to some pages
* Minor updates to English translation of Sonohinoki
* Add [Ŋarâþ Crîþ translation](/estel/arka/works/selneeme-4v7.html) of Sonohinoki
* Add [stuff on documenting ŊCv9](/langstuff/levian4/documenting.html)

## 2021/01/13 <span class="idtag hacm">32 vio lin</span>

* That's "thirteen eye twenty twenty-one". Well, fuck it, if I have to tell you this, then I might as well change the date format now. Also haha funny flelzol joke
* Revamp layout
  * Now dark-mode
  * Changed the fonts because serif fonts aren't particularly good for screens (as they say)
  * Now [distinguishes between visited and unvisited links](https://www.nngroup.com/articles/change-the-color-of-visited-links/)
  * Code highlighting!
  * If your screen is wide enough, then footnotes (at least those that are done via Kramdown instead of manually jerry-rigged) will show up on the side
* Add a [sitemap](/sitemap.html)
* Yes, I know it's already the day of Relezona for those of you who actually speak Arka

## 30XII2020 <span class="idtag hacm">32 vio lis</span>

* Replace manual back-links with automatic breadcrumb trails
  * Gee, thanks for making me learn Ruby again.

## 27XII2020 <span class="idtag hacm">32 dia mir</span>

* Add [The reason of Ŋarâþ Crîþ's existence](/langdocs/reason4v7.html)

## 23X2019 <span class="idtag hacm">30 fav jil</span>

* Post for [Conducer Hub Contest \#0](/contests/ch00.html) is live!

## 21X2019 <span class="idtag hacm">30 fav lin</span>

* Make [another stupid thing](/langdocs/chinpunkanpunpsim.html)

## 5X2019 <span class="idtag hacm">30 fav dia</span>

* Add Conlanginktober
* Add a few more 'frinds don't let friends' messages
* Update about page

## 5VIII2019 <span class="idtag hacm">30 zan nen</span>

残念！

* *friends don't let friends forget update notes*
* [New article](langdocs/sedplague.html)

## 7VII2019 <span class="idtag hacm">30 ral lax</span>

Add lyrics to my first (substantial!) original song.

## 2VI2019  <span class="idtag hacm">30 mel tan</span>

* Add a list of personal projects
* Add a new homepage animation option

## 26IV2019 <span class="idtag hacm">30 dyu mel</span>

* Add the changelog
* Add tanapo
