---
title: On part-of-speech changes for casers
---

# On part-of-speech changes for casers

*[This article] was posted by davfapel unilent (aka Lulu Asaka) on their website.*

We attempt to classify casers by how their meanings change as their parts of speech change. This commentary was originally written on Twitter before being edited and posted on Google Sites. There might be postscripts depending on further progress.

In this matter, the following is a temporary classification:

1. Those that do not change their parts of speech
2. Those that show one of the following states of the preceding sentence that nominal usage of the caser in question signifies
    1. Those that are rewritten by using <hacm>e</hacm> and <hacm>le</hacm>
    2. Those that are rewritten with <hacm>le</hacm> alone; in this situation, such a word's usage as a caser becomes visible
    3. (?) Those that show place, time, or extent
3. Those for which the <hacm>a</hacm>-case in verbal usage corresponds to something in the preceding sentence
4. Those that show a pattern when the caser usage becomes a verb

(1) needs no explanation; therefore, we list the examples at once: <hacm>ento</hacm>, <hacm>kont</hacm>, <hacm>siet</hacm> &c.

(2a) forms rewrites as shown below (perhaps I should define this "rewriting"):

* <span class="hacm">noa taut miik du ta.</span> = <hacm>noa taut miik. du e miik le noa taut (yul) et ta.</hacm>  
  "I bought apples at the amount of two" = "I bought apples. The amount of apples that I bought is two."

Other casers in this category include <hacm>du</hacm> and <hacm>ati</hacm>.

In (2b), rewrites are done as follows:

* <span class="hacm">noa ar pam kon mois.</span> = <hacm>noa ar pam. kon le noa ar pam kon et mois.</hacm>  
  "I turn on the light with the remote control." = "I turn on the light. The tool that I turn on the light with is the remote."

Examples in this category include <hacm>kon</hacm> and <hacm>fol</hacm>.

(2c) is one that I don't understand well, but because it seems to have the nature of saying "the meaning in nominal usage is what the meaning in the caser usage takes (or the principal thing among that?)", it seems to be a lot like (2b). Whether or not it was from such circumstances, it was overlooked during the initial classification (I noticed while I made my progress public on Twitter), but since it doesn't look like it forms a rewrite, I'll separate this category as (2c) for the time being.

(3) includes casers such as <hacm>daz</hacm>. I thought <hacm>kax</hacm> and <hacm>enk</hacm>... also belonged here and said so on Twitter as well, but I have quite a weird feeling about that. (Perhaps it's because <hacm>kax</hacm> and <hacm>enk</hacm> probably can't take <hacm>-ati</hacm>, even though the others can...)

* In <hacm>vortmain xalat lif daz mana.</hacm> ("The corpse stayed young as fitting for a girl."), one can also say <hacm>lif</hacm> (or to be exact, <hacm>lifati</hacm>) <hacm>et daz a mana</hacm>; <hacm>ikl ifi ont ext et liefil daz est.</hacm> ("The dirty, old building is deep enough to deserve a name") can become <hacm>ifi(-ati) ont ext(-ati) et daz a est</hacm>. I don't really understand where to take the "something about the preceding sentence" from. It's a topic to be researched in the future.

(4) is explained through rewriting in a similar way as (2). Here as well, the definition of rewriting is more informal.

* <span class="hacm">an keks van haas ano.</span> ("I'm going to travel according to plan.")  
  → <span class="hacm">an keks van. im keks, an haas ano.</span>
* <span class="hacm">an miksat miks haas solet luut.</span> ("I sang the song according to his actions.")  
  → <span class="hacm">an miksat miks. im miks, an haasat solet luut.</span>

Examples include <hacm>haas</hacm> and <hacm>tel</hacm>. <hacm>gart</hacm> might also belong here.

Perhaps there are more categories, but currently (<hacm>mel 32 dia rez</hacm>), these are the bounds for me.

I wrote more at the beginning, but if there's any progress, then I might put up a postscript here.

[This article]: https://sites.google.com/view/luluasaka84/%E3%83%9B%E3%83%BC%E3%83%A0/%E3%82%A2%E3%83%AB%E3%82%B7%E3%83%87%E3%82%A3%E3%82%A2/%E8%80%83%E5%AF%9F/%E6%A0%BC%E8%A9%9E%E3%81%AE%E5%93%81%E8%A9%9E%E8%BB%A2%E6%8F%9B%E3%81%AB%E9%96%A2%E3%81%97%E3%81%A6
