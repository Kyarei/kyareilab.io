---
title: About this site
---

# About this site

*Translated from the [original Japanese]. This pertains to Conlinguistics's blog, not kyarei.gitlab.io.*

2014/01/19

By Nias

This blog is set up to preserve and provide new articles and information related to the conlang Arka through the exchanges of letters with Seren Arbazard, the creator of Arka.

In addition, this blog bears the responsibility of being a point of contact with Mr. Arbazard. To get in touch with him, enter the information you wish to send in any comment section here. Nias will forward the messages. Questions related to Arka are especially welcome here.

**Note:** Information related to the incident or text in Arka itself might be censored out. It's better to write out the Arka parts in *katakana* or describe the phrases in translation. Please keep this in mind.

---

Furthermore, with the approval of Mr. Arbazard, the mirror of the official Arka site will be updated; therefore, those news will be supplied there at the same time. For now, we plan to add supplementary material pages for these.

[original Japanese]: http://conlinguistics.org/blog/?p=19
