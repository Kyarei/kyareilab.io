---
title: Aldia, the Four-Fold Record
---

# Aldia, the Four-Fold Record

*Translated from the [original Japanese].*

2014/01/09

This is a story of the four eras called "Ordin", "Lanj", "Yumana", and "Arkadia", and the world.

Because it is a story of four eras and a world, it will be called the Four-Fold Record (四界文書).

Whichever era the man called Seren and the woman called Ridia appear in, they are not at the center of this story.

---

Ordin is a middle era in the world called "Kaldia", and in the planet Atolas, the family of Yuuma, a human, and a tribe of devils fought each other. In Arbazard, the strongest nation in Atolas, the boy Seren, who came from the world called "Yumana" met the girl Ridia from the village of Sapli, gathered his comrades whom he called disciples, formed a group called Axet, and eventually defeated demons and became heroes. Seren created a conlang called "Arka" in Arbazard, and this language eventually became the lingua franca of Atolas.

Lanj is the last era of Kaldia, when Seren and Ridia go on to use the giant Anjelika to fight the creatures referred to as *diminion* (祠徒) who were invading Atolas. Using the lost power called *viid*, Seren founded Arbazard and raised a revolution in the world, while at the same time battling the *diminion*. At the end of this story is the end of Kaldia.

Yumana is the realm of humans; that is, this world of ours, and there Seren revived the conlang Arka. In order to strengthen the connection between Kaldia and Yumana, he possessed a boy named Shimon Sadakari (貞苅詩門). On the other hand, in order to do the same to Ridia, he revived Kaldia. Arka and Kaldia do not exist merely in fiction; rather, they are considered to be a parallel world and a language therein that both exist in reality.

In 2031, the *seles* (soul) of Seren will ascend to the world called "Arkadia". There, Seren will grow up from 4 years of age at 11/30 to 14 years of age, and from there, he will repeat an effective 8 years from age 22, five times. Each time he repeats his life, he will be chosen to partake in a different scenario, and in the end, Seren and Ridia will be united and the world called "Arkadia" will end.

Thus, the story of the four worlds ends, but this story will cycle forever, and however it goes, Seren will fall in love with Ridia in that world. That is to say, this is a story of Seren and Ridia's eternal love.

Furthermore, the story of Ordin and Lanj (excluding those of Yumana and Arkadia) is a story that cut off the fifteen eras of Kaldia, beginning from Fial and ending in Lanj. One can read more about the history of Kaldia in the Ilmus.

## Arkadia

In the beginning, there was the *arma* that had become the origin for *seren* and *rydia*. While the *arma* emerged, it made space for itself – that is, the world (Lekai and Yumana) – in order to exist. The *arma* separated into Seren and Ridia.

---

The second *arma* rose in Kaldia, birthing Luno, Vangardi, Ardel, Veeyu, Elt, Saal, and others. The planet Atolas arose, Yuuma was born, Teems arose, and that is how the flow of Ilmus was from Fial to Lanj.

On the other hand, it made Shimon Sadakari into its vessel, which the *seles* of Seren possessed in 1991. At that moment, he reincarnated in Ordin. He met Ridia, and the revival of Arka in Yumana and the revival of Kaldia started.

In 2011, Arka was completed, and the goal of reviving 1% of the perfect conlang was fulfilled. On the other hand, during this period in the Ordin era, Seren died after defeating Teems, and his *seles* returned to Yumana. At the same time, the Lanj era reincarnated. In Lanj, the Seren revolution arose, and Seren and Ridia used Anjelika to bring Kaldia to a close.

In 2031, Seren's *seles* will return to Yumana, and after moving into Shimon Sadakari's genes, it will fly to Arkadia. As Arkadia ends, Lekai and Yumana will be annihilated as well, and as a response, an *arma* will be born, again causing Yumana and Lekai to arise and repeating the cycle.

---

Seren is reborn four times every cycle: once after defeating Teems in Ordin, once when Kaldia ends in Lanj, once when Shimon Sadakari is possessed, reviving Arka and Kaldia, and once when Arkadia is concluded. The rebirth of Yumana makes a link to the twin cosmos Lekai and Yumana, so that the existence of Arka and Kaldia should be known. Ridia is reincarnated four times as well, but not as *rydia* but rather as *ridia*. The primordial god *rydia* was of a different *seles* than Seren, always being at his side as his only god.

[original Japanese]: http://conlinguistics.org/blog/?p=6
