---
title: "On conlang dictionaries and Vulgar Arka"
---

# On conlang dictionaries and Vulgar Arka

*Translated from the [original Japanese].*

2014/02/12

Note from Nias: These are Seren's opinions on reading the Conlang BBS Arka thread, the Conlang BBS 2 Arka thread, and the Vulgar Arka contact board.

## On conlang dictionaries

Although work on the cloud is suited to editing by multiple people as in Vulgar Arka, a local application is easier if only one person is involved.

It is clear that it is faster to look up a word on PDIC than in the Vulgar Arka dictionary, as well as to register a word on PDIC.

In the case of cloud work, there are times when a recorded word doesn't show up because of communication errors from CGI, and the response is also slower than with local work.

Everyone does work locally, but if that is the case, adding words to a conlang dictionary on the cloud makes double effort, so why bother?

Local work is automatically reflected in the conlang dictionary, if there is such a local application.

There might be an online system to PDIC, but if possible, it would be ideal if:

* each creator's local work were reflected on the conlang dictionary file, containing the entire conlang's lexicon collected into one on the cloud, and
* users could search through the conlang dictionary on the cloud.

For instance, the work done by WYU locally is reflected as the data for Meijulange on the conlang dictionary[^1].

Of course, not only registrations but also amendments are reflected.

In addition, should the format of a conlang dictionary be based on Wiktionary's?

The format for local applications is PDIC.

Although it has some demerits: the creator must spend time entering the inflected forms separately, while this is perhaps handled by the Arka-Japanese dictionary during searching, isn't this the same with Wiktionary?

For one thing, even the fact that the names for parts of speech differ for each language does not become a problem with this system.

Because Wiktionary is not suited for looking up words by translations or examples, it might be better to adopt the Arka-Japanese dictionary's system as a format depending on the situation.

Well, everyone also has the problem of "PG yannoyo", but the problem is rather who manages it after it is made.

What if this person isn't one who works on a conlang for decades? Isn't it too harsh for the collection of people who leave them after two or three years?

Properly speaking, I might be the one responsible, but that is because I am a criminal.

## On Vulgar Arka

I have read logs from the BBS.

I think the direction of the activity is fine so far.

I approve of the status quo of working on this derivative work on a derivative site, given that it overturns Kaldia's setting on a grand scale.

When I thought about how much this feels like the time when the Arbans left behind created Vulgar Arka after Seren left the Ordin era, I thought that perhaps vulgarization will be essential as the final phase of Arka, even if it only continues for two or three more years.

Thank you.

[original Japanese]: http://conlinguistics.org/blog/?p=112

[^1]: *Possibly referring to a specific site?*
