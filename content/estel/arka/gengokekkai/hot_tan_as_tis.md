---
title: On the usage of converbs and hot, tan, as, and tis
---

# On the usage of converbs and <hacm>hot</hacm>, <hacm>tan</hacm>, <hacm>as</hacm>, and <hacm>tis</hacm>

*Translated from the [original Japanese]. In the original document, the entire text of the question was listed before all of the answers, but here, they are interleaved.*

2014/01/19

Faras asks:

> 1\. About the converbs (動副詞):
>
> Although there are explanations on the Arka-Japanese dictionary and the official site about the nominative converbs and the accusative converbs, isn't there anything on the reflexive and natural converbs?
>
> For the participles, there are four kinds (nominative (<hacm>-an</hacm>), accusative (<hacm>-ol</hacm>), reflexive (<hacm>-ast</hacm>), and natural (<hacm>-ont</hacm>)[^1]), but I'm thinking that it's possible that likewise there are four kinds of converbs.
>
> As the participles are adjectives, I considered that one could turn these into adverbs to derive each kind of converb. Still, even though there's no problem with the nominative converb (<hacm>-anel</hacm>) and the accusative converb (<hacm>-el</hacm>), if we apply this method to the reflexive and natural converbs, we get <hacm>-astel</hacm> and <hacm>-ontel</hacm>, respectively, which are identical to the reflexive and natural gerund suffixes. If the reflexive and natural converbs are to exist, then I would like you to tell me the suffixes.

The reflexive and natural converbs are theoretically possible. Since they can be told apart from the reflexive and natural gerunds from syntax, the homophony is not a problem.

"Ridia laughed bloomingly" becomes  
<hacm>ridia asexat mansontel.</hacm>

"He nodded falling" becomes  
<hacm>lu wikat metontel.</hacm>

> 2\. About the usage of <hacm>hot</hacm> (*only*), <hacm>tan</hacm> (*also*), <hacm>as</hacm> (*also*; signifies mental greatness), and <hacm>tis</hacm> (*no more than*; antonym of <hacm>as</hacm>)
>
> (a) About cases when the above four words are used as adverbs
>
> From the examples of <hacm>tan</hacm>, I understand how it can follow a verb and be used as an adverb without the adverbial suffix. Grammatically, one could say that they are free adverbs; however, in that case, can it be placed before the verb, or appear at the end of the sentence with the adverbial suffix, as possible with other free adverbs?
>
> Likewise, while it is clear from the usage sections of the other three words that they can also be used as adverbs, it cannot be known whether they are used as free adverbs or regular adverbs because there are no examples of such usage. I'd be happy if you told me.

<span class="hacm">hot</span> and friends can't be put before the verb[^2]. They come after. They are free adverbs.

Unlike with <hacm>aluut</hacm>, <hacm>hotel</hacm> and such are incorrect.

Unlike with <hacm>melt</hacm>, they are not ordinary adjectives of state or disposition; as adjectives, they have strong characteristics of function words.

Similarly to modal adverbs, they are like the English *can*, which cannot be inflected as an ordinary adverb into *canly*. Although *can* is an auxiliary verb.

They can be used as free adverbs; for example, <hacm>inat hot</hacm> means "only saw (no other action)" and <hacm>inat tis</hacm> "did no more than seeing".

> (b) About the usage of the above four words as casers or conjunctions
>
> When <hacm>hot</hacm> is placed after <hacm>ol</hacm> (if ~) or <hacm>im</hacm> (when ~), it seems to involve the entire phrase that <hacm>ol</hacm> or <hacm>im</hacm> precedes. But could it be used after casers or conjunctions other than <hacm>ol</hacm> or <hacm>im</hacm>, such as <hacm>xei</hacm>, an adverb that shows assumption when placed in the same location?
>
> Also, I was wondering whether you could do the same with the three words other than <hacm>hot</hacm> above, but is it impossible? (For example, <hacm>ol tan</hacm> for "even if", or <hacm>im as</hacm> for "as long as".)
>
> Excuse me very much, but I'd be happy if you replied. Thank you!

Things such as <hacm>ol tan</hacm> or <hacm>im as</hacm> are possible as you said.

This time, I had to hurry because I didn't have time, so the grammatical explanation of the adverb <hacm>hot</hacm> isn't that good.

Next time you ask me questions, I'll think over them more closely before answering.

[original Japanese]: http://conlinguistics.org/blog/?p=14

[^1]: The "nominative participle" signifies an agent (e.g. <hacm>axt/an</hacm> *one who writes*; <hacm>set/an</hacm> *one who kills*), and it is implied that it can also be used attributively. Likewise, the "accusative participle" signifies a patient (<hacm>set/ol</hacm> *one who was killed*), which is likely also usable attributively. The "reflexive participle" signifies that the head is both an agent and a patient (in this case, the subject and the direct object): <hacm>nalt ovast</hacm> *a moving castle* \[cf. <hacm>nalt ov nos</hacm>\]. The "natural participle" is similar to the accusative participle, but the subject is considered to be <hacm>el</hacm>: <hacm>tek metont</hacm> *a falling leaf* \[cf. <hacm>el met tek</hacm> or <hacm>tek et met</hacm>\].

[^2]: The original text puts "gerund" (動名詞), but this is probably a typo.
