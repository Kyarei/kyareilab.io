---
title: "Lanj: 2011/11/1"
---

# Lanj: 2011/11/1

*Translated from the [original Japanese].*

2014/02/07

**Note from Nias:** This account is ported from an old edition.

**2011/11/1.**

In the orphanage of Flanje, there were four orphans besides Ridia. They did not know their surnames at all, and only barely remembered their given names. They were all found by Liiza and Miifa before being brought here.

The first was Ixtal. She seemed to be about fourteen or fifteen years old, but her actual age was unknown. She always wore black and purple, and she had an obedient personality.

Ixtal was picked up by Liiza a few years ago, despite having been drenched by the rain. For some reason, she occasionally said things in archaic language. She was a strange child.

The second was Milha. Eleven years old. A cross between a *diires* (a demonic race) and a spirit. Picked on for naturally having cat ears. Although she was a demon, she was also a timid crybaby.

However, since Milha's potential was strong, she had the habit of inadvertently pulverizing everything around them whenever she felt an excessive amount of fear.

Because she had an extreme complex about their ears, she always wore a hat. She was afraid of being touched on the head, to the point that she even disliked being stroked.

The third was Konoha. She appeared to be sixteen years old. Although she had an Altian name, she was a fully-fledged *lozet*, or a cross between a god and a human.

In spite of being a *lozet*, she was born ruling an existing thing and was banished from heaven as a result.

She felt that making people happy was her reason to live. An admirable and pure naivete. But because of her cursed blood, her kindness always backfired.

The fourth was Fealis. She were seventeen years old and an abandoned child of human parents. A master of *yuvel*, she cherished Milha as her younger sister.

Fealis was sentimental at her core. Not having recovered from the trauma of being abandoned by her parents, she had an abnormal fear of being rejected.

The four greeted the newcomer Ridia when she entered the circle of friends. But like a sleeping beauty, Ridia would respond only with an idle gaze, and conversation was futile.

The girls gave up on trying to talk to the unfortunate-looking Ridia, to the point that they looked at her, surrounding her from a distance, as though she would cherish flowers.

This day, as Seren was helping the orphanage, he repeatedly organized the personal histories and the characteristics of each girl in his head. He did not know what kind of past Ridia had. But if the girls spoke about the darkness of their own histories, then hers would probably be less bad. If those girls behaved with a bright and lively spirit, then Seren thought about whether or not Ridia would also smile again before long.

"It's got a little cold," he said to Mel, who was sitting across him.

"It's got cold, don't you think?" Mel replied.

Those words were felt warmly.

[original Japanese]: http://conlinguistics.org/blog/?p=65
