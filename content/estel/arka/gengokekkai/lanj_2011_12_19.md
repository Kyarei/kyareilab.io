---
title: "Lanj: 2011/12/19"
---

# Lanj: 2011/12/19

*Translated from the [original Japanese].*

2014/02/07

**Note from Nias:** This account is ported from an old edition. The changes are in bold.

**2011/12/19.**

Although she had dinner every day around eight o'clock, today, Ridia did not go down to the living room.

Worried, Seren went to her room. He knocked, but there was no response.

"Ridia...?"

He opened the door, which had no lock.

The inside was completely dark. Ridia was lying down on the bed inside.

He turned on the lights and approached her. Her face was white and she was cold to the touch.

"Ridia?"

Still skeptical, he took a shake at her shoulders. No reaction. Panicking, Seren put her hand to her cheeks. They were as cold as ice.

"Hey..." Seren turned pale. He put his hand to her chest and listened for a heartbeat. He heard a sound of a *thump*.

Good. She wasn't dead, it seemed.

He descended to the living room. Seeing Seren with a heavy look, the girls seemed to become concerned.

Mel also rose up, wondering what was the matter, but Seren held her back and called Liiza and Miifa to Ridia's room. He didn't want to get Mel or the orphans involved.

When Miifa entered the room, her face turned serious, and she proposed calling a doctor. Seren also agreed. But Liiza was the only one who appeared to come to her senses. "This is fine,", she said.

Seren and Miifa looked at her with a surprised expression. Liiza added, "A doctor won't be able to do anything."

"What's going on?"

"Whatever it is, she isn't ill," Liiza stated nonchalantly.

"If it's not an illness, then what is it?"

"Running out of time," she said readily. "Ridia always seems to be a sleeping beauty, right? No wonder. It's because she doesn't have a heart."

"A heart...?"

"To be precise, flowers of the heart (心の花)." She pressed down on Ridia's chest with her forefinger. "The first is the cosmos. The second is the hydrangea. Her heart is composed of a total of nine flowers. But they have scattered and she has lost them. That's why she remains like a doll without most of her heart, being like a sleeping beauty."

Putting his hand to his mouth, Seren listened closely to Liiza's words.

"To be more precise, she has lost eight of the flowers. When you found Ridia in the summer, she had only one of them left. As a result, she could maintain her life force by a narrow margin. But that quickly becomes the limit."

"The limit, ..."

"Unless you collect the other flowers, she will become cold to the touch in this state."

"If that's the case, then what will happen?"

Liiza gave no answer. Perhaps Ridia would die then.

"... Even if those words are true, how do you know about all this?"

As she turned around, she chuckled and answered, "Well?" But Seren still understood for some reason. Why did Liiza easily approve of him picking up Ridia in the summer? Could she have known from the start that she was not a normal orphan and had some sort of problem? But most of all, Seren had no idea that she knew something.

Not having known about the situation, Miifa asked Liiza in order to sort things out. "So to get her back to what she's supposed to be, we have to get the other eight flowers of the heart back?"

"Yes."

"So... what specifically are we supposed to do?"

Liiza put her hand to Ridia's chest. Upon doing so, one cosmos flower emerged.

"This is the flower that's maintaining her life, even though she's already very much weakened."

Miifa put her hand to her chin. "The hydrangea and the fragrant olive are somewhere the same way, right?"

"Yes. Her flowers flew all around and entered the minds of people."

"Whose...?" asked Seren nervously.

"In the beginning, they scattered around the world. Before long, they went through time and entered the minds of eight girls who held wounds in their hearts. The flowers are healing their wounds. If one could soothe their souls, then they would no longer need those flowers. In that case, the flowers can be retrieved from their bodies and returned to Ridia."

"Eight girls... was it?" That was astounding. "I have so many questions piling up. First of all, it's hopeless to try searching for those girls, because there are so many whose hearts are injured."

"For that point, it's okay." Liiza looked around the room. What do you think this orphanage is here for?"

"Pardon...?" He tilted his head at Liiza's question, which seemed irrelevant.

"It seems that the ones in this orphanage are all girls, right? Ixtal, Milha, Konoha, Fealis. All of these orphaned girls have some kind of wound in their hearts."

"Are you saying that each of them has one of Ridia's flowers in their own minds?"

Liiza nodded slightly.

"But that's still four people, only half of them."

"Oh, aren't we girls too?" she chuckled. "Mel, Miifa, Liiza. Three people here."

"Among you all, too...?" Seren shook his head. "Wait a minute. I don't know the meaning of this. From the way you're saying it, do you mean that this orphanage was built to gather those who have Ridia's flowers in their hearts?"

"Yes, that's the reason."

"This doesn't make sense chronologically. You've been collecting them before Ridia arrived here? And didn't you build this orphanage in 396 anyway?"

"Oh, the calculations are coming together. After all, Ridia was born on the year 395, in the month of Zana and the day of Raldura."

That clicked to Seren, who, specializing in the study of magic, was well-acquainted with medieval history. That was exactly 400 years after the birthdate of the heroine Ridia Lutia from the Ordin era.

"That is, you want to say that she's the reincarnation of the heroine Ridia Lutia, right? But that's impossible. It's now 423. **She's seven years old,** so she must have been born in **415**. That's far off from 395."

"You were judging that from her appearance, right? She reincarnated in 395. And then she was left blank in that state, until she appeared last summer in the form of a seven-year-old at the pond in Felixia University. No one knows where she went during the meantime, or why she became a seven-year-old."

That was hard to think about. But if what Liiza said were true, then the numbers and the calculations add up. Ridia reincarnated in 395, and she had one flower left and had lost the other eight. Liiza built the orphanage in 396 to collect the humans who had Ridia's flowers. —If one thought that, then it made sense. Then he was satisified with the details of how Ridia's situation came to be up to now.

"Let's suppose what you're saying is right. Or rather, I have no choice but to believe you. —Apart from that, what in the world am I supposed to do?"

Liiza chuckled and turned about. "It's straightforward. You just have to heal the emotional scars of the girls of the orphanage." Surely it was simple. But at the same time, it was terribly difficult. "Or else," she said as she put her hand on Ridia's chest, "she will turn cold in this state."

"..."

Liiza grabbed Seren's hand.

"You were also picked up and brought here. Do you plan to keep the promise?"

"... Yes," Seren calmly answered.

Liiza placed a blanket on Ridia and tried to leave the room.

"May I ask you one more question, Teacher?"

"What is it?"

"Including you all, there are seven people. The last one, ..."

Liiza smiled as she put one of her small hands towards her mouth. "You'll see."

[original Japanese]: http://conlinguistics.org/blog/?p=69
