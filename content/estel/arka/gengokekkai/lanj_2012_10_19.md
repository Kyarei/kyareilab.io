---
title: "Lanj: 2012/10/19"
---

# Lanj: 2012/10/19

*Translated from the [original Japanese].*

2014/02/07

**Note from Nias:** This account is ported from an old edition.

**2012/9/19.**

Seren had a cold and was in the hospital. Mel came as well as an escort.

He had told her that she didn't have to because she might catch it, but she came anyway because she was worried.

"To think you'd get a cold at this good time of year."

"Just summer fatigue piling up, probably. And it's also the time when the seasons change again."

"For now, it'd be better not to turn up at the orphanage. You'll spread the cold."

Seren stared blankly at the door of the examining room. It had yet to be his turn.

A man in a suit came through the hospital entrance and called out to the counter. From his behavior, he seemed to be a medical representative.

After speaking to the reception desk, the MR stood beside the entrance. Even though there was a vacant seat where he could sit, thought Seren.

"... Even the hospital doesn't reform, does it," he vented.

"Huh?"

"MRs are in the pharmaceutical business. There are plenty of drug companies, and plenty of effective drugs in this world. And a doctor chooses the medicine to prescribe to a patient. When it comes to that, the company naturally wants it to be one of their own drugs."

"So they want to increase their sales?"

"Promoting a drug is just business. To get them to use their own company's products, MRs from every company curry favor with doctors."

"So things like understanding doctors' preferences and giving them gifts, and also woman MRs inviting them to dinners and use their sex appeal?"

"That's it. They follow the selfishness of doctors by any means. There's been times when they go through the trouble of getting a ticket to an idol concert for a doctor or something."

"They sometimes exchange sexual favors or something?"

"Oh, of course it's probably not zero in this world. And even worse, the MRs seem to be paying attention to the nurses."

"What kind of things do they do?"

"There's this story. An MR from Company A drops some sweets to a nurse. The one from Company B doesn't. One day, the doctor was in a bad mood. When Company A came, the nurse made the smart move of telling them that it was better to call later because he had a bad mood. But when Company B came, she didn't tell them that. Company B retreated after the doctor vented his anger."

"Whatever you think about the gift of sweets, the nurse was just a dick there, wasn't she?"

Seren slammed his right fist into his left palm. "Hey, isn't this effectively bribery? Even without any special treatment, a doctor is supposed to adopt the least expensive medicine suitable for the patient. The nurse should have told the other MR that it was bad time to come, even if they didn't give her any sweets."

"Sure," Mel nodded.

"Most MRs have less academic background than doctors. They have less social status and income as well. In other words, they're the weak. The doctors are the strong ones. The strong ones are taking the bribes, and they're being exploited at the same time the weak ones question their expressions. This is the reality."

Seren stared at the door of the examining room. His tone of voice became irritated.

"How dirty it is, the way it goes! These MRs who act obsequiously to doctors and try to outwit other companies, and the doctors and nurses who want to quietly receive bribes."

"Yeah, I get it..."

"When there was Mirok, was society corrupt in this way?"

Mel shook her head. "If you're speaking of Mirok, then I certainly think he engaged in political purges. Since I think he was a person who hated that kind of corruption as much as you. Actually, I think society was even more impartial in his days."

"Don't you agree? Strong people exploit the surviving corrupt weaklings by acting quickly and skillfully. Those on the receiving end are honest people, tactless and vulnerable with a pure heart. Can we let this keep happening?"

"I don't think so."

"The lovely people with honest and pure hearts are put at a disadvantage and fall further into the vulnerable classes. In such a society, only corrupt people can survive, laughing as they become nothing but pigs fat from greed. This world is different."

Mel used both of her hands to wrap around Seren's fist.

"It seems you want to change the world. Just like Mirok."

"Shit, if I had the power, then I'd impose a benevolent government...!" muttered Seren loathsomely.

"If you were a doctor, then you'd probably yell at an MR trying to bribe you and send them home, wouldn't you? As an upright and cleanhanded doctor. ... That's cool." Mel snuggled into Seren's arms.

"What, you don't like how I became a scholar? You'd rather be a wife of a doctor?"

"Nah, it just would've been cool to have a big brother as a doctor. Being a scholar or an artist fits you the best. Even so, it isn't popular—"

"Even if it's not popular, is it okay?"

"Or rather, it's better that it's not. It's just like you. Don't you think that the act of being popular itself is pandering to the corrupt earthly world?"

Mel smiled, and Seren carressed her head gently.

[original Japanese]: http://conlinguistics.org/blog/?p=88
