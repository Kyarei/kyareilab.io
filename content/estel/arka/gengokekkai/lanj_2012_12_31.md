---
title: "Lanj: 2012/12/31"
---

# Lanj: 2012/12/31

*Translated from the [original Japanese].*

2014/02/07

**Note from Nias:** This account is ported from an old edition. The changes are in bold.

**2012/12/31.**

When, Seren entered the living room of the orphanage, the four orphans were having dinner.

Milha: "Oh, Seren. Welcome home."

"Where are Liiza and Miifa?"

"They're not here today. But I made dinner. You eat some, too."

"Thanks, I'll do so."

Seren took a seat. He looked at Milha with a sidelong glance. Even though she was indoors, she was wearing a hat.

Milha was a cross between a *diires* (one of the demonic races) and a **spirit**. She was born with cat ears. She was teased when she was a small child, and she had a complex and became shy. It seemed that she did not have any real friends in school.

"Milha... aren't you going to take off your hat yet?"

"No..." She clutched onto her hat tightly.

It seemed that she had such a complex about her ears that she would not take off her hat even in the familiar orphanage.

When dinner was finished, the orphans went back to each of their rooms.

Seren visited Milha's room.

"Oh, Big Brother." Milha was in pajamas. She hurried to put on her hat. But Seren interrupted her move.

"Milha. You don't want to show them even to me?"

"Huh... um... because it's weird, these."

Being silent, Seren stared at Milha's eyes. "... Is there anything wrong with being weird?"

"... I'll get bullied."

"I won't do that. I think your ears are cute."

Turning red, Milha looked down.

"... I'll pet you?"

Milha nodded slightly.

Seren stroked her head. She twitched as he touched her head but did not seem to dislike it.

"When you're in front of someone who won't make fun of your ears, how about you take off your hat? You can overcome your complex step by step."

"Do I have to overcome it eventually?" She looked worried.

"No, it's not that. A complex is an essential part of a human. Humans aren't strong enough to surmount all of their complexes.

"Even if you don't manage to overcome your complex, you can be proud that you made the effort to do so."

Milha buried her face in Seren's chest. She turned her arms and clinged onto him tightly.

"Milha... If you're with me, then you can show them."

"Really, thank you..."

Seren smiled and caressed her head without reservation.

[original Japanese]: http://conlinguistics.org/blog/?p=94
