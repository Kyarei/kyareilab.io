---
title: "Lanj: Preface 1"
---

# Lanj: Preface 1

*Translated from the [original Japanese].*

2014/02/07

**Note from Nias:** This account is ported from an old edition. The changes are in bold.

**2011/07/19.**

When Ridia put her lovely little hand to her mouth, she stepped up to the back of a man like a mischevious girl.
That young man was sitting on top of a short wall made of light brown bricks, looking at the park's wall clock.
It was 8 P.M. She let out a sigh into the summer night.

"Seren..." she murmured in a tiny voice. But not noticing the voice, the young man kept staring at the clock.
Waiting for the man to notice her, the girl counted in her mind, *1, 2, 3...*.
Eventually, when she reached *8* in her mind, she decided to call him again.

And once more she whispered before him, this time in a voice so soft that no one in the world could hear it. "The unseen truth is made up. Seen lies exist here."

---

Just a minute before that. **Shimon Sadakari** was grieving while staring at the clock in the park.

He lamented the lost past, he lamented the gloomy present, and he lamented the future that it[^1] could not see.
When he noticed, he was becoming 30, unlike his former self in his teens. His mind and body changed.
In those days, "the future is yet to be decided" was still a hope. Since when was it instead despair?

He didn't think his life is especially miserable. But something was different. Something was insufficient. Such were his thoughts.
Every time he woke up in the morning, he would end up thinking, "Is there any meaning to this kind of life I live?" Every time he came back in the evening, he would end up thinking, "Will I end up dying with nothing achieved after day after day of idleness?"

If he went to a psychiatrist, would the dull melancholy caused by exhaustion be explained?
Then they would prescribe some medicine that doesn't even work.
As he put on a bitter smile, he reached inside his pocket and searched for the medicine.
Soon, it became 30 minutes after dinner. He had had one past seven at the family restaurant in front of the station, but he went out early because the inside was noisy.

When he took out his hand from the pocket of his suit, a mountain of medicine came out and out. There were several sheets of silver. Truly unfitting for a young man of 30.
That's the way it is, taking heaps of medicine just to preserve his "body without pain" that he had naturally enjoyed in his teens and twenties, right?
These silver sheets would surely grow in number as his age does.

Today was harsh. No; rather, today was harsh *too*. And tomorrow will be gloomier than today.
He has not grown up. Nor has he changed for the better. All he has done was conceding aimlessly.
That is what it means to be an adult. The "title" that he longed for in childhood, when he looks at it in his own hand, amounted to little, nothing more than shackles.

After gulping down water from a plastic bottle with his medicine, he let out a heavy sigh.
He stared at the wall clock. It was already eight.

—He did not even make any conjectures at this time.

From the moment the hour hand pointed to 8, thoughts such as becoming an observer himself of the dream of gods.

On the other hand, Ridia was waiting for the lover there to notice her. But however much she stared at his back, all he did was stare at the clock as if he were in a daze.

"Seren-kun..." whispered Ridia in a soft voice as if she were comforting him. "... At last, I found you."

Suddenly, being greeted, he was surprised. "Ridia...?" That was a familiar voice. Her flax-colored hair flying, she stood still.

She always called him "Seren-kun". This is a name in a conlang he had made called "Arka". While in the beginning she had called him by his official name, now she had switched to "Seren-kun".

This name has a high handed meaning of "one who silences the other person", and from the start, he was called that as his nickname. For that reason, he didn't like it.

However, because he was afraid to becoming used to it, he gradually felt it more natural to be called by his Arka name over his official name. Both of them started with an S, and both of them were equally long when written in katakana. Because of such reasons, now he had accepted this name.

In practice, he used the name "Seren" even when objectively viewing himself in his head, and he even made other people who knew him well call him by that name. His official name was now used only on his identification papers, in his workplace, and in other non-private matters.

"Ridia...? Why are you here?" They didn't have any promises to meet each other, much less today, at this time, in this place.

Unlike Seren, bewildered, she showed off her manner as if she saw through quite everything. "It's been 20 years since we've last met, right? Seren, you must have been thinking of coming here for me." Ridia spread her hands out as if they were wings. "If so, then we've really met. It's amazing." She seemed very happy.

"Yes, but I didn't get in touch at all; I came here alone." Seren made a concerned face.

"It's okay. Because you believed we'd meet each other." She sat down to the left of Seren then rested her head slightly on her shoulder.

"... There are people watching us." Seren noticed his surroundings. An ill-mannered group was hanging out. It would be a bother to get involved. He met the gaze of a man nearby. Being a middle-aged person who seemed to be an office worker, the stranger didn't seem like someone who would come saying something, but Seren, somehow turning uneasy, turned his head down.

"Am I a nuisance to you?" Suddenly, she showed an expression like the night cherry blossoms, tinged with grief.

"That's not it," he responded right away. "... I'm glad we met too."

At those words, she smiled as she were blossoming.

Ridia looked toward the clock in the park. "It's eight o'clock. The same time as when I first called out to you. As well as the same time as I was born. Well, during these 20 years, a lot's happened, hasn't it?"

"Ah, yeah."

"You made the conlang Arka. And I made the conworld Kaldia."

"Then the two together was named Arxidia (幻奏). That is to say, between those 20 years—"

"—We've been playing the illusion, haven't we?"[^2]

"But conversely, even when we've spent so much time on this, only those two could form, you know." Seren looked at something far away.

"There's something else."

"Hmm?"

"... Something important, that's there." Ridia brought her small, pale hand near Seren's. When she touched the tip of her pinky to his hand, Seren, seemingly nervous, became stiff in his body.

"—Me, you, we could fall in love."

"... Fall in love, us?" he said awkwardly, seeming embarrassed.

"Can we... put our hands together?" Ridia asked with a blushed face. Instead of answering, Seren put his hand on top of hers. Without a word between them, their pulses went through each other. A warm warmth.

Soon after the silence, the two said at once, "Thank you." For the rest of their encounter, they looked at each other's faces instinctively.

"... Then, how did we say it like that?"

"Huh?" Ridia, looking humiliated, looked down. "Well, ... as in, thank you for coming to like me..."

"Ah." Seren looked at the sky. "The same with me. Thank you for liking me."

---

"Nevertheless, what I insisted upon and made up to this degree is certainly all a fantasy, right?" murmured Seren, appearing lonely.

No matter how much one insists, a fantasy is a fantasy. No matter how much effort one piles up, fiction is fiction.

He understood that; in fact, he understood it from the beginning. Yet he felt a tinge of loneliness in that.

Whether she knew his feelings or not, Ridia whispered this as she saw him with her jade-colored eyes.

**"Hey, what if the fantasy you sketched from your imagination were real?"**

The perfectly clear and tranquil sigh in the summer night tickled his ears.

But when Seren made a wretched laugh, he did not try to make a response. He knew that he was not young enough, to the extent that he had already become the protagonist of the world of dreams.

"We're characters in the conworld Kaldia," said Ridia. "We are the heroes who lived in the middle era called 'Ordin' and brought peace to the world by defeating demons."

"—In the story we made up, you know."

"No." She shook her head. "That's not it."

Thus – facing the moon, she announced, as if she were making a vow: "That world existed."

She stretched her hand toward the 17-day-old moon floating in the pitch-black sky. Seren silently followed what she was pointing to with his eyes.

"We who had fought demons have reincarnated and left the otherworld Kaldia. Our destination was Yumana – that is, this world."

Seren could not conceal his feeling of perplexity toward Ridia's strange words, given that she was not one to tell jokes often.

"As we were bound to meet, we have met, and in this world as well, we have created Arka and Kaldia once more."

Unlike the calm and composed Ridia, Seren looked sullen. "Frankly, I find it hard to approve. In the old Arka, there were words from English and Japanese. If Arka were from another world, then how would you explain that?"

But Ridia, in a tone as if she had thought of this doubt from the beginning, answered, "In the end, Arka has reached the state it is in now, without any words from natural languages such as English. Now it's completely *a priori*. We were intending to create Arxidia, but in fact, we were unconsciously awakening our memories from before our reincarnation."

With his hand at his chin, Seren pondered. He did not understand the aims of what she said.

"Why do you think there hasn't been any other language in this world that was created to the extent that Arka has?" asked Ridia. "It's not because you've surpassed others in any particular way."

"... Because of this blessing of our memories from before we reincarnated, is it? In other words, after I saved a parallel world, I reincarnated into Earth, and then I spent 20 years on Arka, from 10 years of age to 30."

Ridia nodded with a serious look. She felt worried about how far he would say such things. "Even so, don't question it. Why go through the trouble reviving something made in a parallel universe? Isn't it a waste of effort, doing it twice?"

Ridia suddenly stood up. Her skirt danced in the wind. "Refreshing", she said as she spread out both of her hands. There was a fleeting feeling that it would go flying in the sky as it is.

"You can't bring a language from Earth into a parallel world. Likewise, you can't bring a language from a parallel world into Earth, either. However, you can bring a language from a parallel world into a parallel world. Because a strong reaction against it won't occur."

Seren thought that reminded him of organ transplants a bit. "In other words, if I made Arka on earth, this is just like putting it on Kaldia?"

"Yep. The 'you' that once reincarnated in a parallel world forgot Japanese, and conversely, the 'you' that reincarnated in this world forgot Arka. But this time is different. Because you've already made Arka."

"'This time'? If that's how you say it, then I can even hear the wind telling me to go to Kaldia once more after this," Seren said with a bitter smile. But she easily agreed with his words.

"These 20 years of hardship were spent for us to try to live in *that* world from the start. Because of that, we were able to speak Arka from the start and know the culture and history of the other side in detail."

By her facial expression, she didn't seem to be joking at all. That much sent a chill down his spine.

"It was hard when you first turned up in the Ordin era. You didn't know even one language from Kaldia, and you were even losing your knowledge of Japanese. So this is happening so this time, this doesn't happen. **So above all, the purpose of reviving Arka and Kaldia in Yumana is to make a link between Yumana and Kaldia.**"

Seren sighed. "Sorry, Ridia. Isn't it time to stop joking already? Fantasy is fantasy, and reality is reality. If you have troubles or wishes, then I want to tell you something. If I can do it, then I'll do anything."

When she heard his words showing that he did not try to believe from his head, she grew sad and shut herself in like a morning glory in the night. Although Seren was baffled, he looked at her with concern.

"... Sorry. I didn't mean to call you a liar. It's just that it sounds too good to be true."

"All right... If I hadn't recovered my own memories from before reincarnation, I'd be confused too." But she seemed quite gloomy. Her small, lavish body seemed even thinner than usual.

"Memories from before reincarnation?"

"I remember.... A contract... with the god of death," Ridia muttered, as if she had lost her mind.

"Yeah, um... question, okay?" Seren did not want to see her sad face. Although he was confused, he had decided to continue speaking.

"Yeah?"

"What are we going to a parallel world again for?"

"... It's all going to end."

"End...?"

"In twenty years, the parallel world Kaldia will approach its end."

"Kaldia is ending?" As usual, Seren was astonished by that.

"We resurrected on Mel year 422, in the Lanj era. So twenty years have passed."

"Twenty... years?"

It was the exact same time as Arka was created. Perhaps she wanted to tell him to prevent the destruction of the world during those twenty years.

"So... when is that reincarnation happening?"

She was looking at the clock in the park when Seren asked that. "About five minutes."

"So we only have that long left, right?" Seren opened his eyes wide. Those words were too sudden for him. There wasn't even time to prepare. He was already too old to be able to go anywhere with only his own body. All he had was some household medicine and other various items.

But Ridia shook her head. Her hair, reaching past her shoulders, fluttered softly. "No, it's already been five minutes since our reincarnation. At eight o'clock, our souls—*seles*, that is, went off to the other side."

"Huh..." That it already happened was an idea he did not expect.

"Because earthlings usually have no *seles*, we've now become ordinary human beings." Having said that, she did not feel that she had lost anything in particular.

"Even if the soul travels to the other side, would it have trouble with not having a body?"

"On the other side, you are there with a body but no *seles*. Even among the people of Kaldia, the norm is to not have a *seles*. In one of these bodies, the *seles* will enter."

Apparently, there is a vessel arranged for the soul to enter in the otherworld.

"The 'you' on the other side will come to spend time in the otherworld, so naturally he will speak Arka. The *seles* moving into that body will need to be able to use Arka. Otherwise, the body will reject it."

"I see. So that's why Earth-me revived Arka here."

Calmly, yet, he nodded deeply. "At any rate, I've ended up being made to perform as the protagonist of the myth once more in those years."

"Huh?" she said, not expecting that. "Seren, you're not the main character."

"Is that so?" he said with a half-relieved face. "Then are you the main character?"

But she didn't agree with that either. "The protagonist of this story is neither you nor me. Her name is <hacm>lein melkant</hacm>, also known as <hacm>melkantfian</hacm>."

"Lein Melkant, 'the girl who recites time'...? She's not part of Axet, is she?"

"During the twenty years from Mel 422, she observes the world ending."

"What for?"

"To change the end of the world."

"Change? But she only watches the world, right?"

Ridia showed an enigmatic smile toward Seren, who was confused.

"To observe the world means having the power to change it."

"Hmm... it's a difficult topic."

"Instead of being difficult, is it just hard to accept for you?"

As Ridia said this in a tone poking fun at him, he instantly shut his mouth. This was a perfectly fitting phrase.

"Anyway." Seren made a dry cough. In case something happens to the other side's 'me', will anything happen to me here?" He suddenly showed a sign of unease. But Ridia answered to shake that unease off.

"Nothing will happen. Even if the other 'you' gets hurt, it won't affect you, and vice versa. The important thing isn't the body but rather the *seles*."

"*Seles*... the soul. Reminds me, when you say *seles*, why was it only the soul this time? What could it mean when our bodies remain on Earth?"

"The meaning, is it..." Ridia reached out her long, slender arm. She had a young figure, much like a child.

"Honestly, even if you say that a parallel world is a real thing, it doesn't mean my body will go there, so it's not quite sinking in for me. Even the fact that I once defeated demons in the Ordin era or something, I've forgotten that completely."

To Seren, tilting his neck, Ridia affirmed, "Although your efforts during the Ordin era only spanned from Mel 2 until 22 in Kaldia, that translates to 1991 to 2011 in the Western calendar we use here, right?"

"Ah, but I was on Earth too during that time? I don't have any memory of being in Kaldia."

"In other words, you were resurrected into the Ordin era in 1991, and only your *seles* was resurrected. In our case, we each have two bodies at all times. One on Earth, and one in Kaldia."

"I see..." he nodded. "That means our bodies will stay here on earth this time too. But why are our bodies left here every time?"

"Isn't it strange?"

"You see, usually in parallel world works, the body goes through as well. What in the world does it mean for us to be left on this side?"

Then Ridia brought her cherry-pink lips up to Seren's ear. The faint aroma of peaches tickled the insides of his nose. "I'll tell you, being left on this world, our mission is to—"

—After the moment of silence, he looked impressed and amazed.

"... How could it, well, be the one who thought of this 'manner' so skillful. Truly, if for that method, I can achieve this 'mission'. And even if something happens to me here on the way, the plan will work."

As Ridia giggled like a child, she slowly got further from Seren's ears.

"In addition, under these circumstances, I'll agree to it even though I don't remember what happened in Kaldia." Seren folded his arms and nodded.

"Um, no, you're supposed to remember at least a little. Even though you might not have the memories, because the unconscious engraved onto the *seles* remembers what Kaldia is with a tight grip."

"Is that so?"

"For example, on the days when special things happened during those twenty years, something special would often happen in the Ordin era. The unconsciousness of the *seles* has been made to act similarly to us."

"When I revived Arka on Earth, was that also a part of it..."

"Now, but the contract with the god of death over there is involved in a big way."

"The god of death?"

"Ah, umm..." Ridia hesitated. She tried to change the topic. "Besides, I wonder if drawing, music, things like that are that way too. Seren, do you like neoclassicism? If it's in Kaldia where God exists, then it would be one of the most developed fields there, right?"

"As for my liking to neoclassicism, is it influenced by my memories from the otherworld? And is that true with music too?"

"Earth's, well, the West's twelve-tone scale is typical, but in Kaldia as well, there are the same number of notes because they use the twelve gods of Armiva. In addition, because the physical characteristics that make harmony pleasing to humans is fixed, the structure of harmony will be the same in another world."

"About that, the chords and melodies used wouldn't be completely arbitrary, right? That is, even in Kaldia, they'd use the same chord progressions and melodies."

"Yep. And in any world, there will be an almost countless number of musical compositions. When it comes to simple melodies, it's likely that there will be those that are same in both worlds."

"It looks like reinventing the wheel, where even without any exchange of information, the end result ends up being the same. ... For example, what piece of music is in your mind?"

At that, Ridia giggled with delight. She was a bit strange for today. It might be because her memories of her past life came back to her, or something on top of that, but at any rate, she seemed to be understanding various things that Seren didn't know. Ridia started counting on her fingers as if she were enjoying how Seren answered.

"For example, that famous Japanese candy song. It goes something like "*chokkore—to—*". There's a similar song to that that's the national anthem of Altia. So your ears should be charmed by that.

"And then, Grieg's *Peer Gynt*, 'In the Hall of the Mountain King.' That one is similar to a song called <hacm>non keno hacma</hacm> from the Kako era.

"And the national anthem of Lutia is just like the theme song of the movie adaptation of the most famous fantasy novel born in the United Kingdom. Not to mention the theme song of the second most famous RPG in Japan. These make up the star-song (星歌) of the planet Atolas."

"'Star-song'? Not a national anthem?"

"I really don't remember. On the occasion when we fought demons as Axet, we made allies called *tolvador*, as well as the star-song, the *anxaldia*, that crossed nations."

"That became the song of hope for humankind, and later, it even was adopted as the national anthem of Arbazard.

"—And the one who made that song is, well, you."

Seren opened his eyes wide. "Me!? Why, it was that company that—"

"—Made it, of course in this world. But in Kaldia, you were the one who did. Together with the *hacm* song."

"But look, but even if you say that, there are going to be a lot of problems with copyright and such..."

"Are you declaring that to the other person in the otherworld?"

"... You're right."

Pressing down on her hair fluttering in the cool breeze, Ridia laughed with a *hee hee*. "I think so. Why you're charmed by that game, why you like that candy jingle. It's because you've been charmed by the memories of your soul."

"Indeed..." Looking tired, Seren turned his neck. His shoulders were stiff. He learned too much information that night. Dear me, the past life, the mission, the music, the mechanisms of the story, ....

"Huh, mechanisms...?"

Noticing something suddenly happening, he stared at Ridia. "Look, if you hadn't made me hear the mechanisms of this story, would I have been able to see into these twenty years?"

"Wouldn't you have been left not understanding them?" Ridia easily denied. I think not only you, but everyone else would fail to notice, because the most complex device lurks in the parts where it's easy to see and parts where it's hard to see."

"So... that might be it. Plain tricks appear as decoys—"

As he was trying to say something, Ridia placed a finger to his lips. Then she whispered a prank indeed. "God told a lie in order to lie. This is a tale of such a lonely god. The repentance, of God."

---

Silence arrived.

"Hey," he muttered as if he were recalling something. "A while ago, I told you I'd do anything I could, right?"

"... You did." She had an awkward face. "But those words hold if there are troubles or wishes."

"If it's a request, then it exists."

Swing her legs, she laughed with an innocent face.

"... If you ask, then I'll do it."

"I'll tell you—" She clasped her hands in front of her chest. Her voice seemed higher from being nervous. "—I'll always like you and I want to be with you forever."

Seren was silent for a short while. Those words were more heavy than audible. Whatever they meant, to what extent she would go through troubles and make sacrifices: he was fully aware of that. For this reason, he was unable to respond right away.

Soon, having reflected on the weight of those words for enough, he answered, "I get it."

"I promise, Ridia," he calmly declared.

And then he told Ridia his own wishes, but the wind of the summer night suddenly drowned out the words.

"Uh huh..." nodded Ridia, her cheeks the color of cherry blossoms. "I promise, Seren."

Suddenly, Ridia gazed at the clock in the park with a listless expression. "Soon, the 'us' that reincarnated will arrive at Kaldia." As she murmured, her face turned gloomy.

"What in the world are the reincarnated 'us' going to be doing there?" questioned Seren. But all Ridia gave back was a smile as fragile as baby's-breath. Those eyes appeared as if she knew what was going to happen.

"Is it okay... if we hold hands?" That voice sounded like it could collapse at any moment.

"Yeah..." With her own tiny hand, she clasped his tightly. That hand faintly trembled.

"Why?"

"I'm scared..."

"Scared?"

"You mean, before we were reincarnated, there was no guarantee that you'd love me," said Ridia, seeming as if she were about to burst into tears. "The important thing are the souls... the *seles*. Those *seles* has already gone to the otherworld. From the point of view of the story, they've already left our bodies on Earth.

"I'm not confident about whether the 'you' in Kaldia will choose me again, and that's made me really worried."

"Is that so..." Seren caressed her head without reservation.

In order to answer, she snuggled into his shoulder. "Can I sleep here for a bit?"

"Yeah, it's been a long trip. Feel free to rest a little."

"When I close my eyes, the story of us here will be finished. The souls of our next selves will meet in Kaldia. But there's no guarantee that our relationship there will be maintained..." As Ridia wore an anxious face, she held his hand even more tightly. "Are you going to keep holding my hand?"

"... Ah, I'm not going to let go," Seren asserted. "Even if we leave for the next world."

Encouraged by those words, Ridia looked relieved, and shut her eyes happily. "At this rate, when the dream descends, the first one I'll meet on the other side will surely be you, Seren."

[original Japanese]: http://conlinguistics.org/blog/?p=47

[^1]: In the original text, this phrase is given as "**底**の見えない未来を憂いていた", which literally translates to "laments the future that the **bottom** cannot see", but since 底 is read as そこ, the same as the word for "that place", this might be a typographical error.
[^2]: 「――わたしたちは幻を奏でていたんだね」
