---
title: "Lanj: Preface 2"
---

# Lanj: Preface 2

*Translated from the [original Japanese].*

2014/02/07

**Note from Nias:** This account is ported from an old edition.

**The Girl who Recites Time**

The world said that it didn't need me.

I said that I didn't need that sort of world.

---

When classes were over, I quietly exited the school alone.
That doesn't mean I talk to anyone. I am the only one walking on the way back.
This is routine for me, and it's the natural thing to do.
It's not that the world is rejecting me. It's just that I'm rejecting the world.

As I am walking for a moment carrying my bag, I enter a small side road.
This is the forgotten path that everyone can see, but no one notices.
A cat, the wind, and I are the only ones who go along the quiet dead end.
I greet the familiar white cat basking in the sun, then advance deeper.

At the end of the road, there is an old-looking, brown antique store.
Across the window panes are numerous antiques.
I have never seen anything be sold there.
I'm always the only guest there.

When I opened the door, it creaked.
I go inside. The door slams shut.

"Hello," I shouted inside the store. It was a bit hoarse. Speaking of which, this is the first time today that I've said something there. No, today too, is it?

"Welcome." The shopkeeper, sitting at the counter inside, raised her eyes.[^1] As soon as she noticed me, she left his seat. "What's your mood today?" She always me some tea, in the flavor I request.

"Jasmine, please."

"Jasmine, is it?"

As tea leaves are put into the teapot, they make a beautiful sound like the rustling of trees. As usual, I take a seat at the chair in front of the counter and put my bag on the floor. The store is full of shelves, on which there are clocks, tableware, toys, dolls, lamps, music boxes, books, and various other objects. Like a tree that has set all of its roots, they don't move from their usual place at all.

"You're welcome." She puts the jasmine tea on the counter.

"Thank you." There are two cups, the usual cups. The only tableware used in the store.

"Are you reciting today too?" The shopkeeper rises up and looks at a small door at the back-left corner of the store.

"Of course." As I take a sip from the tea, the fragrance of jasmine fills the inside of my mouth.

"By the way, you seem different from usual."

"Is it so?"

"Did something happen?"

I look upwards. "Oh, now that you mention it, it's my birthday."

"Well, congratulations," smiles the shopkeeper. "How old did you turn?"

"I'm 14. But why should I so happy about that? It just means I'm aging."

The shopkeeper took a sip from her tea and answered, "Isn't it still fine to congratulate you at this age?"

"Well, then when I age, my birthdays won't be so enjoyable, right."

"Couldn't you say that it's congratulating the fact that you survived to that point?"

"Maybe you'd say it's a sorrow to survive through a life of distress."

"In that case, I'd say that means 'bless your strength to stick it out'."

Hearing this, I smiled and got up from my seat. "You're a magician of words, madam."

"It's not what you say, it's how you say it."

"And thank you for the tea."

I look toward the small door in the back-left corner. "You're admirable every day without rest."

"That's because you're the girl who recites time." She approaches the door and clutches the round knob. Some of the bronze coat has worn off, leaving the iron visible.

"The one who can open this door, strangely enough, is you and you alone, Lein Melkant," the shopkeeper called me so. Whenever or whoever it was, the fact is that this person called me that. From now on, I would go by this name.

When I opened the door, it was quite another world. Crystals that were blue in the center fill the space, large and small ones forming a crowded pile. I can pass through only a small room, but because of the light dispersing, I can feel it terribly well all around the place. Even though there was no light in the room, the crystals themselves emit a faint glow.

At first glance, the crystals seem to be scattered in disarray, but I feel that they are arranged in perfect order. Every crystal is lined up precisely according to chronological order. When it comes to consistency, there is nothing to be seen; they are things to be discovered. I can find order in midst of this chaos. However, it seems that I'm the only one with this ability. Thus, only I can "recite the memories of the world" from these crystals. By touching one of these crystals, I can become aware of events that happened in the past. Because of that, the shopkeeper called me that name.

—"The girl who recites time."

"Should I close the door?" shouted the shopkeeper from across the entrance.

"Please."

"Then recite time in peace."

She closed the door without a sound. I have a feeling that this room is the only place cut off from the world.

"Well, for today, *eeny meeny miny moe.* Should I continue reciting from yesterday, or should I recite another story?" The time given to me is limited. I have school on weekdays, and on my days off, I'm busy in one way or another. The only time I can use freely is the time inside the cracks of my schedule, like when I return from school. Using this time, I would recite time every day.

—Until the end of the world.

Whenever it was, someone said something: the world would end in twenty years. I am the girl who can observe the world, and the only one with the ability. That is, by the way of the crystals of memories, haphazardly scattered across this small room.

Nevertheless, since when have I been reciting time? I recall that it's been since a very long time ago. From then on, I have been reciting the memories of the world every day. That is my role as the girl who recites time.

As soon as I leave the room, I'll return to being nothing but an ordinary child. But when I'm inside here, I am made to be a human in charge of a duty. This duty of mine is to observe the demise of the world. Strictly speaking, to be aware of why the world fell into its demise. Every day, I will observe the world that will end on the twentieth year, counting from the first, in order to find the cause of its end.

The history of the world is quite long. But the source of its death is hidden within twenty years in Ordin and twenty years in Lanj. That's what I've been told. Whenever it happened and who did it, that's what I've been told. Therefore, I'll spend every day for twenty years observing the affairs of two different eras. Using the crystals of memories that shine blue, I will recite these two overlapping eras at the same time. And every time I find an event worthy of mentioning, I will record it. Thus, I do this in order to find the cause of the end of the world.

There are four hundred years between the two eras. Obviously, the language has changed during that time. In addition to summarizing the information I observed, I will translate it into the modern language. Although there are various language that can be described as "modern languages", the four that I will translate into are Arka, the official language of Arbazard, Arbaren, Tiaren (the language of Lutia) and Altiaren (Nagili). What are in these old languages I will fix and translate into modern language. For example, I would translate Old Siiziaren and Middle Tiaren into modern Tiaren. In addition, as a general rule, Middle Arbaren will be translated not into modern Arbaren, but rather into modern Arka. There are cases when modern Arbaren is used for regional records and such. Also, there are places where records in divine languages such as Filveeyu have been presented as such when necessary. Because I include the summary and the translations along with the observations, the task of reciting time is more work that one would think.

The memory crystals increase in number day by day, until the day when the world ends. A day of memories takes up only a tiny crystal. As they build up, it becomes a full crystal. Because crystals are born every day, I can even recite the things that happened today. Only the future is beyond observation. However the world ends, I won't know until its last day arrives.

In any case, I will observe the world day by day during my free time for the next twenty years. Even if something happens, I will only look over the events. An observer does not meddle in them. All they can do is to examine them silently, being able to sense even the tiniest existence.

But *perhaps* that carries the power to change the world, because the observer effect takes effect on the world.

For example, in the physical world, one needs to hit light onto an electron in order to observe it. Without doing so, nothing can be seen. But when an electron hits a photon, its trajectory is changed. In short, the act of seeing something itself changes what is seen. This is the observer effect. Not only is it seen in physics, but it is also seen in sociology. It is a versatile concept.

One can say the same about the world. As I, the girl who recites time, observe the world, I cause the observer effect to arise. That is, how I observe the world might affect the way the world ends. The end of the world is in the hands of its observer. In my palms. ... That's what I learned. But, who was it...?

"I wonder how the world will end..." I murmured, without another person, in this lonely room.

"... Is that somehow significant?" Suddenly, I am reminded of these words the shopkeeper revealed to me once.

Beginning is end.

End is beginning.

As I shake my head calmly, I pick up a blue crystal that tumbled on the floor. "Let's do it with this one today." After how many days will time be left behind by me? What is waiting at the edge of the world's end?

Squinting at the light blue light shot out by the crystal, I whispered, "The world said that it didn't need me.... I—"

[original Japanese]: conlinguistics.org/blog/?p=50

[^1]: The shopkeeper's gender is never clearly revealed in this section.
