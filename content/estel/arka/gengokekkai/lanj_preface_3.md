---
title: "Lanj: Preface 3"
---

# Lanj: Preface 3

*Translated from the [original Japanese].*

2014/02/07

**Note from Nias:** This account is ported from an old edition.

**Scale of the Death God**

2011/7/19.

That place was nothing but blank space. In this place was nothing, to the extent of driving one mad, except for an endless expanse of white. Seren had been sent to that place. He had collapsed, perhaps into a coma.

The next instant, a man in black clothes appeared standing in front of him. "O person who travels through the gates of the underworld," greets the man, waking Seren up. Although he was bewildered by the unfamiliar place and person, he immediately realizes that he has reincarnated. It was supposed to happen at eight o'clock, but he remembers what happened up to the point where he and Ridia held hands and closed their eyes. Perhaps his *seles* has not been detached from Yumana.

The moment he remembered what happened in the park, he blurted out in a loud voice, "—Ridia!?" Ridia, whom he should have been holding hands with, is nowhere to be seen. In an instant, he felt himself being crushed by the darkness of anxiety.

"O person who travels through the gates of the underworld," called out the man in black once again. As Seren looked up at the man, he tried to ask whether he had not seen his comrade, but before he could open his mouth, the man produced a scythe from the blank space and pointed himself in front of Seren. At that moment, the white space suddenly rustled. "My name is the death god Vangardi. I am the one who rules over hell, and I govern contracts and offering-pots[^1]. O person who travels through the gates of the underworld, show me thy offering-pot."

That voice of the young and beautiful man was mumbled yet lingers in one's ears. Seren searches in his head for a description in the Arka-Japanese Dictionary—the dictionary of the conlang Arka which he had completed himself. Vangardi, also known as Alatia, is the king of hell. He rules contracts and offering-pots. Whenever someone conveys their wish to him, he grants them their wish in exchange for something. Whatever amounts to compensation is called the "offering-pot", or <hacm>esto</hacm> in Arka.

Although translated as "offering-pot", it is actually a container called <hacm>esto</hacm>, yet there is another meaning to that word. It also means "compensation", and at the same time, makes the wish come true in exchange. In other words, the offering-pot is "compensation" and "fulfillment" at the same time. In this, one can notice an equivalent exchange in the contract with Vangardi.

At any rate, why would Vangardi turn up at this stage? Perhaps he is offering to make a contract on something.

Seren feels a slight itch from inside his head. He had a feeling that Ridia explained it a moment ago, but now, when he tries to remember, his head itches. Although he has a hunch that Vangardi's name popped up somewhere in her explanations, he can't remember it no matter what. Perhaps it was just before his *seles* was detached from Yumana.

"Do I need compensation to go on to Kaldia...?" asked Seren. The man's long, black hair swung weakly as he quietly nodded. Ah, that's it. A nod means yes, even in Kaldia. Seren was impressed in a strange way. "Isn't it enough just to forget Japanese?"

To Seren's questions, the man answers without disturbing his tone of voice. "That is but the circumstances of Bert and Meltia. I shall not allow thee to bring thy *seles* as it is, sullied by Yumana."

"A sullied soul..."

"As a condition before thou payest thy offering-pot, the majority of thy memories will be lost, because the body that thou wilt enter will be thy body that hath spent time in Kaldia with thirty years of memories."

That was an understandable thing to say. He would not be able to avoid the chaos if the memories of two people who had lived separate lives entered one body. Since he was going to cross into Kaldia, it would only be reasonable to have his memories from the Yumana side erased. Moreover, Kaldia and Yumana are different worlds, so he should not take information from Yumana into Kaldia as a prank. Indeed, these are the terms typical of a death god who manages souls.

But what he was worried about was this offering-pot thing. Holding his breath, Seren asks, "And any information about that offering-pot...?"

Fitting for the king of hell, the man demanded a cruel offering-pot in response.

"The one thou shalt place in the offering-pot is the one thou lovest. Once thou passest the gates of the underworld, thou shalt lose the one thou lovest."

"... What a request."

As if he saw right through it, the death god tells Seren, who appears puzzled, "The most wicked interpretation that thou couldst fear is the answer."

"In other words... myself after reincarnation being a total stranger to Ridia?"

The death god nods silently.

At this moment, he breaks into a cold sweat. "To put it another way, herself after reincarnation won't like me, or even know me in the first place, right?"

"—Affirmative."

Suddenly, he understood that he had lost his hope.

"No way.... Then what the heck am I going to..."

His legs stagger. He became unsteady, as if the waves carried off the sand at his feet. The Ridia on the other side would know nothing about him. A hopeless situation. Here, he does not understand: for what in the world did they hold hands and depart for the otherworld together?

—No, wait.

Having felt down, he has an idea hit him. Perhaps the Ridia on the other side surely wouldn't know him. But wouldn't his other self still know Ridia? If either one of them came out without losing the thought of the other, it would be possible to do it all again.

Thinking this, he shouts, "I won't forget about Ridia, will I?"

But the death god coldly asserted, "She will also pay the offering-pot. Therefore, she hath decided that thou wilt lose this thought."

At that verdict, Seren saw his hopes crumble apart. "So when I go to the other side, I'll have forgotten about Ridia..."

After standing still for a short while, he dropped his shoulders from the disappointment and muttered, "Ridia won't know me, and I won't know Ridia. Even if we want to meet again in the otherworld, we'd be complete strangers. Now I see...?"

The death god asserted, mercilessly, even.

"No way... that's too much. All this, what the heck is it all for..."

Pressing down on his forehead, and not changing his tone of voice, the death god told Seren, "The size of the offering-pot is proportional to the size of the wish. This was true at the time of contract several decades ago.

"Thou who hast slain demons in the Ordin era and resurrected in Yumana hast wished to meet thy company again in Yumana to rebuild Arka, and for thy compensation thou shalt give up the joy of being average."

"The joy of being average...?"

"Whether or not thy body hath nothing to mind. For instance, the health of thy flesh and soul, the harmony of thy house, the solidity of thy future, —"

"—That's enough." He shakes his head in small movements. Vangardi's words weigh heavily on his heart. Before he could know it, the various events of his past flash across like a revolving lantern.

"But I met Ridia and fell in love with her, as far as to lose this joy of being average. Isn't it unreasonable to take away these thoughts of mine at this point in time?" His voice was filled with emotions.

But the death god diligently maintained an impersonal demeanor. "If thou stayest in Yumana, then thou needest not pay the offering-pot a second time. As one who hath finished a conlang of a new form for the first time, thou mayest live, mourning the compensation of 'the joy of being average' that thou hast lost.

"However, if thou crossest the gates of the underworld again, I shall demand an offering-pot from thee."

Seren ponders, his hand against his forehead. "If I stay in Yumana, then everything will be the same as usual... Ridia, my friends, Arka, ..." he mutters.

"If thou stayest in Yumana, then all would return to usual. But even if thou desertest Arka at this hour, I reckon not that thou couldst regain the joy of being average."

"..."

The death god continues as if he were pouring salt over Seren's wounds. "There will be no bright future for thee as thou flailest about. There will be no days without pain. There will be no days without suffering. There will be no days where thy emptiness is filled—"

"I get it!" Seren shook off the death god's scythe with his right arm. "I know, Vangardi!"

"..." As the scythe dropped to the ground, the death god quietly makes up for his words.

"... A contract with a death god is such a serious thing, isn't it."

Seren let out a deep sigh. "So even if I return to Yumana and throw away Arka, it'll be hell for me. On the other hand, if I cross over to the otherworld, I'll still have to pay a price. It seems that just living anywhere is going to be difficult for me." Suddenly, Seren seemed as if he had noticed something.

"... Look, Vangardi the death god. Haven't I met you a few times in the past? The summer when I was ten, the autumn when I was thirteen, the chilly night when I was seventeen, and then six years ago.

"At those times, I made strong wishes. When I aim to make great sacrifices, I mean I want to fulfill my mission or grant wishes."

But the death god did not attempt to answer. Nonetheless, he did not refute his words.

"Since then, I've made a lot of effort. But every time I made hard struggles in order to grant wishes, some of my happiness escaped me like water spilling from my palm.

"Instead of being rewarded for my efforts, I lost as much as I've worked."

"That is the compensation that thou hast paid. Each time thou facest thy wishes, thou losest thy happiness according to the contract."

Seren lets out another sigh, a gloomy one. "At any rate, the next-offering pot is my loved-one, isn't it? Say what you like, but that's too great, losing so much just by crossing over to the otherworld."

Vangardi showed a bewildered expression for a moment towards the reluctant Seren, and reluctant to confirm the contract, added, "This is a just offering-pot to balance against your wishes. You two will influence the history of Kaldia. And at the end, you have wished to become—" Vangardi suddenly paused his speech there and looked at the white sky behind. "—The observer of memories, was it not? How impudent. The observation point is... the bamboo basket of Arkadia."

When the death god mows down the space with his scythe, it becomes distorted and the records become disordered.

The girl reciting the crystal makes a puzzled face.

In the scene in which the succeeding records are tied together, Vangardi had already finalized Seren's contract.

The girl that recites time scowls.

Seren's memories of Yumana have already grown fuzzy. Having listened to Vangardi's explanations, he understood the task and their own wishes once more this time.

"—Surely a befitting offering-pot."

Seren nods, appearing to accept the terms and change completely from what he was before. He has finally sorted out his affairs with the death god and appears relieved.

"But what a merciless scale balance it was. That offering-pot itself's got in the way of our wishes."

"Your wishes were big to begin with. Contracts are never a sweet thing. That is what thou hast known firsthand for decades."

"It is... that way, isn't it?" he agrees with a sour face.

"But wilt thou pass through the gates of the underworld?"

After spending a moment without a word, Seren nodded.

Thus Vangardi made a black gate appear in the blank space.

Seren slowly rises up and lays his hands on the gate.

"—Thou canst not turn back from this," said the death god coldly. "Across the gate, she will know thee not and love thee not. I shall not mind, even if thou regretest it."

Seren gripped the gatepost tightly.

"... I've always been regretting things. I don't think there's any way to avoid regret completely. Choose one path of life and you pay the price of the other."

As he turns towards Vangardi, Seren confronts him. "I get it at last; this story is my *raison d'être*. This is a struggle with you.

"You said you can't get back the offering-pot you've lost. I guess the contract is a fair exchange."

"Affirmative."

"If so, then I leave my offering-pot to you. But some day, I'll surely show you how I'll get it back."

"It is futile. Since thou hast wished for something, thou needest a corresponding compensation. What wouldst thou offer as a new offering-pot?"

Seren replied immediately, "It's settled. I'll make a tiny human that looks like myself. —This is going to be hard."

The death god makes an ironic smile. "Even if thou losest thy happiness until it adds up many times?"

"You don't think so? Whether it's just because I haven't worked hard enough for my wish until now, or whether I can meet the price if I let it add up?

"That's why I'll keep doing it, even if I lose something or I suffer, until I reach the height of my wishes."

Hearing those words, Vangardi paused for a moment. Amazed, he said, "Wilt thou suffer so much and not give up? This is a joke of a madman."

"Laugh all you want. So far, everyone else has laughed at me too. But I won't laugh at myself. If a person laughs at me, then I'll use that time to go up and do it."

The words resound across the space of nothingness. With his lips slightly open, Vangardi looked at him. "... That kind of person is said to be a pioneer. Indeed, it is worthwhile to leave one's name in history. Interesting. Thou art a *seles* with many results to reap. Contrary to expectations, even thy self in Yumana might have completed his task entirely," he said, trying not to laugh. "According to my conjecture, thou hast met me several times. And even decades ago, thou hast drawn the same conclusions as now. Suppose that thou fulfillest the task with hard work. Then that price is paid by the effort, and thou shalt surely regain thy offering-pot... that is."

"And then you'll pay back."

"Although I know not of the hardship that bears neither pain nor anguish, I cannot stand hearing the resolutions that thou sing'st—. Then according to the contract from before reincarnation, I have continued to steal thy happiness from thee and gift thee agony. In spite of this, the conclusion that thou hast drawn is the same as last time."

"Is it...?"

Vangardi stashed his scythe into another dimension. "Now thy preparedness is worthy of hearing. But thou hast one misunderstanding. This is not a fight between thee and me. This is a fight between thee and thyself."

"... How ironic," Seren shakes his head with an exhausted face. When it comes to myself, I'm so weak when my friends are around, but when my enemies are around, I'm stronger than anything else."

Seren put one leg through the gate. At his back, the death god raises his voice.

"Her wish is greater than thine, and thus she hath paid a greater price than thou." Those words could not have come from the death god's script. "It will not have ended after thou losest thy wishes. She will bear the fragments along with her flesh and soul."

"... Is it?" mutters Seren, who seems lonely.

"In other words, it appears as if her wishes toward thee will grow stronger." For the first time, Vangardi's voice had a faint warmth to it. "Should she take back her part of the offering-pot, ordinary effort is unlikely to suffice for thee. ... Even so, thou wilt proceed, is that correct?"

As Seren turned his head, he spoke as if his words were a vow, "<hacm>fiina ridia</hacm>"

[original Japanese]: http://conlinguistics.org/blog/?p=52

[^1]: In the original text, this was called "鼎", a term used as the translation of Arka <hacm>esto</hacm>, a type of pot used for votive offerings.
