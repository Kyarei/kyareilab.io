---
title: "Appendix: novels, short works, and diaries"
---

# Appendix: novels, short works, and diaries

*Translated from the [original Japanese].*

2014/01/29

By Nias

[Appendix] (in Japanese)

Now, materials that were not officially recorded have been collected and saved.

**In particular, please offer very old and very new data (that have not been published on the Arka atwiki) if you have them.**

Right now, pages for novels, short works, and diaries have been created.

Materials and information about them will be added there from now on.

---

Below is a list of works that have been published for the first time.

The first-person novels and private documents are almost entirely unrelated to conlangs.

* Novels: *Book of Seren* (セレンの書)
* Diaries: *Idler's News Flash* (暇人速報)

Please take caution as the works might contain extreme or inappropriate expressions.

[original Japanese]: http://conlinguistics.org/blog/?p=30
[Appendix]: http://conlang.echo.jp/files/
