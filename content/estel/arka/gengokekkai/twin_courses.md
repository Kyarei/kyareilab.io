---
title: "Twin courses"
---

# Twin courses

*Translated from the [original Japanese].*

2014/03/01

**2/17.**

Could you send me the wiki article for "veteranarian", as well as a summary of what to do to become one?

I'm interested in how you should become one because Yult said that he wanted to be one.

I'd prefer to get information for Japan.

Also, I would be delighted if you sent me a French version of the wiki article.

**2/26.**

*Seere* for "veteranarian"!

Because my daughter said, "If Yuu-chan becomes a veteranarian, then I should become an animal nurse," I said, "What about drug discovery since you're so smart? I wonder if drugs for animals would work on humans, too." She answered, "I'll do it," but for drug discovery, is the course from the college of pharmacy to a pharmaceutical lab?

About discovering drugs for animals, I'd like for you to look up what kind of course or study would be appropriate, even though it might be a bother to you.

I'd prefer to get information for Japan.

When I'm released from prison, my daughter will be eighteen; if she is in Japan at that time, she'll be a university student, and I was worried that I wouldn't be able to completely support her at that time.

Could you please post this exchange and the previous one relating to my children on a BBS?

Although I wish I could see it for myself.

[original Japanese]: http://conlinguistics.org/blog/?p=130
