---
title: "Fantasy Theory"
---

# Fantasy Theory

*Translated from the [original Japanese] on 2021-12-30.*

1. lalala.
{:toc}

Lein: This time, we're having a symposium about fantasy theory, but since this is a very hard story, I think you'll have the patience to read this through.

## Arxidia and real fantasy

Alia: First of all, let's start with some terminology. The conlang *Arka* and the conworld *Kaldia*. The result of combining these two is called *Arxidia*.

Shion: Since the language is one part of the world, wouldn't Arka be included within Kaldia? Then isn't it weird that the two are put together to be called Arxidia?

Arxe: It takes a huge amount of effort to create a language and about the same amount of effort to create a world, so the two are considered as equals. So the two combined are called Arxidia.

Shion: I see. Even though this website is called "Conlang Arka", it also explains the conworld Kaldia, so in practice, its theme is "Arxidia". Then "Arxidia" means "playing the illusion (like a musical instrument)", doesn't it? In short, is it just one type of fantasy?

Arxe: Ah, that's right. By the way, when you speak of fantasy, there are various kinds, isn't that right? Whether it's something fleshed out in terrific detail, or something absurd that a little kid could've drawn. Also, a work's setting can be classified into whether it's in the real world or a fictional one.

Lein: Things set in reality are called *low fantasy*. On the other hand, things set in a fictional world are called *high fantasy*. So in the case of our Arxidia, it belongs to high fantasy, right? Because the planet Atolas and the conworld Kaldia are an *isekai* story.

Alia: On one hand, works with a world appearing to be created with great detail are called *hard fantasy*, like Tolkien's *Lord of the Rings*. On the other hand, works with a world that wasn't created that thoroughly are called *light fantasy*. As a matter of fact, most light novels are classified as such. Arxidia is classified as hard fantasy. That means that it's classified as both high fantasy and hard fantasy.

Shion: For some reason, when you call light novels light, I feel a bit vexed. Surely by the name of it, it feels like they're looked down on. But I like light novels a lot. There are a lot of them that are interesting and move me emotionally.

Lein: Uh-huh, light novels, they're intriguing. They're sold a lot in Arbazard, too. Light and low are just classifications; that doesn't say anything about whether they're good or bad. Since hard and high fantasy settings are hard to understand, don't you agree that the reader can't read them comfortably? So for some people, light is better because it's more accessible.

Arxe: In real life, light sells better than hard. If anything, Arxidia is the one that should be ashamed.

Alia: Though from the point of view of the effort taken on the work, it's a hard truth that hard and high fantasy is the most exhausting to do. In the case of the light novel industry, you've gotta publish a certain number of books each year, and the authors end up not having the time to write hard- and high-fantasy works from the deadlines.

Shion: Hmm. Anyhow, Arxidia (Arka + Kaldia) is classified as hard and high fantasy, right? By the way, when I look at Arka-related sites, I see something advertised as a "real fantasy novel", but what's that? Look, it's even written on *Book of Shion*, isn't it?

Lein: That term refers to fantasy that elevates reality to its extreme beyond hard and high fantasy, and it seems that the creator of Arka is advocating for it.

Shion: Elevating reality to its extreme, is it? In other words, one step short of reality?

## 0.999...9 = real fantasy!?

Arxe: This is where fantasy theory gets difficult. The creator of Arka defines *real fatnasy* as follows: 0.999...9 = real fantasy.

Shion: I suddenly don't get it, why an equation LOL?

Alia: It is, right LOL? By the way, Shion, which do you think is bigger: 0.9 or 1?

Shion: But it's definitely 1 LOL.

Arxe: Then what about 0.999... and 1?

Shion: By 0.999..., you mean that there are an infinite number of 9s after the 0.999, right? Since 0.999... is a tiny bit smaller, 1 is still bigger.

Lein: Shion, 0.999... is actually equal to 1. 0.999... = 1

Shion: Heh, why?

Lein: The 9s in 0.999... go on forever, right? Then what number does this approach?

Shion: It approaches 1, I guess. So to speak, 1 is the destination of 0.999..., isn't it?

Arxe: That's right. And from the point of view of math, that destination can be shown with an equal sign. This can be shown with the limit symbol that's called "lim" on Earth.

Shion: Saying you can show the goal with the equal sign, I mean that's not very math-y; it seems a bit careless to me. I can't understand it yet because I have a strong sense of suspicion.

Lein: Well, then, what do you think of this? First, let's call 0.999... *c*. That is, "*c* = 0.999...". Then how about we multiply both sides by 10?

Shion: That would give "10*c* = 9.999...", right?

Lein: Now try subtracting both sides by *c*. That's "10*c* - *c* = 9.999... - *c*", or in other words, "9*c* = 9.999... - *c*". Since *c* is 0.999..., the right side becomes "9.999... - 0.999...", which is exactly 9. So in other words, the right side becomes 9. By the way, what was the left side?

Shion: The left side was 9*c*. On the other hand, the right side is 9, so "9*c* = 9". Therefore, "*c* = 1".

Lein: Surely we defined "*c* = 0.999...", right? Then "*c* = 1", or in other words?

Shion: "1 = 0.999..."!? Wow, it's for real! Just as I'd expect from you, Lein, that was easy to understand!

Shion: —what math lesson was this LOL? But it's fantasy theory, right? But I think most of the readers have already pressed the back button in their browser (sweat). Whatever the heck about this has to do with fantasy theory, I want to ask the creator of Arka.

Alia: I get how you feel. But hang in there for another moment. Out of 0.999...9 and 1, which is bigger?

Shion: There are a lot of 9s in 0.999...9, but eventually the 9s end, right? So it's not equal to 1. That is, 0.999...9 is *slightly* smaller than 1. ... Right?

Lein: Yep, that's right. You've got it well. 0.999...9 is extremely close to 1, but it's not 1.

Arxe: Well, let's assume that the value of reality is 1. On the other hand, let the completely absurd fantasy be 0. Then let *x* be the amount of reality held in a fantasy, so the domain of *x* is 0 ≤ *x* < 1. What comes next?

Shion: Something? It's just a suspicion, but why think of fantasy as math formulas?

Arxe: "A well-polished fantasy", "a real parallel universe that's been perfected in detail", "an elaborate fantasy", "an expedient story", "a crudely-made work". These sort of vague expressions can't capture the accuracy of a fantasy in specific, can they?

Shion: Oh, I see, I finally get the point. We're dealing with fantasy quantitatively since words are fuzzy things. To be concise, that means we want to call a work at about *x* = 0.5 a light fantasy and one at about *x* = 0.8 a hard fantasy, right?

Alia: That's the feeling, isn't it? Then real fantasy refers to things that, among hard fantasy, get extremely close to 1. So we reach the equation "0.999... = real fantasy".

Shion: I get it now; I can see what this means when it was just gobbledygook at the beginning. You three are pretty smart, being from Arna University, right?

Arxe: I'll add something to this. A moment ago, didn't Shion think, "words are ambiguous and we want to deal with fantasy quantitatively, so it was explained with a formula"? That's definitely correct, but I think it's the language of math. It's the most widely-spread conlang in humanity. The language of math is specialized to understand everything logically. For this reason, it was used to construct fantasy theory.

Shion: Using the language of math was meant to make it precise, right? It pretended that it's a formula, and as a matter of fact, it explained itself in a mathematical conlang?

## Minimizing the fantasy factor *δ*

Arxe: Arxidia is fitting to be "0.999...9" because it's a real fantasy. Then it's only slightly smaller than 1. The difference between 1 and the *x* above is called *δ*, and *δ* means the fantasy factor with regards to works. As this *δ* gets smaller, that fantasy starts having more reality.

Shion: ... What does *δ* mean for Arxidia, then, I wonder. Arxidia's *δ* is the difference between 1 and 0.999...9, right? Then that's an extremely small number.

Alia: For Arxidia, the *δ* is <hacm>viid</hacm>. In our world, there exists an energy called *viid* that doesn't exist on Earth. That's magical energy, and it's the energy that deduces the existence of demons and death gods.

Shion: Deduces.... Umm, in other words, you mean "there's an energy called *viid* in our world, but in all other aspects, it's identical to Earth"?

Arxe: That's right. And in this case, you can say that *δ* is an axiom of a fantasy to put in in other words.

Lein: By the way, when you say "axiom", it's something like a "condition" that you say "first of all, let's suppose this is true for this case" to.

Shion: Assuming that something is true right away without any proof, isn't that really wild? I feel that the existence of an axiom itself is just a way to cut on time.

Alia: Nice point. Don't you think this for this reason? If we want to thoroughly exclude any double standards, wouldn't it have to become a real fantasy that's thoroughly made in terrific detail? That is—**as if you hallucinated that this fictional world is completely real.**

Shion: — Aah! Is that right... that's the driving force behind everything, isn't it?

Arxe: For Arxidia, the *δ* is the single detail of "the existence of *viid*". Aside from that, there are no other axioms adopted. Arxidia is a real fantasy fit to be called "0.999...9" for this reason.

Shion: I see.... Now I get the meaning of developing the theory mathematically. Huh? ... By the way, what if we didn't adopt any axioms at all and made *δ* zero? I think that would be even more realistic.

Alia: That would simply be "reality", *x* = 1. It exceeds the domain of fantasy.

Shion: Oh, that's it. Wah! Speaking of which, the creator is an absolute oddball! I don't know why he'd think of fantasy using equations.

Arxe: I've got no answers LOL

## The fingers of the Arxidian fiddles with the first domino

Shion: In any case, thanks to *viid*, there are gods in Arxidia and magic as well. And there are demons and death gods too. Surely you all descend from the gods, right? A species falsely similar to the humans of Earth? Although in the beginning, there was only one hypothesis of *viid*, the planet Atolas has become very different from Earth, hasn't it?

Alia: That's because all things are controlled by the law of cause and effect. If the "cause" in the beginning is changed, then everything else naturally changes in a chain of causes and effects like dominoes falling over. As a result, Atolas ends up differing from Earth. If you change the angle at which the first domino falls over slightly, then the two worlds each advance in their own paths, gradually diverging.

Arxe: The creator of the world did only one thing; they simply fiddled with the first domino. As a result of that, every progression of cause and effect that followed fell over in a different way. The Arxidian touched only the first domino. It's quite like mathematical induction, isn't it?

Shion: Just by adding a single factor to it, a lot of differences arise from that, isn't that right...!?

Lein: Nonetheless, because the *δ* of whether *viid* exists or not is the only difference between Earth and Atolas, they're similar in the fundamental aspects. For example, as humans from Atolas, our bodies were built in the same way as yours, so if you and Arxe married, you could have a child.

Shion: You mean that there are things that are similar between them and things that aren't, right? But I wonder if you can deduce all the fantastic phenomena like magic from the one thing called *viid*.

Arxe: That's very true. Well then, why don't we try reading the entry for <hacm>dolmiyu</hacm> that talks about *viid* in the Arka-Japanese dictionary? You should be able to understand how the fantastic side of Arxidia was deduced from *viid*.

Shion: Wow... this is a magic textbook?! Just in a single entry, there's something as long as a short story or a thesis....

Alia: This is nicknamed "the final boss of the dictionary, <hacm>dolmiyu</hacm>" LOL

Shion: Ahaha.... Seems that *viid* is truly the root of everything fantastical. Anyway, real fantasy seems like a lot of pain to make, because you can't let in a single thing out of convenience.

Alia: Right, isn't it? The point is whether you can define *δ* to be something versatile and expand it into various theories of fantasy.

Shion: It's very difficult to understand the fact that theories of fantasy can extend within the limits of a single axiom. Hmm..., but wait. It's okay to have its environment, outside of that one axiom, the same as Earth's, right? If it is, then wouldn't the creation of the real part of real fantasy be easy? And since the percentage of the real part of real fantasy is higher than that of other fantasy, wouldn't it be even more simple to make?

Arxe: If that world were *a posteriori*, then it would indeed be okay to rip off the setting from Earth. In that case, making the parts unrelated to the axiom would be a walk in the park. But the conworld in a real fantasy is *a priori*. Even if its setting resembles Earth in the end, you have to construct the world from zero for the moment. Without taking a single piece of culture or a single word. I'll show you two good examples of how Arka's words are made in this way while taking into account the culture.

→ The procedure of drafting an article (NYI) · Considerations before creating a word (NYI)

Shion: Wow, this is really fussy.... Earth and Atolas both ended up having a word for camphor because camphor existed in both of them. But in the *a priori* conworld, the existence of camphor was examined and constructed from zero. At the same time, there are things that just aren't there in the parallel world, such as the dice calendar. In Atolas with computers and cell phones, I didn't expect such a primitive thing to be absent. The fact that something low-tech wasn't imported into the otherworld without a second thought, or rather, that it was proved to not exist... it's a wonder. It really takes a lot to coin an *a priori* word, doesn't it.

Shion: As a matter of fact, it seems that making up the real part of real fantasy isn't so fun.... I get it now. By the way, Lein, are there more differences between Earth and Atolas that result from *viid*?

Lein: Huh? ... Oh well, the map is different. The map of Atolas looks a lot like the map of Earth, don't you agree?

Alia: In the beginning, when the continent of Pangaea existed, Atolas and Earth looked quite alike. Pangaea also split in a similar way because the conditions such as the axis of rotation, the direction of rotation, the wind, and the ocean currents were similar. As a result, the continents ended up being arranged in a similar way as in today's Earth.

Shion: Is it so? I thought that the creator would get tired of creating another world and imitate Earth.

Arxe: Just by looking at his work experience, it's the opposite. When you look at the original map created in 1995, it indeed has a nonsensical form that seems to have come out of an RPG. Because that was an extremely original map, it doesn't seem that he found it too bothersome to create one from zero. Rather, since he disliked seeing it attacked on its inconsistencies from a realist point of view, it seems that he had arrived at the current form while he made it into something where reality can escape. That's because he's a fussy person.

Alia: In the first half of the 2000s, the map of Atolas was really carelessly made. It seems to be from about 2003 that it came to be made realistically. But still, it was a world with many continents and nothing humans could have lived on.

Shion: Humans can't live on it if there are too many continents? But don't you think, rather, that plenty of them could live if there were a lot of land?

Lein: Shion, Earth is really cold at night. The fact that the temperature doesn't fall more than about 10 °C below daytime is the blessing of the seas because it's harder to cool down sea than land. On the other hand, on the opposite side of Earth where it's day, the sea warms up. That heat also influences the dark side of Earth.

Alia: If the land-to-sea ratio were, for instance, 7:3, then humans would surely die out, right? When you're pursuing such realism, a real fantasy will end up looking like Earth in the end.

Shion: Yeah, I see. But there's something I want to know. Even though making one from zero is a great deal of work, you say that the end result will look like Earth, but wouldn't it not have much impact for the reader? Even if the world was made with a bang, full of contradiction, would many readers complain?

Arxe: That's right for sure on a commercial basis. Real fantasy takes a lot of work for little gain. It definitely won't sell well. So rather than thinking of it as a work for pleasure, it's better to think of it as an artistic or scholarly work. Then the author likes that sort of thing.

Shion: Yeah....

Lein: When you follow him because he's pitiable, being treated as a crank that much, I think it's a good thing in itself to contribute to scholarship and the arts, even if you don't get any profit or reward. Though it's because it's the first attempt at this scale and quality, isn't it?

Alia: Even in the world of science, there are too many double-faced researchers to count. It's often the case that it iself is basic research of no use whatsoever, but it can be made into a product in combination with other research or the foundations for other research.

Arxe: Even your cell phone, Shion, was a product of efforts made by countless double-faced researchers. Though in the end, the only thing written on there is a company's name.

Shion: Hmm, I understood, somehow.

## The result searched for; what was found was loneliness

Shion: By the way, is Arxidia really the first real fantasy to be made in this world? I feel that Tolkien's *Lord of the Rings* had existed earlier.

Alia: On top of that nature, real fantasy needs a detailed *a priori* conlang, but the only conlang with quality and quantity adequate for this purpose in 2011 is Arka. To create a conlang like Arka absolutely requires a PC. So this is something only modern humans can do.

Shion: So even though Tolkien, Yehuda, and Zamenhof might have been experts at conlanging, they couldn't have done it because they didn't have the environment to do so, right? But if it's possible for modern humans, wouldn't there probably be other languages like Arka?

Arxe: I've been searching within Japan for more than ten years, thinking there might be. Books, of course, but I didn't find anything with the same qualifications on the Internet, either. Of course, I investigated some sites in the English-speaking world and even exchanged some BBS posts and e-mails. English, French, German, Italian, Chinese, Korean. Searched them all and nothing to be found.  
"*A priori* / Naturalistic conlang / Detailed craftmanship (quality) / Abundant documentation (quantity)". There is nothing at all out there that meets all four of these criteria. All I found were things like "many words but *a posteriori*" or "many words and *a priori*, but ends up being a vocabulary notebook with one-to-one correspondences in usage and weak descriptions of the culture", a lot of disappointing ones....

Shion: Ummm.... Have you searched in other countries?

Arxe: Frankly, if it hasn't been researched even in developed countries, then there probably isn't any. When you think of it realistically, people in developing countries often don't have the time or the money to work on something considered useless like this. Besides, even if someone in a small country made such a thing, they would usually end up announcing it in English. So when I investigated English sites, I should have found one there.

Shion: Ah, I see. That's right, isn't it?

Arxe: Besides, if it were on the scale of Arka, then you should get a result for it near the top just by searching the net with the word for "conlang" it that country's language, like with this site in reality. I tried searching in various foreign languages, but the sites on the top page don't meet the requirements, and I couldn't find any, even if I dived deeper into the Google results. Even in foreign BBSes, I couldn't get anything that satisfied my criteria fully.

Shion: Hmmm. Then is it possible that a conlang that meets these requirements exists but simply isn't published on the Internet?

Alia: Arka itself evolved greatly when it went public on the net because it became supported by a large number of readers and a smaller number of speakers. Because the number of people interested in an obscure field like conlanging is small, it's hard to look for someone interested in it offline. But online, you can find them relatively easily. If there happens to be a conlang that fulfills the same conditions as Arka, it's reasonable to think about being active on the net. After all, if Arka weren't active online, then it wouldn't have had the quality it has right now.

Shion: Really...? Well, however much work you put into a conlang, it's not gonna be understood by the public, and it's not gonna earn any money either. In Arka's case, it's existed continuously for decades, and it was also constantly blessed with some kind of collaborator. If it didn't have those sorts of miraculous circumstances, then it might not have been made. So when I think about this, it means there might be no way that another conlang with the same criteria exists in the world.

Lein: In reality, although the creators have come to this point, it took them 20 years to do so. That's because they've passed a whole bunch of twists and turns. If you thought of making a new work with the same concepts as Arxidia, it would be much simpler than last time because you have a precedent to draw from called Arka and Kaldia. So I think we'll see works that are even more refined than this, sooner or later. The creator must be waiting for such works. So he's releasing information that he thinks might help with this.

Shion: Huh... is that okay, from the author's point of view? Wouldn't you be annoyed if some stranger manages to do in a few years what it took you 20 years to do?

Arxe: "If no one thinks of opening this heavy door, then I'll be the one to open it. If I open it, then I might grow weary and become unable to walk (mainly the lower back or something). But I hope that those behind me will be able to pass through."— as he says. Such is the life of the leading person. No one uses Edison's light bulbs anymore, but his name is left. It's the same in a minor world such as the conlanging world as well.

Alia: If he hadn't done anything himself, then no one would have been able to see anything past the door, much less open it, right? Rather than stopping there without doing anything, he might have thought that even if people couldn't pass through the door, they'd at least be able to see the scenery on the other side.

Shion: Arxidia exists with its only fantastical component being *viid*, its *δ*. And then *dolmiyu* is the magical theory that seeks to extend *viid* to all fantastical phenomena. In addition, words are crafted with the tenacity that can be seen in camphor. Furthermore, careful consideration is done before coining a new word, as seen with dice calendars. Surely, they've carefully considered the influence that culture and climate gives to language, haven't they? But what about the essential language? Has Arka been made to reflect linguistics accurately? If you're not careful about this, then you're back to square one....

Arxe: Ah, is that so? Just as we talk about this, you're thinking about that, huh? As a matter of fact, in his graduate school days, the creator majored in linguistics like I did. He studied linguistics by himself starting in high school, and he entered the university in this state. And he thought linguistics was necessary for creating Arka. (Or rather, his words were turned around half-forcedly by a friend.) Let's introduce some articles related to language.

→ [How to create a linguistically consistent conlang](146.html) · [Inquiry into the cognitive linguistics of Arka](147.html)

Shion: Oh, I see. I'm relieved that he's taken into account language, culture, and climate all together. Well, it's become pretty long, but I'll sum up the discussions on fantasy theory here.

## Summary

1. Conlang (*Arka*) + conworld (*Kaldia*) = *Arxidia*
2. Fantasy is divided into *high* and *low* depending on whether the setting is fictional or real.
3. Fantasy is divided into *hard* and *light* depending on the detailedness of craftmanship.
4. "Light" doesn't mean that something is trivial, and "hard" doesn't mean that something is wonderful. However, it is a fact that hard fantasy takes more work to construct.
5. Arxidia is hard and high fantasy.
6. When we set reality to 1, completely nonsensical fantasy to 0, and the amount of reality held by a fantasy to *x*, then one can show the domain of *x* to be 0 ≤ *x* < 1.
7. *Real fantasy* is one kind of hard and high fantasy, shown by a value of 0.999...9.
8. Arxidia is the world's first real fantasy. Real fantasy requires an *a priori* conlang with detailed quality and quantity, and the fact that there is currently no conlang other than Arka fulfilling these criteria becomes that basis.
9. The *fantasy factor*, or the difference between 1 and *x*, can be shown with *δ*.
10. The quantity *δ* can be thought to be the number of conveniences (= fantasy settings) needed for a fantasy world. In other words, it can be interpreted as the number of axioms present in that fantasy.
11. When *δ* is at its smallest, a work becomes real fantasy. Because it ceases to become fantasy and starts becoming reality when *δ* = 0, *δ* cannot be zero. That is to say, there must be at least one fantasy setting put up.
12. The *δ* of Arxidia is the sole point of "the existence of *viid*". Outside of this, Earth and Atolas are identical.
13. The factor of *viid* manifests through the first domino of the "dominoes of cause and effect", and the way the dominoes have fallen is different between Earth and Atolas. The resulting differences manifest in magic, gods, death gods, descendants of gods such as Lein, and the shape of continents.
14. Arxidia will not be the only real fantasy, if only you in the future move your hands.

(recommended reading list omitted from this translation)

[original Japanese]: http://conlinguistics.org/arka/study_yulf_145.html
