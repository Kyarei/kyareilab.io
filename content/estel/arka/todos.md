---
title: "Arka material translation to-do list"
---

# Arka material translation to-do list

A list of things related to Arka that I want to translate from Japanese.

## High priority

* The Arka dictionary; in particular, the [Vulgar Arka Dictionary](http://mindsc.ape.jp/klel/).
* [Book of (X\|Sh)ion](http://conlinguistics.org/arka/images/xion.pdf): this is one of the hallmarks of Arka, but it's currently only in Japanese. I started a translation a few years ago, but abandoned it mid-way. I'd like to get back to it, even if I start over and throw away my old work.
* [Ilmus](http://conlinguistics.org/arka/images/ilmus.pdf): major source for in-world history and lore

## Medium priority

* Other major works: [Mailcat](http://conlinguistics.org/arka/images/umi_del_alpan.pdf), [<s>Sonohinoki</s>](http://conlinguistics.org/arka/images/selneeme.pdf), [Dreamweavers](http://conlinguistics.org/arka/images/melidia.pdf), [Cacodemonomania](http://conlinguistics.org/arka/images/anvert.pdf), [<s>Cat Diary</s>](http://conlinguistics.org/arka/works_xale_1.html)
* The rest of [Aldia](http://conlinguistics.org/ardia/); i.e. what's not already reposted on the Conlinguistics blog.
* [How to Create a Conlang](http://conlinguistics.org/create/).
* Japanese articles on the official website that don't have an English version: the [study section](http://conlinguistics.org/arka/study_yulf.html) in particular
* Things in the [appendix](http://conlinguistics.org/files/):
  * Old works: Book of Lein (seriously, 537 pages?), Book of Ridia (almost as long)
  * [The Linguistics Girl and the Tower of Babel](http://conlinguistics.org/files/klei/901_%E8%A8%80%E8%AA%9E%E5%AD%A6%E5%B0%91%E5%A5%B3%E3%81%A8%E3%83%90%E3%83%99%E3%83%AB%E3%81%AE%E5%A1%94/babel.pdf)
  * [Book of Seren](http://conlinguistics.org/files/klei/920_%E3%82%BB%E3%83%AC%E3%83%B3%E3%81%AE%E6%9B%B8/%E3%80%8E%E3%82%BB%E3%83%AC%E3%83%B3%E3%81%AE%E6%9B%B8%E3%80%8F.pdf)
* Posts on [na2co3's blog](https://titlilgas.blogspot.com/)
* [Vulgar Arka Materials](https://sites.google.com/site/slaitismarka/) website
* [Script of that one Arka movie featuring Seren and a rando girl](https://sites.google.com/site/faraspalt/levian/arka/lusian)... along with other Arka-related materials on this site
* [Content from Aios's site](https://w.atwiki.jp/aiosciao/pages/4.html)

## Low priority

* Older versions of the Book of (X\|Sh)ion (listed [here](http://conlinguistics.org/files/klei/index.html))
* [Idler's Newsflash](http://conlinguistics.org/files/axlei/index.html)
* [Conlang Encyclopedia](http://conlinguistics.org/encyclopedia/)
* Things on [this part of the Conlinguistics site](http://conlinguistics.org/conlinguistics/)
* [Posts](http://vanoneeme.blog.fc2.com/blog-category-2.html) on the Vanoneeme blog relating to Arka, at least starting from the 2013 incident. I want to focus on these since there isn't much content in English about this incident. Due to the sensitive nature of these posts, there's no guarantee that I'll make them public.
* [This BBS thread](https://awabi.5ch.net/test/read.cgi/gengo/1347009712/) about Arka. Also covers the 2013 incident, so no guarantees about the translation being publicly available.
* Letters from Seren on [this wiki](https://w.atwiki.jp/serenarbazard/), as they are not publicly available and require requesting access

## Translation of the Vulgar Arka Dictionary

There is a [work-in-progress translation](http://zpdic.ziphil.com/dictionary/arkarattoklel) into English.

Progress:

* Translate entries for level 1 words (in progress)
* Translate entries for level 2 words
* Translate remaining entries with culture or usage sections
* Translate entries for level 3+ words
