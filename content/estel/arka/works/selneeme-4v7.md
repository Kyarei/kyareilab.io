---
title: "tê genven ŋecþatla"
---

# tê genven ŋecþatla

*A translation of [Sonohinoki] ([English translation]) to Ŋarâþ Crîþ v7, using the Arka version.*

ineþa naðasiþ naðaserþ censirþ ceaþ crînas cþaþriþ crîna enôn nagre. vânot lê genven šimen ceha. enêl cêri vin enva cferłimeca cjâs lê črêcas vin gleva. tete; envas lecþat gere.

---

ovesis an gcjeros vonat annêrþon nelrape. vescþeca mjoveca silve.

cjamon'pe calas cjamit vpeþre. ea criþnavit eši jorniłas cemi mene. it calan dranlit'se cenþan meŋcoc'pe łirlan grisþan. meŋco; ondis ela cevo'pe. ela os p·evoþrat lecþa.

asnis endriþ vinþlos nelse. njanton searnime envlesmas vona.

«a&.»

a nâmrit njantasos gesjorniłe. ea ošis aračantasos gesvona.

«vimra; monon'pe ħela.»

soris peanha menþe veła. ai ela čeles'pe os c·jaširþ t·orit ročit sivre. ces; čelanen eþ dtorit dêt relton torime cepasos ores menat'pe dosilvape.

«cerit šona se &{}.»

elan aliþ'pe en telit moþes cemi nâmre.

a gcemrit'pe pemeneþa ceen łirla crînen łirlas vesa. crîna dosilvimeca lerþape'moc.

cenþas a lłirlas gcarmenat tliše. lê cþosnas monþes emter cevos mena.

«ħê?»

tê gcevo pelas men eþit vrasa šan? ea pelas men inos mena; cleli; ai emtan ceła. omtenþa dêt viviþ veła.

«cem'pe.»

ea viviþ gesmagrape; dodranlit gesrende. ai magrapeþ.

«\*cure!»

njanton searnime envlesmas cjamit ħara.

«lê neðas dira &{}.»

eroc lê genvan cehit ħare. os t·orait rendat dôm anhias gesirłe.

vjocþor čil nâto. torit ħaralo so pen dira se? cleli; apasas irłe. olasta; drołit þara. cinras pena pen doħelapeþ se? on cemen dodranlit tlečan cemi eniin enilja.

ceðrimeca gen pelas mina'moc ceła. melon emołila givit penta. fic relsat penteþa relser mina'moc ceła þal. cemi maŋit cenmire so'moc gleveca mjoveca pentes cþam cem'pe tehanen.

cemi gleve so cemen cînte viþca. riłiþime riłiþas cemi vreta.

riłiþit'pe relsin genin doceła. olasta; el varon dovesit pečit nevla. meŋco; riłiþaŋ navon os riłiþit cem ðanhat dopeča. ela cjeli leþma šan? vil; leþma fose edaca.

cemraþ clontos gcarmena. endin veþcirþ ilatan gcinra.

---

«met aneliþ dtariþ se? mê neðas tê vescþas ŋecþatlas geristaþ se? isi; elaþa a menvat pesre ečil gesołaþ» ne isiłe.

tê neðas peni girłeþ se? cleli; leeristat'pe lesaþ. «isplan cačran pelčiþ njofilþa coreþas nelseþ» reþ varacre.

cþjôsos crînen reþojona tfel sar menvaðapeþ. emtan vardreþ so endin cemen voþre viþca.

a reþojona varmenat eči vescþac ferna eþit reflapeþ. pjot emtan fernan varimeca tovlic vescþime neðaþ.

ŋecþatlas B{šinen ðês emtas fenþlele} menvaðime samis'ac menaþ. telpanêrþel'moc sami emter sarin mirv·escþit geðeþ viþca.

«vescþidir» nes fenþlit rendameca «ŋecþatla» ner on lemirnenðat oreþ. an silvit emtas vfenþleleþ. ea cþîgennašepeþ.

lora'c darnaþ. ineþceen môrjan anonaðason f·oscanon'ce firfjas ton vernaatrêr desorselton'ce lomeþe ineþen rislałiŋ'ce pelčaþ. pelecþeþa sar desorsoltos merva tovrac·oþħa. tovrac·oþħas cilþamotola ŋača fomec nefa vełanta.

ŋecþatla cþîmitrašeþ. ea cemen dovelšit gjotaþ.

«šjomeþas mečeveþ» ne gisiłaþ.

tê mivon tafon cþišit rjotaþ. paðorila lê vferna ansadin peþ varit dosilvapeþ. ai nepit fonepeþ. tete; pesre ečil gesołaþ fose lemsat rendaleþ. ledodiraþa šino pelas men searnit. gcemac cenčanoŋ cerecimeca anhias irłenta. meŋco; ceŋac en eþit cem anhias doirłapjoþ.

elaþa olvjesa mîr gjorniłaþ. tê ðês on endrit łanat'ac oreþ. ai amîn neðas cemic on menvat ħarat pentat oreþ.

---

dtliše.

os eristat mîr anelin lê neðas ŋecþatlas roma nelse. šinen ðês mârapeþ. anelin roma cjâsin memaren. ea ac lemenvat'pe os refat'ac mena.

lê neðas lemenvat dâla dosilvape. anhias draner tamasos'ce irłe fose nepa. ðês varasos dranan ros visêrcit amarłe.

a geristat os p·elesþit tecto sadon maraþ.

«cerit rečas so setas dranas gcemaþve.»

tê os m·ivo p·edosilvit ŋačaþ amðameca ondis dosilvit šonipe. lê neðas anrin meŋcoc'ac ndodranlit þarenpe. elit coþi en irłit elaþ ðên ela.

---

cirtel; ša cemen os d·odranlit etor as jorniłit'pe nedo dranlit sivre? cemen ndoedace.

vânot menvat rendale; anhias irłit renda. ai ŋecþatlen meŋcec samis mena se? morton covos ro ndonagra. ondis ŋecþatlan ðaris dranan'ce ŋgrenfan.

ea cemp·aða «ŋecþatla peŋan dranal vande; cem ŋecþatlan mirvande» reþ gisiłape.

ces; ela criþa. toracþa a ledoendrit'pe etor jorniłimeca endrit tersat deneca.

«ħela so ša os lemenvat on vargrit visêrčige dolilcit rjotes?»

norveca'pe «en ħelit ŋêlis os d·ranlit sivrit gleva; ŋecþatla peþ os lemriþ p·ecelclit censa!» ne mare.

meŋco; ela dranal'ne tamos'pe. ela dosilvit denecapeþ se. narvalirþ arantar ŋačaþ.

ea cevon gcarnagra. canagreþos m·ortos mennen mortos. os ŋecþatlas t·ecsat ŋačos šinen mortos'pe el mennen mortasos censa. ŋecþatlen loras tecsat cesvreþ. ŋecþatlas as menvaðit rjotat nedo lê peresþan cjâsin varacre.

vôndos mennen morton ŋgrenfe. «ŋecþatlas o loras tecsat'pe it menvaðit rjotat mepe o silvit'pe'moc menvaðit rjoteþ» reþ orit cesvre.

cjos pesre pelas veła fose silve. pesre vemitrasre so dodranlime pedocarlit'ac dosimitralvape.

«as cemp·aðas marit mîr lesþapeþ» ler menvaðapeþ.

triłit'pe ndosohoraleþ se? veŋ ersa gcarvescaþ se? veŋ paða ndiraþ &{}? isi; lesþapeþ. fjo; pelsa gcarvescaþ fose lesþapeþ. pelas en ŋacþatla relčit ŋêlis os menvat ħarat on vargrit visêrčige os d·olilcit'pe censa.

---

*vânot carðir sensraþfe olvjesa mîr ceha. anelin lê neðas roma &{} a p·emečit sami covos'ac čil esoła. ea om anhoca m·oŋcehac šałanpe.*

*a geristat'pe mela lê genôn aimit'ac menvaðaþ. enôn navon'ac'ce vimrit þare viþca. meŋcoci ac vonat elas rentraþ. olasta; minaerþ ančil eþit cenmireþ. ea elan mareþ.*

*«cerit rečas so setas dranas gcemaþve.»*

*dranlat sivrit cþinle. ea dodranlit pentale. cirtel; on dodranlit lepentat'pe racra se? en fel pentat ŋêlis pelca pesre ceŋa o pesren mirvesrit. dodranlit łagit an ledosilvit nelsiþ menve. ea ela pelca pesre ceŋa os p·esren m·irvesrit cilva. ančil pesre veła fose ac ndodranlit nêrþanon'ac mirat penta. meŋcoc'ac penen entran se? olasta; lê neðas ceha se?*

---

*eleþ more. sondes cehit vraseþ.*

*an mârit ceŋac asanon esvaracre.*

---

vôndos ro mennen morton dosara; cemra.

ŋecþatlas menvat renda.

ŋecþatlas carlit fona.

esłeŋcþime «nelsit ħapa» reþ linelšeþ fose seta.

anolðas ros it ħelit mepe ceŋac asanon varacre.

lê neðas menvat penteþac ŋecþatla &{}

cjamortos drełoþ sanjaŋ ndoelca.

&{} pelas es vescþiþ samis cemepe. pesre'pe en eþit tê genven ŋecþatla.

[Sonohinoki]: http://conlinguistics.org/arka/images/selneeme.pdf
[English translation]: selneeme.html
