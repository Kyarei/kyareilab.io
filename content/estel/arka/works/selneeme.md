---
title: "Sonohinoki"
---

# Sonohinoki

*AKA Selneeme AKA "The Princess of that Day". Translated from the [original] (in Arka and Japanese), mostly from the Arka version. Also ended up [translating this into Ŋarâþ Crîþ]. I actually did it, the madman I was.*

The white becomes blue, the blue becomes red, the red becomes black, then the darkness covers the world. Today is ending as usual. This day is perfectly ordinary for the rest of the world, but not for this young man. Indeed, it is extraordinary for him.

---

As I sit on the balcony, seeing the sun setting, I feel troubled. Rather, I feel uneasy.

I try staring at my body with my mind. Then I feel myself standing here like a doll. And my eyes reflect the red light as if I were a corpse. I see, this is my face now. That's weird; this isn't like me.

Leaving the balcony, I enter the room. I sit on the chair and look at the calendar.

"Ah..."

Just as I say that, I rise from my chair. Then I look up at the bed.

"It's gone bad, my heart."

There's a mirror that I like on the ceiling. But I am scared to imagine it falling on my neck and cutting it. No, I'm not scared to imagine my neck being cut but rather the veins inside, because I don't want to look at the spouting of blood.

"I'm alive for real, aren't I..."

I blurt out again to myself, in the room that no one else is in.

I close my eyes. The red light that I feel becomes black light. Although I am fearing the dark, I also start to feel relieved.

After the red light has left my view, I open my eyes. At this moment, I see her face in the mirror.

"Huh?"

This means that beautiful face is to the left of me, right? Thus, I look to my left, but of course, she is not here.

Then instead of her, there is only a mosquito.

"Hi."

Thus the mosquito starts flying away from me. I start to wish it were dead. But it has escaped.

"Shit!"

I sit on the chair again, gazing at the calendar.

"It's today..."

This day has come for me again. I feel so happy that I want to dance.

There are cuts on my wrists. I wonder what will happen if I make another cut. They hurt, of course. Perhaps I'll cry. Most importantly, why did I do this in the first place? These questions have kept me from killing myself.

I am not inferior, yet I am not skilled at anything. I can succeed quite well in many things. For that reason, I can't focus on any one thing. In other words, I seem to be a fool with talent, however paradoxical that might seem.

If I were not myself, I'd punch me. I am lazy. I hate my lazy self.

I have nothing that I'm skilled at because I'm lazy. Furthermore, I try to avoid changing this personality. Listen, lazy people try to avoid fixing their laziness because they're lazy, makes sense? Yes, quite a shock.

I close my eyes again as I stop seeing my fists. I close the present and open the past[^1].

---

"How many years have passed," I whisper. "I wonder when I met that beautiful princess. Anyhow, I fell in love with her when we met."

What did I feel on that day? I, of course, didn't expect to meet her. I remember, I went out aimlessly while wearing a jacket that didn't suit me.

I noticed someone across the dark thicket near my house. If I'd ignored her, then I wouldn't have become my current self.

I peeked throug the thicket, and I was surprised to see a very beautiful girl there. She was a little kid like me, but she was beautiful, splendid[^2], and charming.

The princess – that's what I always call her – noticed and smiled at me. I don't think even an angel can smile like that more beautifully than her.

I was about to call her a pretty girl, but I felt that "princess" was more fitting for her. I called her by that while feeling worried. But she slowly approached me.

She had long hair. She was wearing a pink shirt[^3], a light blue sweater, a beige skirt with frills, and white, spherical accessories on her head. What stood out, however, was the big ribbon on her skirt. The ribbon had two ends that reached the back of her knees.

The princess came near me. I couldn't move myself.

"Finally, I've found you," she whispered.

I couldn't understand what she meant by that. On the contrary, I was worried that the girl might have been here to attack me. But she didn't want anything from me. In fact, I was going to help her because I fell into love at first sight. She was merely there to my left. Although we didn't even know each other's names, we felt a sense of joy. Indeed, we felt joyful just by being with each other.

After twelve o'clock, she stood up. At that moment, I felt that she had to leave. But I felt that we would be able to meet each other again someday.

---

I open my eyes.

After that encounter, I go to meet the princess on that day every year. She had waited for me every time. We talk to each other about what happened over the year. Then I make sure she's real by having met her.

I'm feeling a little nervous about meeting her today. That's because I feel joyous and, at the same time, scared of death. Every time I meet her, I decide whether I should live or die.

Before she left on our first encounter, she said this:

"If you don't have to live, then I'll give you a peaceful death."

I have never felt any fear from these words before, but I very much do now. Today, I'll perhaps be killed by her green eyes. This is the first time I've felt this way.

---

By the way, am I scared of dying, even though I was about to kill myself? I've made a fool of myself.

I want to meet her as usual and feel happy. But would the princess's eyes smile at me? I cover my face with one of my hands. Now the princess and death are against each other on the scales.

Then my other self inside me whispers that death is as important as the princess, that I was more important than the princess.

No, that's wrong. I'm about to drive out my alter ego, but he continues to insist:

"Then why can't you choose whether to meet her?"

I say out loud: "I can't choose, not because I'm scared of death, but because the princess might decide I'm boring and not worth caring about!"

I see, it's not death but rather this I'm scared of. This is what has kept me worried. I let out a long, slow sigh.

Then I take my hand off my face. It was the left. This left hand is the only one that has touched the princess. In the past, I've instinctively touched the princess's hair. I clearly remember this happening, even though the princess didn't even notice.

I put my left hand on my cheek. The princess hasn't noticed me worrying, I feel, the same way she hasn't noticed me touching her hair.

I love her. That's why I feel worried. I fear that she might kill me and reject me the more I love her. After speaking with my other self, I realize that she has left.

Did she get tired at my advice? Or perhaps she doesn't need it anymore...? At any rate, she has already left. Now I get it: she has left me because I don't need her anymore. I no longer need the princess because I've decided whether I should go and meet her.

---

As usual, he comes to me after eight o'clock. Every year, on this day, after eight o'clock... He smiles for me as soon as he finds me. Then he tempts me with those eyes that I like.

I noticed that the world was so inconsequential for him when we first met. Perhaps he would make waste of the world and its people. I guessed this by looking at his eyes. To add to that, he seemed sad. So I told him this:

"If you don't have to live, then I'll give you a peaceful death."

He doesn't try to be scared of death. This is the reason I can kill him. By the way, I wonder if he knows how I can kill him. I can kill him because he loves me more than himself. He comes to meet me while he worries that I might kill him. So that shows that he loves me more than himself. I can take his soul by killing him because he loves me. I wonder what his eyes look like this year. More importantly, will he come today?

---

The sun is setting. Perhaps I'm earlier than expected.

I recollect our memories with each other while waiting for him.

---

I take my hand off my cheek and close my eyes.

I want to meet the princess.

I don't want the princess to reject me.

I feel calm because I've erased my worries and decided whether I'd go.

I recollect our memories with each other the same way I do so every year.

— The princess I can meet on this day

I wipe my tears in my eyelids with my hand.

Inside me, smiling at me beautifully — the princess of that day whom I love.

[original]: http://conlinguistics.org/arka/images/selneeme.pdf
[translating this into Ŋarâþ Crîþ]: selneeme-4v7.html

[^1]: Here, the Arka version uses <hacm>sesmel</hacm> ("the past") but the Japanese version uses 未来 ("the future").

[^2]: In the Arka version, this was misspelled as <hacm>fav</hacm> instead of <hacm>faav</hacm>.

[^3]: In the Arka version, this was misspelled as <hacm>sala</hacm> instead of <hacm>salas</hacm>.
