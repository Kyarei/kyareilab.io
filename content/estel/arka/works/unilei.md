---
title: "Cat Diaries"
---

# Cat Diaries

*Text only; consult the [original Japanese] page for the images.*

## \#01

| Arka | English |
|-|-|
| <hacm>non males il e</hacm> | I've Remembered It All |
| <hacm>passo, vaxtik il a!</hacm> | Yippee, all correct! |
| <hacm>xia, twa fel lana flomtas soona?</hacm> | Xia, you're studying for the finals, right? |
| <hacm>xom, sef ixm. misk fan twa.</hacm> | Then pass me the notebook. I'll quiz you. |
| <hacm>alkyunk, miifa. non males il e.</hacm> | Got it, Miifa. I've remembered it all. |
| <hacm>dia...</hacm> | Day 1... |
| <hacm>balba!</hacm> | Gratin! |
| <hacm>tia.</hacm> | Correct. |
| <hacm>vio...</hacm> | Day 2... |
| <hacm>popdiax!</hacm> | Pancakes! |
| <hacm>tia.</hacm> | Correct. |
| <hacm>lis...</hacm> | Day 3... |
| <hacm>nekomimi!</hacm> | *Nekomimi*! |
| <hacm>tia.</hacm> | Correct. |
| <hacm>...m? tu et eda toot?</hacm> | ... Hmm? What kind of menu is this? |
| <hacm>myup? hao eda e dunex noan imen xelt tox.</hacm> | Huh? It's the menu for my dinner last month, of course. |
| <hacm>re mal alt!</hacm> | Remember something else! |

Description: Xia: Naive and easygoing. Gets scolded often by Miifa because she teases people without ill intent. Likes animals such as cats. The type of person who is not self-aware, even if there's a problem.

## \#02

| Arka | English |
|-|-|
| <hacm>artfian yuule liiz</hacm> | Yuule the Magical Girl |
| <hacm>yuule yun "artfian fiinya". xom ren axek a la.</hacm> | Yuule, you look like "Fiinya the magical girl". So cosplay as her. |
| <hacm>la aria e xaleveiz? yuna na txu ento vort e.</hacm> | Is she the heroine of a cartoon? I feel so embarrassed that I'm dying. |
| <hacm>xom ren so silm non vast tyu on tas e fis nonno?</hacm> | Then do it if I beat you on today's test, okay? |
| <hacm>tas? smsm, xiyu e!</hacm> | A test? Heehee, okay! |
| <hacm>pant e tas ik miyu a vav im fis dizmal.</hacm> | The subject of the test has been changed to PE today. |
| <hacm>yuule, ar axek! (xante</hacm> | Yuule, transforming! |
| <hacm>ak anjur!</hacm> | How cheerful! |

Description: Yuule: A serious-minded honor student. Shy and obedient but sometimes loses these qualities. Her hobbies are reading and antique collecting. Troubled by her lack of athletic ability.

## \#03

| Arka | English |
|-|-|
| <hacm>teex</hacm> | Folding Fan |
| <hacm>teex...</hacm> | Fan... |
| <hacm>to at sod e?</hacm> | What happened? |
| <hacm>non lax teex aan...</hacm> | I want a fan... |
| <hacm>teex?</hacm> | A fan? |
| <hacm>al ferden. ai, amel meiden hatia em ca sordat yuulia.</hacm> | O Ferden, my sister would love to finally become Prince Charming. (\* fans are the equivalent of chocolates given from boys to girls on Valentine's Day) |
| <hacm>non insat teex lol ka xe ate... (viine</hacm> | I saw an interesting fan at a store... (calm) |
| <hacm>tees dar e.</hacm> (*recte:* <hacm>teex</hacm>) | An automatic fan. |
| <hacm>ya ya, hasan gaato fand a!</hacm> | Yeah, yeah, this is the punch line anyway! |

Description: Miifa: A big sister-type of person who is good at caring for others. A common-sense person who often acts as the straight person. Can't take advantage of other people while being upfront. Her hobby is sports. Skilled with the naginata. Troubled by her short temper.

## \#04

| Arka | English |
|-|-|
| <hacm>mal saen injin</hacm> | Memories before Reincarnation |
| <hacm>lulu, moe felor seta in, feel liiz</hacm> | Oh, it seems you're studying history, Feel. |
| <hacm>tee. tu et xale xiima.</hacm> | No, this is a comic. |
| <hacm>hm? moe et vana on seta?</hacm> | Hmm? You're confident about the history \[test\]? |
| <hacm>hao. noel at kmiir sa injin.</hacm> | Of course. I was Kmiir\* before my reincarnation. (\* a historical heroine) |
| <hacm>xom noel males il e le feme!</hacm> | So I remember everything from that era! |
| <hacm>altoo... (alis a, xanxa lunak.</hacm> | Err... (worry) Oh, the teacher arrived. |
| <hacm>arte! noel malates il kokko arbaren aaaa!</hacm> | What! I remembered it all in Arbaren, aaaah! |
| <hacm>ya ya.</hacm> | Yeah, yeah. |

Description: Feel: An eccentric daydreamer. Convinced that she is the reincarnation of the historical heroine Kmiir. Likes things that are parted from reality, such as magic and myths. Troubled by her short stature.

## \#05l

| Arka | English |
|-|-|
| <hacm>monkast</hacm> | Transfer Student |
| <hacm>xom....</hacm> | Therefore... |
| <hacm>ktak</hacm> | *clack* |
| <hacm>myup? ke--ket?</hacm> | Huh? A cat? |
| <hacm>unii</hacm> | *meow* |
| <hacm>lala es ket luna felez yundi....</hacm> | Well, wonder why is a cat coming into the classroom... |
| <hacm>kop kop</hacm> | *step step* |
| <hacm>xaftik (xante</hacm> | Present ♪ |
| <hacm>s-skiniiiiik! fok lu et aalo a!</hacm> | I-it sat down! And it's so nimble! |

## \#05m

| Arka | English |
|-|-|
| <hacm>haadis</hacm> | Witch Trial |
| <hacm>tyu sil koksaim lenan sete? xom non fit est lex palue myu (xante</hacm> | You're gonna be our classmate, right? Then I'll name you Palue. |
| <hacm>haan, palue xan. mil ket siina xidia pot palue axem?</hacm> | I see, Palue. Because the cat likes to sleep in sunny places, right? |
| <hacm>myu? lu xiel na vem al ema siina...? arte, lala es yundi.</hacm> | Huh? Perhaps he's scared of me...? Oh dear, he is. |
| <hacm>kum alxa nalo sen xalte tisse (axte</hacm> | This animal can sense the mood in this place (LOL) |
| <hacm>momon ca adel fala 85 sanna! re is axek lana noel atm xian a--</hacm> | Momon, the 85th demon, are you? Get out of that transformation so I can sell you— |
| <hacm>dil la rakar, netal (alis</hacm> | Someone, protect against this zealot (sweat) |

## \#06l

| Arka | English |
|-|-|
| <hacm>tac non vastat.</hacm> | But I Passed. |
| <hacm>ema na nik a twa vastat arnamana.</hacm> | I'm surprised that you passed into Arna University. |
| <hacm>saa, im lodtas e....</hacm> | Huh, during the entrance exam... |
| <hacm>tas sil took xi 30 jin tisee.</hacm> | The test will end in 30 seconds. |
| <hacm>passo, tu et knoos veer! ...myup?</hacm> | It's okay, this is the final problem! ... Huh? |
| <hacm>apamas et xoket du 1 saks xan!</hacm> | I'm off by one row on the answer sheet! |
| <hacm>tac non vastat....</hacm> | But I passed... |
| <hacm>koot tu et siiyu soona??</hacm> | Rather, isn't that amazing?? |

## \#06m

| Arka | English |
|-|-|
| <hacm>sertia</hacm> | Kmiir's Scythe |
| <hacm>xom on moen,? feel liiz.</hacm> | And what about you, Feel? |
| <hacm>noel?</hacm> | Me? |
| <hacm>hao, vastat sofel a,! kolset kmiir salan.</hacm> | Of course, I passed easily, thanks to Kmiir! |
| <hacm>tet kmiir korat kantar nanna?</hacm> | But didn't Kmiir go to Kantar? |
| <hacm>moe en ser--?</hacm> | Don't you know—? |
| <hacm>gelx! sertia!</hacm> | Knockout! "Kmiir's Scythe"! |
| <hacm>lami a!</hacm> | I'm in danger! |

## \#07l

| Arka | English |
|-|-|
| <hacm>en pank!</hacm> | Not a Button! |
| <hacm>le yun...</hacm> | Those are like... |
| <hacm>saa knoos. feng til 6 zam. ax az teo?</hacm> | Here's a question. Spiders have six legs. True or false? |
| <hacm>ax!</hacm> | True! |
| <hacm>gnog</hacm> | *click* |
| <hacm>yai! (selest tu til to?</hacm> | Oww! (confused) What's inside this thing? |
| <hacm>taik ixn et evit a....</hacm> | By the way, the answer was wrong... |

## \#07m

| Arka | English |
|-|-|
| <hacm>hait nod sor</hacm> | Short Over Tall |
| <hacm>soonoyun. to es sod?</hacm> | Hi. What's going on? |
| <hacm>etto kuverat noel et caiz... (leeve</hacm> | Big brother said I was a midget... (cry) |
| <hacm>passo e. haiten et ail xina nod soren.</hacm> | It's okay. Sometimes short people are luckier than tall ones. |
| <hacm>xian tees elf noel et caiz sanna (haizen</hacm> | You're not trying to deny that I'm short, right? (scold) |
| <hacm>dong</hacm> | *crash* |
| <hacm>ser? tu et ca "ail" yuulia.</hacm> | You see? This is the "sometimes" I was talking about. |
| <hacm>en na nau!</hacm> | I'm not happy! |
| <hacm>namt hal</hacm> | (Beware of above) |

## \#08l

| Arka | English |
|-|-|
| <hacm>caifa fir</hacm> | White Rapids |
| <hacm>ximik bakm!</hacm> | Got some milk! |
| <hacm>el em sor ol xen bakm di.</hacm> | You get tall if you drink lots of milk. |
| <hacm>daim, veil moe til sil tomyai e.</hacm> | Stop, or else you'll get a stomachache. |
| <hacm>ti ik sod?</hacm> | What happened? |

## \#08m

| Arka | English |
|-|-|
| <hacm>tu til to?</hacm> | What's Inside This? |
| <hacm>re mono! (haizen</hacm> | Stop! |
| <hacm>keiluko dia tea so sen a.</hacm> | Why don't you catch me if you can. |
| <hacm>ep.</hacm> | Heh. |

(Up to you to imagine what sound it made)

## \#09l

| Arka | English |
|-|-|
| <hacm>te em vax</hacm> | Don't Be in a Hurry |
| <hacm>lop lunak. re em vax a!</hacm> | The train's come. Hurry up already! |
| <hacm>lop leev sat.</hacm> | The train's gonna leave. |
| <hacm>te em vax. vat dia kes kont isk lei.</hacm> | Don't be in such a hurry. Why don't we wait for the next one while reading a book? |
| <hacm>lop lunak me. re em vax a.</hacm> | The train came again. Hurry up. |
| <hacm>ren vat kalel. atu et fand linoa.</hacm> | Wait a moment. I'm at the climax right now. |
| <hacm>lop leev sat!</hacm> | The train's about to leave! |
| <hacm>te em vax. vat dia kes kont....</hacm> | Don't be in such a hurry. Why don't we wait for the next one while... |

## \#09m

| Arka | English |
|-|-|
| <hacm>fanaenvi</hacm> | Concentration Ability |
| <hacm>feng xan! melis a! netal ren stas leebe!</hacm> | It's a spider! Disgusting! Someone, drive it out! |
| <hacm>teo, ami oj ris.</hacm> | No, I don't want to touch it. |
| <hacm>vekm eftik pof dent! re fos!</hacm> | The crow stole my bread! Catch it! |
| <hacm>woooo re mono!</hacm> | Waaaaaah, stop! |
| <hacm>gelx!!</hacm> | Shoot!! |
| <hacm>haizen (haizen so ka tant!</hacm> | Hey, you, do it outside! |
| <hacm>aa! pxet ik rig sin....</hacm> | Aah! The sprinklers are destroyed... |
| <hacm>nyaaaaaaaaaaaaaaaaaaaaaaaaa</hacm> | Gaaaaaaaaaaaaaaaaaaaaaaah! |

## \#10l

| Arka | English |
|-|-|
| <hacm>tu et kin xel el xos 2</hacm> | It's Hard to Carry Two |
| <hacm>tyu ximik vix sete? kor dia valnvals e.</hacm> | You've got a wound, right? Why don't we go to the infirmary? |
| <hacm>tee. passo e. tu tis.</hacm> | No, I'm okay. It's just this. |
| <hacm>teo, non xos fan tyu.</hacm> | No, I'm going to carry you. |
| <hacm>sentant. xian et niit na.</hacm> | Thanks. You're so nice. |
| <hacm>lukor lanaka teu dudkipar antisse.</hacm> | We're reaching our destination: the scale. |
| <hacm>halka et wolt rak nyannya (flea</hacm> | You've gained too much weight, haven't you? |
| <hacm>d, dad....</hacm> | H-heavy... |

## \#10m

| Arka | English |
|-|-|
| <hacm>diasomt</hacm> | A Death God |
| <hacm>non nik fan feel kokko kyosab len yutiasel.</hacm> | I'm gonna surprise Feel with a costume for Summoning Day. |
| <hacm>dia niit az fia viit?</hacm> | A gentle lie or the grim truth? (\* a death god's phrase of decision) |
| <hacm>diasomt aa!!</hacm> | I-it's a death god! |
| <hacm>re aldert nan sami a. noel sins fan envi e taflan t'avelant eu.</hacm> | Curse yourself with misfortune. I'll show you the power of a death god hunter. |
| <hacm>myup? to?</hacm> | Huh? What? |
| <hacm>ggg.... ai mel lunak xel noel aldespa tu niadolk....</hacm> | Mwahaha... The time has finally come for me to unseal this hair ornament... |
| <hacm>loki vil tet lami a. non re elf!</hacm> | I don't understand but it looks dangerous. Time to run! |

## \#11l

| Arka | English |
|-|-|
| <hacm>arzon</hacm> | Magic Wand |
| <hacm>non xant arzon l'ern noan olax tat mec.</hacm> | I present you a magic wand that my relatives hand down every generation. |
| <hacm>te pilp noel e.</hacm> | Don't play around with me. |
| <hacm>fai!</hacm> | Fire! |
| <hacm>fai!!</hacm> | Fire!! |
| <hacm>kokko flam!?</hacm> | Physical attack!? |
| <hacm>tet non porserat.</hacm> | But I predicted it. |

## \#11m

| Arka | English |
|-|-|
| <hacm>arzon 2</hacm> | Magic Wand 2 |
| <hacm>lulu, tu et....</hacm> | Oh my, this is... |
| <hacm>yuule, ar axek!</hacm> | Yuule, transforming! |
| <hacm>nanan.</hacm> | Just kidding. |
| <hacm>m, moe isk lan texa tam e "artfian fiinya"?</hacm> | D-do you want to read an ultra-rare collection of "Fiinya the Magic Girl"? |
| <hacm>vergil eyo!?</hacm> | Is this a bribe!? |
| <hacm>dina es tyu til tuumi!?</hacm> | Apart from that, why do you have this!? |

## \#12l

| Arka | English |
|-|-|
| <hacm>mer on leimalx</hacm> | Perfect for Natural Numbers |
| <hacm>ren fit rexir, xia.</hacm> | What's the answer, Xia? |
| <hacm>tu et 3.</hacm> | It's 3. |
| <hacm>tia. fas rat.</hacm> | Correct. Good job. |
| <hacm>nian</hacm> | (Break) |
| <hacm>nat nik a twa vaxtat.</hacm> | I'm surprised that you solved it. |
| <hacm>xink non lokit vil.</hacm> | To be honest, I couldn't understand it. |
| <hacm>hep?</hacm> | Huh? |
| <hacm>kolset zanlalx tisse.</hacm> | It's thanks to the roll of a die. |
| <hacm>xina soona!</hacm> | How lucky! |
| <hacm>ol rexir noden 6?</hacm> | What if the answer is more than 6? |
| <hacm>gpp tyu en ser? zanl til pit di tisse.</hacm> | Gahhah, don't you know? There are many kinds of dice, you know. |
| <hacm>re fel!</hacm> | Study for once! |

## \#12m

| Arka | English |
|-|-|
| <hacm>alkyunk</hacm> | Leave It to Me |
| <hacm>ema loki nei ak twa lodat al arnamana.</hacm> | I don't get how you got into Arna University yet. |
| <hacm>lala es eyo?</hacm> | Oh, why? |
| <hacm>mil twa si nife rat taik te elef....</hacm> | Because you don't have good grades, and you're not attentive at all... |
| <hacm>me vekm eftik pof dent! re fos!</hacm> | A crow stole my bread again! Catch it! |
| <hacm>alkyunk!</hacm> | Leave it to me! |
| <hacm>siiyu vart! atu et aks 3 kaak!?</hacm> | Amazing! This is the third floor, isn't it? |
| <hacm>siiyu da! xia!!</hacm> | That was awesome! Xia!! |
| <hacm>tet la es not a tu felka.</hacm> | But she's familiar with this school. |

## \#13

| Arka | English |
|-|-|
| <hacm>vadrens</hacm> | Tongue Twister |
| <hacm>klans rens sen "slea bcea trea" ras 3 vadel?</hacm> | Can you all say "*slea bcea trea*" three times fast? |
| <hacm>ou, vadrens xan.</hacm> | Oh, it's a tongue twister. |
| <hacm>slea bcea tr....</hacm> | *slea bcea tr...* |
| <hacm>vesn ik dio!!</hacm> | I bit my tongue!! |
| <hacm>liia rekik ano tur anramma, haxt.</hacm> | We've failed the current plan, boss. |
| <hacm>eheh.</hacm> | Eheh. |
| <hacm>nee xia, on twa?</hacm> | Hey, what about you, Xia? |
| <hacm>slea bcea trea slea bcea trea slea bcea trea</hacm> | *slea bcea trea, slea bcea trea, slea bcea trea* |
| <hacm>sof sete?</hacm> | Simple, right? |
| <hacm>aalo loodel fien xia tis!</hacm> | More skilled than I expected, even if it's just Xia! |

[original Japanese]: http://conlinguistics.org/arka/works_xale_1.html
