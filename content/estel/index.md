---
title: "#flirora's Translation Center"
---

# #flirora's Translation Center

Translations I've done from $language1 to $language2\. Naturally, the quality of the translation depends on my knowledge of $language1 and $language2\.

[Toluthin Antenna](antenna.html) ($language1 = Japanese; $language2 = Arka)

[Daughter of Evil](musume.html) ($language1 = Japanese; $language2 = Arka)

[Random translations into Arka](arka_misc.html) ($language1 = English; $language2 = Arka)

## Translations of Arka-related documents

I've translated these documents from Japanese to English because I want to see them translated and I think they're an important part of conlanging culture. Note that I'm currently an amateur at translating from Japanese. **None of these translations should be construed as an endorsement of Seren Arbazard's opinions.**

See [here](arka/todos.html) for plans.

### Major works

* [Cat Diaries](arka/works/unilei.html)
* [Sonohinoki](arka/works/selneeme.html)

### Pages from the Conlinguistics blog

These pages are from [_Gengo Kekkai_](http://conlinguistics.org/blog/) (Arka Border), the Conlinguistic Research Society's blog, used to upload things written by Seren Arbazard. I am aiming to translate all of these articles from the oldest first.

The dates listed are the dates when the posts were uploaded to the original blog in Japanese.

[Aldia, the Four-Fold Record](arka/gengokekkai/aldia.html) (2014/1/9)

[On the usage of converbs and <hacm>hot</hacm>, <hacm>tan</hacm>, <hacm>as</hacm>, and <hacm>tis</hacm>](arka/gengokekkai/hot_tan_as_tis.html) (2014/1/19)

[About this site](arka/gengokekkai/about.html) (2014/1/19)

[Appendix: novels, short works, and diaries](arka/gengokekkai/supplement.html) (2014/1/29)

[Lanj: Preface 1](arka/gengokekkai/lanj_preface_1.html) (2014/2/7)

[Lanj: Preface 2](arka/gengokekkai/lanj_preface_2.html) (2014/2/7)

[Lanj: Preface 3](arka/gengokekkai/lanj_preface_3.html) (2014/2/7)

[Lanj: 2011/7/19 8 P.M.](arka/gengokekkai/lanj_2011_7_19_8pm.html) (2014/2/7)

[Lanj: 2011/7/27 A.M.](arka/gengokekkai/lanj_2011_7_27_am.html) (2014/2/7)

[Lanj: 2011/9/20](arka/gengokekkai/lanj_2011_9_20.html) (2014/2/7)

[Lanj: 2011/11/1](arka/gengokekkai/lanj_2011_11_1.html) (2014/2/7)

[Lanj: 2011/11/18](arka/gengokekkai/lanj_2011_11_18.html) (2014/2/7)

[Lanj: 2011/12/19](arka/gengokekkai/lanj_2011_12_19.html) (2014/2/7)

[Lanj: 2012/1/23](arka/gengokekkai/lanj_2012_1_23.html) (2014/2/7)

[Lanj: 2012/2/22](arka/gengokekkai/lanj_2012_2_22.html) (2014/2/7)

[Lanj: 2012/3/9](arka/gengokekkai/lanj_2012_3_9.html) (2014/2/7)

[Lanj: 2012/6/13](arka/gengokekkai/lanj_2012_6_13.html) (2014/2/7)

[Lanj: 2012/6/27](arka/gengokekkai/lanj_2012_6_27.html) (2014/2/7)

[Lanj: 2012/7/15](arka/gengokekkai/lanj_2012_7_15.html) (2014/2/7)

[Lanj: 2012/7/28](arka/gengokekkai/lanj_2012_7_28.html) (2014/2/7)

[Lanj: 2012/9/19](arka/gengokekkai/lanj_2012_9_19.html) (2014/2/7)

[Lanj: 2012/10/19](arka/gengokekkai/lanj_2012_10_19.html) (2014/2/7)

[Lanj: 2012/11/15](arka/gengokekkai/lanj_2012_11_15.html) (2014/2/7)

[Lanj: 2012/12/5](arka/gengokekkai/lanj_2012_12_5.html) (2014/2/7)

[Lanj: 2012/12/31](arka/gengokekkai/lanj_2012_12_31.html) (2014/2/7)

[Lanj: 2013/3/5](arka/gengokekkai/lanj_2013_3_5.html) (2014/2/7)

[Lanj: 2013/4/14](arka/gengokekkai/lanj_2013_4_14.html) (2014/2/7)

[Lanj: 2013/5/14](arka/gengokekkai/lanj_2013_5_14.html) (2014/2/7)

[Lanj: 2013/6/27 (1/3)](arka/gengokekkai/lanj_2013_6_27_1.html) (2014/2/7)

[On conlang dictionaries and Vulgar Arka](arka/gengokekkai/dictionary_and_arka.html) (2014/2/12)

[Lanj: 2013/6/27 (2/3)](arka/gengokekkai/lanj_2013_6_27_2.html) (2014/2/13)

[Lanj: 2013/6/27 (3/3)](arka/gengokekkai/lanj_2013_6_27_3.html) (2014/2/21)

[Lanj: 2013/6/28 (1/2)](arka/gengokekkai/lanj_2013_6_28_1.html) (2014/2/21)

[Lanj: 2014/3/8: Box of Etoa (1/2)](arka/gengokekkai/lanj_2014_3_8_1.html) (2014/2/28)

[Twin courses](arka/gengokekkai/twin_courses.html) (2014/3/1)

[Lanj: 2013/6/28 (2/2)](arka/gengokekkai/lanj_2013_6_28_2.html) (2014/3/5)

[Lanj: 2013/6/29](arka/gengokekkai/lanj_2013_6_29.html) (2014/3/5)

[Lanj: 2013/6/30](arka/gengokekkai/lanj_2013_6_30.html) (2014/3/5)

[Lanj: 2014/1/23 (1/2)](arka/gengokekkai/lanj_2014_1_23_1.html) (2014/3/5)

[Book of Arka](book_of_arka.html)

[Yu Arka](yu.html)

### Theory articles on official Arka website

These pages are listed under [here](http://conlinguistics.org/arka/study_yulf.html). Many of these are not translated into English, and even if there is an official English translation for an article, it is usually incomplete (in which case it is marked with an asterisk).

| \# | Original | Official translation (if any) | Translation here (if any) |
|---:|----------|-------------------------------|------------------|
| 1 | [アルカはほかの人工言語とどう違うのか](http://conlinguistics.org/arka/study_yulf_1.html) | [What's Arka's features?](http://conlinguistics.org/arka/e_study_yulf_1.html)\* | [How does Arka differ from other conlangs?](arka/study/1.html) |
| 2 | [アルカの人工言語学的な意義](http://conlinguistics.org/arka/study_yulf_2.html) | | [The conlinguistic meaning of Arka](arka/study/2.html) |
| 145 | [ファンタジー論](http://conlinguistics.org/arka/study_yulf_145.html) | | [Fantasy Theory](arka/study/145.html) |
| 146 | [言語学的に矛盾しない人工言語の作り方](http://conlinguistics.org/arka/study_yulf_146.html) | | [How to create a linguistically consistent conlang](arka/study/146.html) |
| 147 | [アルカの認知言語学的考察](http://conlinguistics.org/arka/study_yulf_147.html) | | [Inquiry into the cognitive linguistics of Arka](arka/study/147.html) |
| 3 | [なぜ今まで幻奏のような作品がなかったのか](http://conlinguistics.org/arka/study_yulf_3.html) | | [Why there has not been any work like Arxidia until now](arka/study/3.html) |
| 4 | [アルカは英語に似ているか](http://conlinguistics.org/arka/study_yulf_4.html) | | [Does Arka resemble English?](arka/study/4.html) |
| 164 | [音声学・音韻論](http://conlinguistics.org/arka/study_yulf_164.html) | [Phonology and Phonetics](http://conlinguistics.org/arka/e_study_yulf_164.html) | |
| 126 | [アクセント](http://conlinguistics.org/arka/study_yulf_126.html) | [Accent](http://conlinguistics.org/arka/e_study_yulf_126.html)\* | [Stress](arka/study/126.html) |
| 127 | [アクサン考](http://conlinguistics.org/arka/study_yulf_127.html) | | |
| 125 | [オノマトペ](http://conlinguistics.org/arka/study_yulf_125.html) | | |
| 5 | [アトラスの国際音声字母](http://conlinguistics.org/arka/study_yulf_5.html) | [IPA in Kaldia](http://conlinguistics.org/arka/e_study_yulf_5.html)\* | |
| 6 | [粗野な発音](http://conlinguistics.org/arka/study_yulf_6.html) | | |
| 141 | [回復点](http://conlinguistics.org/arka/study_yulf_141.html) | | |
| 163 | [綺麗な言語](http://conlinguistics.org/arka/study_yulf_163.html) | | [A beautiful language](arka/study/163.html) |
| – | [幻字の発展](http://conlinguistics.org/arka/hacm.html) | [History of Lunar Alphabet](http://conlinguistics.org/arka/e_hacm.html)\* | |
| 149 | [表音幻字（幻字）](http://conlinguistics.org/arka/study_yulf_149.html) | [Lunar Alphabet](http://conlinguistics.org/arka/e_study_yulf_149.html)\* | |
| 150 | [表意幻字（幼字）](http://conlinguistics.org/arka/study_yulf_150.html) | [Lunar Letter](http://conlinguistics.org/arka/e_study_yulf_150.html)\* | |
| 151 | [天秤詞](http://conlinguistics.org/arka/study_yulf_151.html) | [Libra Tone](http://conlinguistics.org/arka/e_study_yulf_151.html)\* | |
| 152 | [幼字の字源](http://conlinguistics.org/arka/study_yulf_152.html) | | |
| 161 | [幻京書体崩壊の危機](http://conlinguistics.org/arka/study_yulf_161.html) | | |
| 12 | [属格](http://conlinguistics.org/arka/study_yulf_12.html) | [Genitive Case](http://conlinguistics.org/arka/e_study_yulf_12.html)\* | |
| 13 | [呼格](http://conlinguistics.org/arka/study_yulf_13.html) | | |
| 14 | [代名詞の代入](http://conlinguistics.org/arka/study_yulf_14.html) | | |
| 15 | [nosの使い方](http://conlinguistics.org/arka/study_yulf_15.html) | | |
| 17 | [xeとfiの語法](http://conlinguistics.org/arka/study_yulf_17.html) | | |
| 132 | [xeとfiの細分化](http://conlinguistics.org/arka/study_yulf_132.html) | | |
| 138 | [アスペクト考](http://conlinguistics.org/arka/study_yulf_138.html) | [Aspect](http://conlinguistics.org/arka/e_study_yulf_138.html)\* | |
| 19 | [新生のアスペクト考](http://conlinguistics.org/arka/study_yulf_19.html) | | |
| 20 | [アスペクト論争の流れ](http://conlinguistics.org/arka/study_yulf_20.html) | | |
| 21 | [アスペクト考の術語集](http://conlinguistics.org/arka/study_yulf_21.html) | | |
| 22 | [反復相はなぜアスペクトと呼ばれるか](http://conlinguistics.org/arka/study_yulf_22.html) | | |
| 27 | [時制の一致](http://conlinguistics.org/arka/study_yulf_27.html) | [Sequence of Tenses](http://conlinguistics.org/arka/e_study_yulf_27.html)\* | |
| 24 | [動詞マーカー](http://conlinguistics.org/arka/study_yulf_24.html) | | |
| 25 | [複合時制](http://conlinguistics.org/arka/study_yulf_25.html) | | |
| 26 | [知覚動詞の自発性](http://conlinguistics.org/arka/study_yulf_26.html) | | |
| 29 | [移動動詞](http://conlinguistics.org/arka/study_yulf_29.html) | | |
| | (... more will be listed in the future) | | |

### Others

[On part-of-speech changes for casers](arka/davfapel/pea.html) (davfapel)

[CONLANG](conlangue.html) (na2co3)

## Conlanginktober

[2019](clongtober/30.html) (Ŋarâþ Crîþ v7)