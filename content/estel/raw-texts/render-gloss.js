var map = {
  '&': '&amp;',
  '<': '&lt;',
  '>': '&gt;',
  '"': '&quot;',
  "'": '&#039;',
  '#': ' ',
};

// thx https://stackoverflow.com/a/4835406/3130218
function escapeHtml(text) {
  return text.replace(/[&<>"'#]/g, function(m) { return map[m]; });
}

function getClassesOne(props) {
  let a = ["one"];
  if (props.script === "hacm")
    a.push("hacm");
  return a.join(" ");
}

function isSeparator(c) {
  let m = {
    "-": true,
    "=": true,
    ".": true,
    "%": true,
  };
  return m[c] !== undefined;
}

function renderLineTwo(s) {
  let out = "";
  for (let i = 0; i < s.length; ++i) {
    let c = s.charAt(i);
    if (map[c] !== undefined) {
      out += map[c];
    } else if (c === "%") {
      ++i;
      let j = i;
      while (j < s.length && !isSeparator(s.charAt(j)))
        ++j;
      // now j stands at next separator or end of string
      out += '<sc>';
      out += escapeHtml(s.slice(i, j));
      out += '</sc>';
      i = j - 1;
    } else {
      out += c;
    }
  }
  return out;
}

function renderGloss(raw, elem, props) {
  let lines = raw.trim().split("\n");
  if (lines.length % 2 != 0) {
    throw "renderGloss: the number of lines should be even";
  }
  let html = "";
  for (let i = 0; i < lines.length / 2; ++i) {
    html += '<div class="sentence">';
    let words1 = lines[2 * i].split(/\s+/);
    let words2 = lines[2 * i + 1].split(/\s+/);
    if (words1.length != words2.length) {
      throw "renderGloss: the number of words on lines should match";
    }
    let words = words1.map(function(e, i) { return [e, words2[i]] });
    for (word of words) {
      html += '<div class="word">';
      html += '<div class="' + getClassesOne(props) + '">';
      html += escapeHtml(word[0]);
      html += '</div>';
      html += '<div class="two">';
      html += renderLineTwo(word[1]);
      html += '</div>';
      html += '</div>';
    }
    html += '</div>';
  }
  elem.innerHTML = html;
}

function loadFile(path, callback) {
  let client = new XMLHttpRequest();
  client.open('GET', path, true);
  client.onreadystatechange = function() {
    if (this.readyState === 4 && this.status === 200)
      callback(this.responseText);
  }
  client.send();
}

function loadAndRender(id, fname, props) {
  let elem = document.getElementById(id);
  loadFile(fname, function(con) {
    renderGloss(con, elem, props);
  });
}
