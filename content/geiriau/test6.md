---
title: cerecaþa artfaþo
---

# cerecaþa artfaþo (Forgotten City)

[Link (YouTube)](https://youtu.be/XS63_lIlD3A)

Notes:

* pronunciation is given for the standard dialect when spoken
* <i>&</i>s are shown as pronounced in the song
* there is no productive reduplication and there never will be in Ŋarâþ Crîþ v7
* there are no politeness levels and there never will be in Ŋarâþ Crîþ v7

> **naðason jonas'moc virfeþon anon cenþon c·ajon'ce varacre.**  
> \[naˈðason jonasˈmok viɹˈfeθon ˈanon ˈkenθon aˈjoɲce vaˈɹakɹe\]  
> blue-REL.TER.NOM,ACC now-LOC.SG=also nostalgic-REL.TER.DAT,ACC sky-ACC.SG red-REL.TER.NOM,ACC (REL\\)ground-ACC.SG=and remember-1SG  
> *Now I remember the nostalgic blue sky and the red ground.*

> **ai anon eneða nagrame el tfołor annotac nava ceła þal.**  
> \[ai ˈanon eˈneða naˈɡɹame el tfoˈɬoɹ anːoˈtak ˈnava ˈkeɬa θal\]  
> but sky-ACC.SG ash-NOM.SG cover-and this.TER path-LOC.PL go_for_walk-REL.HUM.NOM.NOM person-NOM.SG not_exist-3SG ASSERT  
> *But the sky is covered with ash and there is no one walking on the roads.*

> **fel rjota.**  
> \[fel ˈɹjota\]  
> ELLIPSE.TARGET fail_to-1SG  
> *I can't [see anyone].*

> **ragime dosilva miþen vistel'ce celcol pecþiširþ jorniło;**  
> \[ɹaˈɡime doˈsilva ˈmiθen visˈtelke ˈkelkol pex͡θiˈʃiɹθ joɹˈniɬo\]  
> high-and CAUS-nervous-REL.CEL.NOM,NOM glass-GEN.SG stone-GEN.SG=and building-NOM.PL 1SG-surround-and stand-3PL  
> *The tall buildings, made of glass and stone, stand around me, making me nervous;*

> **or'moc es sero as reto'ce telit nedo navas minas'moc menat rjota þal.**  
> \[oɹˈmok es ˈseɹo as ɹeˈtoke teˈlit ˈnedo ˈnavas minasˈmok meˈnat ˈɹjota θal\]  
> PR.3PL.LOC=also inside moss INF.ABL vine=and not_exist-INF despite person-DAT.SG one.DAT=also see-INF fail_to-1SG ASSERT  
> *Even though they have no moss or vines, I can't see anyone inside.*

> **ašis desasor'ce šinen cjori'ce avona vaðadir'til sarime mitra.**  
> \[ˈaʃis desaˈsoɹke ˈʃinen kjoˈɹike aˈvona vaðadiɹˈtil saˈɹime ˈmitɹa\]  
> place_above-DAT.SG place_below-DAT.SG=and all-GEN.SG direction-DAT.SG=and wind-NOM.SG wander-DAGENT-SEMBL.II.SG=SEMBL turn-and run-3SG  
> *Like a wanderer, the wind rushes up, down, in all directions.*

> **an vôndon gcilat'ac taras mel gatrêr cfiþes ton enes amarłimeca referþ veła.**  
> \[an vɔ̰nˈdon ɡilaˈtak ˈtaɹas mel ˈɡatɹɛ̰w kfiˈθes ton ˈenes amaɹɬiˈmeka ɹeˈfeɹθ ˈveɬa\]  
> INF.PROL cheek-ACC.SG (INF\\)touch.NONVOL-INF=POSS.3 pass-REL.CEL.NOM,DAT much (GEN\\)colour-GEN.PL leaf-DAT.PL ORN.ADN tree-DAT.PL ponder-but reality-NOM.SG exist-3SG  
> *While it brushes upon my cheek, I think about the trees with colourful leaves of the past, but the reality is there on me.*

> **ša nema?**  
> \[ʃa ˈnema\]  
> INT any-NOM.SG  
> *Is anyone there?*

> **ša amarłeþos mel veła; ša mon crotinsa daraðe inorit šonat gere?**  
> \[ʃa amaɹˈɬeθos mel ˈveɬa ʃa mon kɹoˈtinsa daˈɹaðe inoˈɹit ʃoˈnat ˈɡeɹe\]  
> INT ponder-REL.TER.DAT.NOM many-NOM.SG exist-3SG INT mind-NOM.SG wave-ABL.PL hexmyriad-CTR.EVENT be_empty-INF do_completely-INF truly-3SG  
> *Is there much I think about? Is it true that my mind is completely empty from those 65,536 waves?*

> **ša šinen vesra coþ šinen anhia'ce tračia'ce asena'ce magrirþ gcarvełaþ?**  
> \[ʃa ˈʃinen ˈvesɹa koθ ˈʃinen aɲçiˈake tɹatʃiˈake aseˈnake maˈgɹiwθ ɡarveˈɬaθ\]  
> INT all-GEN strong-REL.CEL.NOM,NOM feeling-NOM.SG all-GEN joy-NOM.SG=and sadness-NOM.SG=and anger-NOM.SG=and flee-SERIAL PFV\\TERMINATIVE-exist-3SG-PAST  
> *Has all of my intense feelings, all of my joy, sorrow and rage, escaped me and left me bare?*  

> **ea lê ŋgljoþos nela cem'pe geriłime naclidime crînen osmjel paðas cjarše.**  
> \[ea lɛ̰ ˈŋljoθos ˈnela ˈkempe ɡeɹiˈɬime nakliˈdime kɹḭˈnen osˈmjel ˈpaðas ˈkjaɹʃe\]  
> thus this.CEL maze-LOC.SG be_lost-REL.CEL.NOM,NOM self-NOM.SG=POSS.1 be_dizzy-and trip_over-and black-GEN dream-GEN.SG other-DAT.SG fall-1SG  
> *Thus I, lost in this maze and disoriented, trip and fall into another dark dream.*

> **a ndrênit cêna inoren jonos šinen ðêþê eþit cenmira.**  
> \[a nɹeˈnit kɛ̰ˈna iˈnoɹen ˈjonos ˈʃinen ˈðɛ̰θɛ̰ eˈθit kenˈmiɹa\]  
> INF.LOC (INF\\)wake_up same-GEN.SG emptiness-GEN place-LOC.SG all-GEN occurrence-LOC.SG exist-INF seems_that-3SG  
> *It seems that I end up in the same empty place every time I wake up.*

> **ela lecþeþa criþþas gleva; tete; ela arefas minas'ot censa þal.**  
> \[ela leˈx͡θeθa ˈkɹiθːas ˈgleva ˈtete ˈela aˈɹefas minaˈsot ˈkensa θal\]  
> this-NOM.SG consider_unusual-REL.CEL.DAT,NOM illusion-DAT differ-3SG in_fact this-NOM.SG truth-DAT.SG one.DAT=LEQ equal-3SG ASSERT  
> *This isn't a weird illusion; in fact, it's the only truth.*

> **cþirin en rilpina þiħat'pe ðês ros amčia pelsa careþime**  
> \[ˈx͡θiɹin en ɹilˈpina θiˈʕatpe ðɛ̰s ɹos amˈtʃia ˈpelsa kaɹeˈθime\]  
> environs-ACC.SG INF.GEN fog-PROL.SG peer-INF=POSS.1 occurrence-LOC.SG each-LOC.SG hope-NOM.SG PR.1SG.ABL TERMINATIVE\\exist-and  
> *Every time I peer into the fog around me, I lose some hope, and*

> **tê sefran asanon carracrit rjota se.**  
> \[tɛ̰ ˈsefɹan aˈsanon kaɹːaˈkɹit ˈrjota se\]  
> that.CEL be_unpleasant-REL.CEL.NOM,ACC memory-ACC.PL TERMINATIVE\\know-INF fail-1SG TAGQ  
> *I can't seem to forget those unpleasant memories.*

> **&&&&ai varos'pe roc ansa;**  
> \[aʔaʔaʔaʔˈai vaˈɹospe ɹok ˈansa\]  
> REDUP~REDUP~REDUP~REDUP~but life-DAT.SG=POSS.1 BENEFACTIVE fight-1SG  
> *B-b-b-b-but I'm fighting for my life,*

> **&&pevaracraþas cemi've roc lercþa.**  
> \[pepepevaɹaˈkɹaθas keˈmive ɹok ˈleɹx͡θa\]  
> REDUP~REDUP~1SG-remember-REL.CEL.ACC,DAT self-DAT.SG=POSS.2 BENEFACTIVE struggle_against-1SG  
> *and I'm s-s-struggling for you, whom I still remember.*

> **drołecþe ašir eslasos anasos vonat ħara; cere fose.**  
> \[dɹoˈɬex͡θe aˈʃiɹ esˈlasos aˈnasos voˈnat ˈʕaɹa keɹe fose\]  
> tear-INST.SG place_above-GEN.SG wide-REL.TER.NOM,DAT sky-DAT.PL look_at-INF again-1SG live-1SG because  
> *With a tear, I look at the vast skies above, because I'm alive.*

> **ondis'moc &&&cerit'pe corþin varime**  
> \[ondisˈmok kekekekeˈɹitpe ˈkoɹθin vaˈɹime\]  
> now-LOC.SG=also REDUP~REDUP~REDUP~live-INF=POSS.1 certainty-ACC.SG be_one_of-and  
> *I'm sure that I'm still alive, and*

> **&amnona'pe mel nâsmjo as feŋit amðat nedo**  
> \[aʔamnoˈnape mel na̰sˈmjo as feˈŋit amˈðat ˈnedo\]  
> mind-PROL.SG=POSS.1 much scar-NOM.PL INF.ABL fade_away-INF not_yet-INF despite  
> *even though all the scars through my mind has yet to fade away,*

> **łeŋcþoc lirna šjorcel anoc it amîr eþit**  
> \[ɬeŋˈx͡θok ˈliɹna ʃjoɹˈkel aˈnok it ˈamḭɹ eˈθit\]  
> clear-REL.TER.NOM,SEMBL.I bright-REL.CEL.NOM,NOM summer_solstice-GEN.SG sky-SEMBL.I.SG INF.SEMBL.II future-NOM.SG exist-INF  
> *I strive for a bright future like the clear midsummer sky*

> **osa endricþerin mečit senla.**  
> \[ˈosa endɹiˈx͡θeɹin meˈtʃit ˈsenla\]  
> PR.2SG.ALL exit_place-ACC.SG look_for-INF try_to-1SG  
> *and search for the exit to you.*

> **ša pelamagreþa sividin trešin lê ŋgašidap·êstis vara?**  
> \[ʃa pelamaˈɡɹeθa siˈvidin ˈtɹeʃin lɛ̰ ŋaʃidaˈfɛ̰stis ˈvaɹa\]  
> INT 1SG-APPL.ALL-escape-REL.CEL.DAT,NOM coward-GEN.SG garden-ACC.SG this.CEL (this\\)shadow-country-NOM.SG be_one_of-3SG  
> *Is this land of shadows to which I've escaped called a coward's sanctuary?*

> **tê ðêþê tomora os inôlista'ce t·lišilit os gðelrit neħrit amarłe.**  
> \[tɛ̰ ˈðɛ̰θɛ̰ toˈmoɹa os inɔ̰lisˈtake θliʃiˈlit os gðelˈɹit neˈʕɹit aˈmaɹɬe\]  
> that.CEL occurrence-LOC.SG fear-ABL.PL INF.DAT personal_problem-ABL.PL=and (INF\\)scurry-INF INF.DAT excuse-INF have_conscience_to-INF ponder-1SG  
> *I wonder if it's even right to have scurried from my fears and personal problems then.*

> **nemasôþa veltas ašir inoras nemename ðêsaŋ novas narvale.**  
> \[neˈmasɔ̰θa ˈveltas aˈʃiɹ iˈnoɹas nemeˈname ðɛ̰ˈsaŋ ˈnovas naɹˈvale\]  
> star-ABESSIVE.PL murky-REL.CEL.NOM,DAT place_above-GEN.SG void-DAT.SG stare_in_fascination-and occurrence-LOC.DU two.LOC sigh-1SG  
> *Gazing at the murky starless void above, I let out two sighs.*

> **careþime šino carracrape so'moc ša varacraspe? cerecas?**  
> \[kaɹeˈθime ˈʃino kaɹːaˈkɹape soˈmok ʃa vaɹaˈkɹaspe keˈɹekas\]  
> TERMINATIVE-exist-and all-NOM.SG TERMINATIVE-know-3SG-1SG if=also INT remember-2SG-1SG not_know-2SG  
> *Even if I disappear, forgotten by everyone else, will you remember me or not?*

> **as ħelit mîr cevos'pe domenat pentat ħapave.**  
> \[as ʕeˈlit mḭɹ keˈvospe domeˈnat penˈtat ʕaˈpave\]  
> INF.ABL do_this-INF after face-DAT=POSS.1 CAUS-see-INF able_to-1SG I_wonder-1SG-2SG  
> *Could I even show you my face after doing this?*

> **telten nalda eči eclje.**  
> \[ˈtelten ˈnalda ˈetʃi ˈeklje\]  
> cliff-GEN.SG edge-NOM.SG there.LOC far_from-3SG  
> *The edge of the cliff is there, far away.*

> **olasta; tê noldela mitrime mitrime marten os alinon c·ehit jonas roče.**  
> \[oˈlasta tɛ̰ nolˈdela miˈtɹime miˈtɹime ˈmaɹrten os aˈlinon eˈxit ˈjonas ˈɹotʃe\]  
> in_addition that.CEL edge-ALL.SG run-and run-and last-GEN.SG INF.DAT destination-ACC.SG arrive-INF now-LOC.SG imagine-1SG  
> *And I'm imagining myself running and running to that edge to arrive at the destination.*

> **ša nema?**  
> \[ʃa ˈnema\]  
> INT any-NOM.SG  
> *Is anyone there?*

> **ericinsa łirlen senčjan cecþełimeca sončjarsa šinen ariga ceła.**  
> \[eɹiˈkinsa ˈɬiɹlen ˈsentʃjan kex͡θeɬiˈmeka sonˈtʃjaɹsa ˈʃinen aˈɹiɡa ˈkeɬa\]  
> ice-ABL.SG light-GEN spark-ACC.PL shoot_out-but spark-ABL.PL all-GEN warmth not_exist-3SG  
> *Although sparks of light shoot out from the ice, they have no warmth to them.*

> **dotračan sohorime minaħas cjanan'moc cemi'pe roc caðrit penta tano ceła.**  
> \[doˈtɹatʃan soxoˈɹime miˈnaʕas kjananˈmok keˈmipe ɹok kaˈðɹit ˈpenta ˈtano ˈkeɬa\]  
> CAUS-be_sad-REL.CEL.NOM,ACC be_tired-and be_alone-REL.CEL.NOM,DAT song-ACC.SG=also self-DAT.SG=POSS.1 BENEFACTIVE recite-INF able_to-REL.CEL.NOM,NOM bird-NOM.SG not_exist-3SG  
> *There are no birds to recite even a sad song for my weary and lonesome self.*

> **fjênþa sividir'til vara en annôrþora magrihe anagrat cþîso ceła fose**  
> \[fjɛ̰nθa siviˈdiɹtil ˈvaɹa en anːɔ̰ɹˈθoɹa maˈɡɹixe anaˈɡɹat x͡θḭˈso ˈkeɬa ˈfose\]  
> honour-ABESSIVE.SG coward-SEMBL.II.SG=SEMBL personality-GEN.SG INF.GEN evil_spirit-ABL.PL escape-or hide-INF nearby_place-NOM.SG not_exist-3SG because  
> *Because there's no place near to escape or hide from the spectres of my disposition like a dishonourable coward,*

> **«cþîmin'pe» esmirłime čirname šinen annêrþanon atrecit łana.**  
> \[x͡θḭˈminpe esmiɹˈɬime tʃiɹˈname ˈʃinen anːɛ̰ɹˈθanon atrɹeˈkit ˈɬana\]  
> sword-ACC.SG=POSS.1 INCHOATIVE-grasp-and be_brave-and all-GEN.SG evil_spirit-ACC.SG conquer-INF must-1SG  
> *I must take my 'sword' with courage and conquer them all.*

> **refa cþîm as pelca telit nedo tê šjomeþas târgit pentalo fose**  
> [ˈɹefa x͡θḭm as ˈpelka teˈlit ˈnedo tɛ̰ ʃjoˈmeθas ta̰ɹˈgit penˈtalo ˈfose]  
> true-REL.CEL.NOM,NOM INF.ABL PR.1SG.INST not_exist-INF despite that.CEL end-LOC.SG defeat-INF able_to-1SG-3PL because  
> *Although my real sword is not with me, I can end up victorious,*

> **elen donemrime tê neðas «meðen miþpiłes» notat peče þal.**  
> \[ˈelen donemˈɹime tɛ̰ ˈneðas ˈmeðen miθˈpiɬes noˈtat ˈpetʃe θal\]  
> sun-ACC.SG CAUS-drown-and that.CEL attract-REL.CEL.NOM,DAT night-GEN.SG sphere_of_influence-DAT.SG walk-INF avoid-1SG ASSERT  
> *and because of that, I won't walk into the dominion of the tempting night that drowns the sun.*

> **ondis šino þônon'pe tlečit rjote þal.**  
> \[ˈondis ˈʃino θɔ̰ˈnonpe tleˈtʃit ˈɹjote θal\]  
> now-LOC.SG all-NOM.SG intention-ACC.SG stop-INF fail-3SG ASSERT  
> *Now nothing can stop me.*

> **&&&&ea varos'pe roc ansa;**  
> \[eʔeʔeʔeˈʔea vaˈɹospe ɹok ˈansa\]  
> REDUP~REDUP~REDUP~REDUP~thus life-DAT.SG=POSS.1 BENEFACTIVE fight-1SG  
> *S-s-s-s-so I'm fighting for my life,*

> **&&pevaracraþas cemi've roc lercþa.**  
> \[pepepevaɹaˈkɹaθas keˈmive ɹok ˈleɹx͡θa\]  
> REDUP~REDUP~1SG-remember-REL.CEL.ACC,DAT self-DAT.SG=POSS.2 BENEFACTIVE struggle_against-1SG  
> *and I'm s-s-struggling for you, whom I still remember.*

> **pevescþasos ašir łirlen anasos vonat ħara; mena fose.**  
> \[pevesˈx͡θasos aˈʃiɹ ˈɬiɹlen aˈnasos voˈnat ˈʕaɹa ˈmena ˈfose\]  
> 1SG-be_beautiful-REL.TER.NOM,DAT place_above-GEN.SG light-GEN.SG sky-DAT.SG look_at-INF do_again-1SG see-1SG because  
> *I look at the beautiful sky of light above again, because I can see.*

> **vlinan drêniþ &&&cþaso eseþit ħare;**  
> \[ˈvlinan dɹɛ̰ˈniθ x͡θax͡θax͡θaˈx͡θaso eseˈθit ˈʕaɹe\]  
> dawn-ACC.SG wake_up-SER REDUP~REDUP~REDUP~gradient-NOM.SG INCHOATIVE-exist-INF do_again-3SG  
> *Dawn breaks, and the gradient appears again,*

> **&amnona'pe mel nâsmjo as feŋit amðat nedo**  
> \[aʔamnoˈnape mel na̰sˈmjo as feˈŋit amˈðat ˈnedo\]  
> REDUP~mind-PROL.SG=POSS.1 much scar-NOM.PL ABL fade_away-INF not_yet-INF despite  
> *And although many scars through my mind have yet to fade away,*

> **omîs tforen nirþen cfjoþes dełir ceŋac miłen os c·evac searnit amče se.**  
> \[ˈomḭs ˈtfoɹen ˈniɹθen ˈkfjoθes deˈɬiɹ keˈŋak ˈmiɬen os eˈvak seˈaɹnit ˈamtʃe se\]  
> future-LOC.SG yellow-GEN.SG willow-GEN.SG leaf-LOC.PL to_side REFL-INST.DU grass-ACC.SG INF.DAT (INF.DAT\\)PR.1.INCL-NOM.DU sit-INF hope-1SG MIRATIVE  
> *I hope that in the future, we can sit on the grass beside the young willow leaves.*

> **cintjel paða dira; vânot vargra avona erca. ša penjas ve?**  
> \[kinˈtjel ˈpaða ˈdiɹa va̰ˈnot ˈvaɹɡɹa aˈvona ˈeɹka ʃa ˈpenjas ve\]  
> morning-GEN other-NOM.SG occur-3SG usual_case-SEMBL.II.SG stay_still-REL.CEL.NOM,NOM wind-NOM.SG cold-3SG INT where-LOC.SG be_at.1SG  
> *Another morning arrives and the still wind is cold as usual. Where am I?*

> **virerþ'pe os f·eŋit mârime veltasos lêcþen anasor lemen'pe dosare.**  
> \[viˈɹewθpe os eˈŋit ma̰ˈɹime velˈtasos lɛ̰x͡θen aˈnasoɹ leˈmenpe doˈsaɹe\]  
> lethargy_after_sleeping-NOM.SG=POSS.1 INF.DAT (INF\\)fade_away-INF wait-and murky-REL.TER.NOM,DAT grey-GEN.SG sky-DAT.SG head-NOM.SG=POSS.1 CAUS-turn_toward-1SG  
> *Waiting for myself to recover from my sleep, I turn my head toward the murky grey sky.*

> **ša oraþa šino pecimreþan cimjan? ša tê mivo criþjâli?**  
> \[ʃa oˈɹaθa ˈʃino pekimˈɹeθan ˈkimjan ʃa tɛ̰ ˈmivo ˈkɹiθja̰li\]  
> INT opine-REL.CEL.ACC.NOM all-NOM.SG 1SG-tell_lie-REL.CEL.DAT.ACC lie-ACC.PL INT that.CEL word-NOM.PL mislead-3PL  
> *Is everything I thought my own lies? Are those words misleading me?*

> **avona seðetaþ linelšas liines cemat tersat denecape.**  
> \[aˈvona seðeˈtaθ liˈnelʃas liˈines keˈmat teɹˈsat deneˈkape\]  
> wind-NOM.SG silent-SERIAL answer-REL.CEL.NOM.DAT answer-DAT.SG give-INF refuse_to-INF continue-3SG-1SG  
> *The silent wind still refuses to answer my questions.*

> **«tê nalen gcelcer gašjodos es cerit senciradis ilra viþca» ne as marit'pe nedo**  
> \[tɛ̰ ˈnalen ɡelˈkeɹ ɡaˈʃjodos es keˈɹit seɲciˈɹadis ˈilɹa ˈviθka ne as maˈɹitpe ˈnedo\]  
> that.CEL deity-GEN.SG building-GEN.PL shadow-LOC.PL inside remain-INF overcome-AGT.ANIM-DAT.SG unfit-3SG COND QUOT.ACC INF.ABESSIVE say-INF=POSS.1 despite  
> *Although I say to myself, 'It wouldn't be of an overcomer to keep hiding in the shadows of that deity's buildings',*  

> **vargrime lê ŋgašido it veliŋ'pe maregit tamel cerþanon irłime nosesa**  
> \[vaɹˈɡɹime lɛ̰ ŋaˈʃido it veˈliŋpe maɹeˈɡit taˈmel keɹˈθanon iɹˈɬime noˈsesa\]  
> stay_still-and this.CEL shadow-NOM.PL INF.SEMBL.II leg-ACC.DU=POSS.1 pull-INF fear-GEN.SG remnant-ACC.SG feel_inside-and water-ABL.SG  
> *I freeze still, feeling a remnant of fear as if this shadow were pulling on my legs,*

> **lêvonor risogor gcreše; asmelrit ħarit cenmira. ivotila tenrime anelše.**  
> \[lɛ̰voˈnoɹ ɹisoˈɡoɹ ˈɡɹeʃe asmelˈɹit ʕaˈɹit kenˈmiɹa ivoˈtila tenˈɹime aˈnelʃe\]  
> bubble-ACC.PL 16^12-CTR.SPHERICAL PFV\\hear-1SG dream-INF do_again-INF it_seems-1SG surface_of_water-ABL.SG swim-and return-1SG  
> *and hear the countless bubbles from the water; I seem to have had another dream. I swim back to the surface.*

> **as jasmos endrit mîr poðeþa asonesa artfoþesa magriþ tfołonsa mitre.**  
> \[as ˈjasmos enˈdɹit mḭɹ poˈðeθa asoˈnesa aɹtfoˈθesa maˈɡɹiθ tfoˈɬonsa ˈmitɹe\]  
> INF.ABL dream-DAT.SG exit-INF after other-ABESSIVE memory-ABL.SG city-ABL.SG escape-SER road-PROL.SG run-1SG  
> *After I leave my dream, I flee from the city alone, running through the roads.*

> **ea lê ŋgljoþos nela cem'pe geriłime naclidime crînen osmjel paðas gcjaršeþ.**  
> \[ˈea lɛ̰ ˈŋljoθos ˈnela ˈkempe geɹiˈɬime nakliˈdime kɹḭˈnen ˈosmjel ˈpaðas ɡjaɹˈʃeθ\]  
> thus this.CEL maze-LOC.SG be_lost-REL.CEL.NOM,NOM self-NOM.SG=POSS.1 be_dizzy-and trip_over-and black-GEN dream-GEN.SG other-DAT.SG PFV\\fall-1SG-PAST  
> *Thus I, lost in this maze and disoriented, tripped and fell into another dark dream.*

> **ša mê neðas a ndrênit gleves jonos ve? emtan šino linelšit cricþa.**  
> \[ʃa mɛ̰ ˈneðas a nɹɛ̰ˈnit ˈɡleves ˈjonos ve ˈemtan ˈʃino liˈnelʃit kɹiˈx͡θa\]  
> INT which day-LOC.SG INF.LOC wake_up-INF different-REL.CEL.DAT.LOC place-LOC.SG exist.1SG that-ACC.SG all-NOM.SG answer-INF don't_know-3SG  
> *Will there ever be a day when I wake up in a different place? No one knows.*

> **ai evin elgren arefan jonas'moc minan'ot entra. calas'pe irłe.**  
> \[ai ˈevin ˈelɡɹen aˈɹrefan joˈnasmok minaˈnot ˈentɹa kaˈlaspe ˈiɹɬe\]  
> but 1SG-ESSIVE here-NOM.SG truth-ACC.SG now-LOC.SG=also one.ACC=LEQ resemble-3SG body-DAT.SG=POSS.1 feel_inside-1SG  
> *But to me, this place looks like the only truth. I can feel my body inside myself.*

> **ai lê genêl sar «širičes tecto refat cenmiresa crjoþþarsa magrime clačit šonale» reþ doħorape.**  
> \[ai lɛ̰ ˈɡenɛ̰l saɹ ʃiˈɹitʃes ˈtekto reˈfat kenmiˈɹesa kɹjoˈθːaɹsa maˈɡɹime klaˈtʃit ʃoˈnale ɹeθ doʕoˈɹape\]  
> but this.CEL land-GEN something-NOM.SG eternity-DAT.SG before true-INF it_seems-REL.CEL.NOM.ABL illusion-ABL.SG escape-and break-INF do_entirely-1SG-3SG QUOT.ACC.IND convince-3SG-1SG  
> *But something in this land convinces me that I'll eventually escape this illusion and shatter it into pieces.*

> **meðomes łercþiel tforen łirlon maðo;**  
> \[meˈðomes ɬeɹx͡θiˈel ˈtfoɹen ˈɬiɹlon ˈmaðo\]  
> nighttime-LOC.SG firefly-NOM.PL yellow-GEN.SG light-ACC.PL emit-3PL  
> *The fireflies glow yellow in the night;*

> **as gelgranen gcehit eloþ meðo'ce flenvil.**  
> \[as ɡelˈɡɹanen ɡeˈxit eˈloθ meˈðoke flenˈvil\]  
> INF.ABL (INF\\)here-ACC.SG (INF\\)arrive-INF daytime-NOM.PL night-NOM.PL-and 256-1  
> *it has been 257 days and nights since I've arrived here.*

> **ša tê neðesta mîr cenčon'pe pen? ilotas ceþas cjašit vrasaþ.**  
> \[ʃa tɛ̰ neˈðesta mḭɹ kenˈtʃonpe pen iˈlotas ˈkeθas kjaˈʃit vɹaˈsaθ\]  
> INT that.CEL day-ABL.PL after name-NOM.SG=POSS.1 what far_past-LOC.SG lake-DAT fall-INF must.EPISTEMIC-3SG-PAST  
> *After all those days, what is my name? I must have dropped it into the lake.*

> **omtas sêna ðês nêmsaevanin monas þiħaþ**  
> \[ˈomtas sɛ̰ˈna ðɛ̰s nɛ̰msaeˈvanin ˈmonas θiˈʕaθ\]  
> that-LOC.SG above occurrence-LOC.SG sky\*-ACC.SG one.LOC peer-SER  
> *Looking at the night sky yet again,*

> **nemasôna somaŋa'pe mivan cþerit iħele.**  
> \[neˈmasɔ̰na somaˈŋape ˈmivan x͡θeˈɹit iˈʕele\]  
> star-PROL.PL finger-INST.SG=POSS.1 word-ACC.SG write-INF pretend_to-1SG  
> *I pretend to write a word through the stars with my finger.*

> **ea gcircjôl motola ineþanaðasa menat amðeþa sennemsâ relcra.**  
> \[ea ˈɡiɹkjɔ̰l moˈtola ineθanaˈðasa meˈnat amˈðeθa seˈnːemsa̰ ˈɹelkɹa\]  
> thus mountain-GEN.PL backside-ALL.SG white-blue-REL.CEL.NOM,NOM see-INF not_yet-REL.CEL.DAT,NOM meteor-NOM.SG fly-3SG  
> *Then a bright blue meteor that I've yet to have seen flies behind the mountains.*

> **ša lê dosilvime vinaša enôr pen?**  
> \[ʃa lɛ̰ dosilˈvime viˈnaʃa ˈenɔ̰ɹ pen\]  
> INT this.CEL CAUS-nervous-and excite-REL.CEL.NOM,NOM world-NOM.SG what  
> *What is this frightening yet exciting world?*

> **ša pena pen cêna aljas marit donevlepe?**  
> \[ʃa ˈpena pen kɛ̰ˈna ˈaljas maˈɹit doneˈvlepe\]  
> INT what-GEN.SG what-NOM.SG same-GEN.SG breath-DAT.SG say-INF CAUS-do_repeatedly-3SG-1SG  
> *Why in the world am I repeating the same syllable over and over?*

> **«le morton'pe provamas» ne isiłan narvan gcrešeþ. ša emtan pen?**  
> \[le moɹˈtonpe pɹoˈvamas ne iˈsiɬan ˈnaɹvan ɡɹeˈʃeθ ʃa ˈemtan pen\]  
> IMP hand-ACC.SG=POSS.1 hold_onto_tightly-2SG QUOT.ACC whisper-REL.CEL.NOM.ACC voice-ACC.SG PFV\\hear-1SG-PAST INT this.ANIM-NOM.SG what-NOM.SG  
> *I hear a voice whispering, 'Grab my hand.' Who is that?*

> **sennemsâ sicarrelcreþes jonos łirlen renda;**  
> \[seˈnːemsa̰ sikaɹːelˈkɹeθes ˈjonos ˈɬiɹlen ˈɹenda\]  
> meteor-NOM.SG APPL.LOC-TERM-fly-REL.CEL.DAT.LOC place-LOC.SG light-GEN.SG column-NOM.SG  
> *A pillar of light appears where the meteor landed;*

> **ea otrosac desa cajos morarþ emitraclje.**  
> \[ˈea otɹoˈsak ˈdesa ˈkajos moˈɹawθ emiˈtɹaklje\]  
> thus foot-ABL.DU below ground-NOM.SG descend-SER far\<DDT\>-3SG  
> *the ground descends below my feet, farther and farther away.*

> **arontarsa mîr amčia monon anhia'ce gcrâna.**  
> \[aɹonˈtaɹsa mḭɹ amˈtʃia ˈmonon aɲçiˈake ɡɹa̰ˈna\]  
> long_time-ABL.SG after hope-NOM.SG mind-ACC.SG joy-NOM.SG=and PFV\\fill-3SG  
> *My mind is filled with hope and joy after a long time.*

> **ša ela šinen šimeþ? ša alinen dase pen?**  
> \[ʃa ˈela ˈʃinen ʃiˈmeθ ʃa aˈlinen ˈdase pen\]  
> INT this-NOM.SG all-GEN.SG end-NOM.SG INT destination-GEN.SG next-NOM.SG what  
> *Is this the end of it all? Where will I end up next?*

> **ai lê ŋgisoris gendreþ.**  
> \[ai lɛ̰ ŋiˈsoɹis ɡenˈdɹeθ\]  
> but this.CEL undesirable_place-DAT.SG PFV\\exit-1SG-PAST  
> *But I've escaped this hell.*
