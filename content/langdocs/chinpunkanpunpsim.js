let fname = './google-10000-english-usa.txt';
let words = null;

function groupBy(l, f) {
  m = {};
  for (let e of l) {
    k = f(e);
    if (m[k] === undefined) {
      m[k] = [e];
    } else {
      m[k].push(e);
    }
  }
  return m;
}

function loadWords(callback) {
  if (words !== null) {
    callback();
    return;
  }
  const request = new XMLHttpRequest();
  request.addEventListener('load', () => {
    let contents = request.responseText;
    let list =
        contents.split('\n').map((s) => s.trim()).filter((s) => s.length > 0);
    words = groupBy(list, (s) => s.length);
    callback();
  });
  request.open('GET', fname);
  request.send();
}

function alter(b) {
  words = null;
  fname = b ? './words_alpha.txt' : './google-10000-english-usa.txt';
  if (b) {
    console.info('Using words from https://github.com/dwyl/english-words/');
  } else {
    console.info('Back to using our old list of words');
  }
}

// thx https://stackoverflow.com/a/4835406/3130218
function escapeHtml(text) {
  return text.replace(/[&<>"'#]/g, function(m) {
    return map[m];
  });
}

function randInt(n) {
  return Math.floor(n * Math.random());
}

function roll(l) {
  return l[randInt(l.length)];
}

function wordLengthDist() {
  let a = 0;
  for (let i = 0; i < 5; ++i) a += 2 * Math.random();
  return Math.max(1, Math.round(a));
}

function rollWord() {
  let k = wordLengthDist();
  while (!words[k]) k = wordLengthDist();
  return roll(words[k]);
}

function titleCase(s) {
  return s[0].toUpperCase() + s.slice(1);
}

function generateTitle() {
  let w1 = titleCase(rollWord());
  let w2 = titleCase(rollWord());
  let num = randInt(1000);
  let numAsStr = '000' + num;
  numAsStr = numAsStr.slice(numAsStr.length - 3);
  return `${w1}${numAsStr}:${w2}`;
}

const substitutions = {
  'a': ['Å', 'α'],  // angstrom symbol
  'b': ['Б', 'β'],
  'd': ['δ'],
  'e': ['Ё', 'ё', 'Σ'],
  'h': ['н'],  // Never mind that it makes the /n/ sound; deal with it.
  'n': ['И', 'Й', 'П', 'Л', 'и', 'й', 'п', 'л', 'η'],
  'o': ['σ'],
  'r': ['Я', 'я'],
  'y': ['γ'],
};

const decorations = '±∞≓≥∴≤↑→∈√';

function randomDeleteInsert(word) {
  const deleteChance = 0.05;
  const insertChance = 0.05;
  let mangledWord = '';
  for (let i = 0; i < word.length; ++i) {
    let roll = Math.random();
    if (roll < deleteChance) continue;
    let inserting = roll < deleteChance + insertChance;
    let before = randInt(2);
    if (inserting && before)
      mangledWord += String.fromCharCode(97 + randInt(26));
    mangledWord += word[i];
    if (inserting && !before)
      mangledWord += String.fromCharCode(97 + randInt(26));
  }
  return mangledWord;
}

function substitute(word) {
  let a = +(Math.random() < 0.12);
  for (let i = 0; i < 3; ++i) a += +(Math.random() < 0.01);
  for (let i = 0; i < a; ++i) {
    let index = randInt(word.length);
    let c = word[index];
    let candidates = substitutions[c];
    if (candidates !== undefined) {
      hasSubstituted = true;
      word = word.slice(0, index) + roll(candidates) + word.slice(index + 1);
    }
  }
  return word;
}

function decorate(word) {
  if (Math.random() < 0.02) {
    let index = randInt(word.length + 1);
    return word.slice(0, index) + roll(decorations) + word.slice(index);
  }
  return word;
}

function generateEnglishyWord() {
  let word = rollWord();
  word = randomDeleteInsert(word);
  let a = 0;
  for (let i = 0; i < 10; ++i) a += 0.07 * Math.random();
  a = Math.pow(a, 2) + 0.1;
  let mangledWord = '';
  for (let i = 0; i < word.length; ++i) {
    let c = word[i];
    let d = c;
    if (Math.random() < a) {
      d = String.fromCharCode(97 + randInt(26));
    }
    mangledWord += d;
  }
  mangledWord = substitute(mangledWord);
  mangledWord = decorate(mangledWord);
  return mangledWord;
}

function generateLine() {
  let numWords = 1 + randInt(4) + randInt(4);
  let line = '';
  for (let i = 0; i < numWords; ++i) {
    if (i != 0) line += ' ';
    line += generateEnglishyWord();
  }
  return line;
}

function anus() {
  loadWords(() => {
    let diarrhoea = document.getElementById('diarrhoea');
    let title = generateTitle();
    let html = `<h2>${escapeHtml(title)}</h2>`;
    html += '<div>';
    for (let j = 0; j < 4; ++j) {
      for (let i = 0; i < 4; ++i) {
        html += generateLine();
        html += '<br>';
      }
      html += '<br>';
    }
    html += '</div>';
    diarrhoea.innerHTML = html;
  });
}

console.info('Use the `alter` function to toggle enlightenment');
