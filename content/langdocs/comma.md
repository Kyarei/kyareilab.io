---
title: "The curious case of the English comma"
---

# The curious case of the English *comma*

*Translator's note: here, English examples are in bold, while Ŋarâþ Crîþ terms will be in italics.*

*Glosses are in Ŋarâþ Crîþ for extra amusement; where a gloss uses a special glossing symbol, I have chosen to use the English abbreviation.*

For most of the *dono* English has an equivalent: **. ; ? !** are the equivalents of the *gen*, *tja*, *šac* and *cjar*, respectively, while **" "** correspond to the *fos* and *þos*. **’** is used in some cases, roughly corresponding to the *ŋos* of Ŋarâþ Crîþ, while the *łil*, *rin* and *cin* lack any equivalent in English. However, English uses several *dono* that are absent in Ŋarâþ Crîþ, most notably the **comma**, which looks like the following: **,**

It is clear that the **comma** is different from a *gen*, as it is ungrammatical to end a sentence with it; it is also different from a *tja*, as seen below:

(Ung.) **This is a golden ring, my mother gave it to me when I was young.**

Instead, a **;** is used in this case:

**This is a golden ring; my mother gave it to me when I was young.**  
ela censit.1.3SG mina.1 erłen sarta; cema'pe cemat.PAST tfoso cem'ac NELSER cemen'pe ÐÊS cem'pe censit.1.1SG.PAST nôrit [^1]  
*ela erłen sarta; a nôrit'pe tfoso gcemeleþ.*

Another possibility is to use a conjunction[^2]:

**This is a golden ring, <u>and</u> my mother gave it to me when I was young.**

In this case, the **comma** is used before the conjunction. However, when the second clause lacks an explicit subject, the **comma** is omitted:

**The squirrel picked up an acorn and climbed a tree.**  
lê.1 malin nerlit-PAST B«nerlit» mina.1 vricos ’ce sênnotat-PAST mina.1 ener [^3]  
*malin vricon nerlime enen sênnoteþ.*

Special independent clause phrases in English also behave unusually: while it is grammatical to have them alone in a sentence (consider **Yuck!** *glac!* or **Tom!** *navas# \*#tom!*), they cannot be separated by semicolons from other independent clause phrases:

(Ung.) **Tom; can you wash the clothes?**  
(Ung.) **Wash the clothes; please.**  
(Ung.) **In addition; two of the pigs have died.**  
(Possibly ungrammatical) **Yuck; there is mould on the bread!**

Rather, a comma is used in these cases:

**Tom, can you wash the clothes?**  
\*#tom pentat cem've nirłit lê.1 selto  
*\*#tomi; selton nirłit pentes?*

**Wash the clothes, please.**  
nirłit lê.1 selto da  
*le selton nirłas da.*

**In addition, two of the pigs have died.**  
olasta nefa SRANA lê.1 merca-PL ŊAČAT dranlit-PPTCP  
*olasta; mercac nefa ndranlanta.*

**Yuck, there is mould on the bread!**  
glac emgren censit.1.3SG goroco čil lê.1 lava  
*glac; lovas čil goroco veła!*

These examples indicate that 'special independent clause phrases' are different from independent clause phrases in English.

Another curiosity is the *so*-clause, some of whose examples in English are given:

**If more of our pigs die, we won't have any food left.**  
so daþa SRANA gcemo'pe merca-PL dranlit cemar'pe OMÎS-AÞ MIRŁIT nema têŋ cerit-PPTCP  
*ndaþon merco'pe ndranlo so šinen têŋ carmirła.*

**Because you don't have important decisions to make, you'll surely find your craft.**  
fose cem've INORA-AÞ MIRŁIT vandat sôm-PL NELSER remat cem've-OMÎS corþa-RÞ mečit cema've cþiremo  
*vandos sôm ceła fose os cþiremon've m·ečit've mene.*

**When I looked through the window, I noticed something floating in the air.**  
pena\_ontas cem'pe varmenat-PAST NASO lê.1 celmas cem'pe menvaðit.1-PAST sar movit-PTCP es lê.1 avona  
*a gcolmasna varmenat'pe avonena movaþa pen menvaðapeþ.*

Note that the *so*-word appears at the beginning of the clause, rather than at the end as in Ŋarâþ Crîþ. In all cases above, a **comma** is used after the *so*-clause. There is an analogous pattern with prepositional phrases modifying clauses:

**After the long battle, only the injured warrior remained.**  
mîr lê.1 arantar circþîm REDEN lê.1 anljat-PPTCP noršidir cerit  
*cereþan aranten circþimînsa mîr anljaŋ noršidir šinos censaþ.*

A *so*-clause or adverbial prepositional phrase can be placed after the main clause as well, in which case the **comma** is omitted:

**We won't have any food left <u>if more of our pigs die.</u>**  
**You'll surely find your craft <u>because you don't have important decisions to make.</u>** *«c»*  
**I noticed something floating in the air <u>when I looked through the window.</u>**  
**Only the injured warrior remained <u>after the long battle.</u>**

However, data collected from +merlan #flirora[^4] indicates that for most writers, the **comma** can, in fact, be placed before **because** in sentence *«c»*; in fact, it is preferred to have the **comma** at that position than not.

**You know nothing<u>,</u> because you're only a kid.** *«e»*  
cem've racrit ces.1 fose cem've-censit.1 REDEN mina.1 ferna  
*fernan varas fose šinon cerecas.*

Similarly, in sentence *«e»*, the **comma** is preferred, although not as strongly as in *«c»*. Further study is required to find out why postposed **because** might trigger a **comma**, while postposed *so*-clauses without **because** do not.

Barring this oddity, however, one can notice that both 'special independent clause phrases' and *so*-clauses are dependent clauses that modify an entire clause. That is, in English, vocatives and interjections are prototypically dependent clauses that happen to be able to stand alone in a sentence. However, this explanation is unsatisfactory because it does not account for **comma** usage in postposed clauses of this type:

**Wash the clothes<u>, please.</u>**  
**We won't have any food left <u>if more of our pigs die.</u>**

In addition, the **comma** is used for other purposes than separating a dependent clause modifying another clause. A nonexhaustive list of such uses is shown below:

Conjunction of three or more items:  
**I bought an apple, a chair and a plum.**  
cem'pe vaðit.PAST mina.1 nemir mina.1 njantos ’ce mina.1 jatol  
*nemin njanton'ce jaten'ce vaðeþ.*  
(Some writers also place a **comma** before the **and**.)

Clarifying the status of a referent like a shadow:  
**I come from Veanse, a city high in the mountains.**  
cem'pe nelsit.paðere ŊÊLIS @veanse mina.1 artfaþo ragit es lê.1 cercêl-PL  
*corcôr es ragirþ vełen artfaþen @veanse tectesto'pe.*

**My sister, <u>who goes to the school by the river,</u> is wearing a new cloak.**  
cema'pe armo penna nelsit-3SG NELSER lê.1 sarałes djorłas lê.1 eltes celsit.1.3SG pelčit-PTCP mina.1 nafit cačraþ  
*eltin dirłen sarałeþa ninła armo'pe nafan cačran pelča.*  
(The writer has only one *armo*, so the underlined clause could be omitted without losing information. If there were multiple *armo*, such that the underlined clause would be essential, then the **commas** would be omitted.)

Between two attributive verbs modifying the same noun in some cases:  
**This is the old, creaky chair.**  
ela celsit.1.3SG lê.1 sildrit ihegit njantos  
*ela sildrime ihegos njantos.*

As seen from the examples above, the **comma** appears to lack any unifying trigger. A complicating factor is that in casual writing, many of the rules of punctuation are flouted; for instance, instead of **Tom, can you wash the clothes?**, one might write (ung.) **tom can you wash the clothes**. Further study is required to ascertain what underlying cause licenses the **comma**, if there is any.

<div style="text-align: right">+menþe #seþalor</div>
<div style="text-align: right"><i>Translated by +merlan #flirora</i></div>

[^1]: **mother** can mean either *melco* or *tfoso* depending on the gender of oneself. There are many kinship terms that act this way in English.
[^2]: Note that English uses the same set of conjunctions, regardless of whether nouns or verbs are being conjoined.
[^3]: **pick up** has a meaning that cannot be inferred from the constituent parts – namely *sênnotat*.
[^4]: A lanky, eccentric person with black hair, in fact.
