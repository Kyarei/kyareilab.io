---
title: "#flirora's conlangs"
---

# \#flirora's conlangs

[Link to grammars.](https://gitlab.com/Kyarei/uruwi-clongos/)

Check out my new article, "[The reason of Ŋarâþ Crîþ's existence](reason4v7.html)"!

## Essays and such

[The reason of Ŋarâþ Crîþ's existence](reason4v7.html)

[Enough with the fake conlangs!](sedplague.html)

[The curious case of the _comma_](comma.html)

### The road to Ŋarâþ Crîþ v9

[**The Ŋarâþ Crîþ v9 website**](http://ncv9.flirora.xyz/)

[Fabricking a path toward ŊCv9](levian4/voldethonk.html)

[Documenting ŊCv9](levian4/documenting.html)

[šinteselneri ("The Dative Case")](levian4/story.html)

## Songs in conlangs

[<span lang="art-x-v3-arx0as" class="hacm">kalte e blenz</span>](/geiriau/kalte-e-blenz.html)

[<span lang="art-x-v3-flicrinc">cerecaþa artfaþo</span>](/geiriau/test6.html)

## Nifty conlang gadgets

[The cþ-date](levian4/cthdate.html)

[Phonorun reduction utility for Middle Rymakonian](7_1gadgets/shuffle.html)

## Things about conlangs made by other people

I have [a whole page on Arka](/arkastuff/index.html). Also see [translations of a lot of things about Arka](/estel/index.html).

## Languages of the Domains

### Domain I

*   [5] **Ḋraħýl Rase** [nɹaˈħil ˈɹase] – the language spoken in Ḋraħyn-Nŷr. One of the few languages that has a speaker-referent pronoun system. Fairly rich morphology, but nothing truly unusual.
*   [5_1] A sister language of Ḋraħýl Rase with an unusual syllable structure.
*   [6] **Varta Avina** [varta avina] (Kavinan) – analytic SVO/NA/Pr language with intial consonant mutations and base-14 numerals. Again, nothing truly unusual.
*   [8] **Jbl** /jbl/ or [jyl] &c. – official language of Nŋln, and widely spoken elsewhere. Consonants and vowels are interchangeable, and instead of nouns and verbs, there are concretes and abstracts. Allows speakers to use whatever base they want, and there are nonconservative determiners as well.
*   [9] A work in progress with infixes, diabolical inflections and grammatical spoonerisms.

### Domain II

*   [7] **Lek-Tsaro** [lɛkˈtsaɹɔ] – spoken in Rymako thousands of years ago. Food-hole sounds are accompanied by rod signals. Features anaphoric pronouns and a tree mode, but very little recursion. Numerals are fairly unusual and impractical. Names can be like the ones of our languages, but they can also be whole clauses, or in extreme cases, multiple clauses. Uses a script inspired by one of my childhood cyphers. This languages was influenced by Isoraķatheð's works _from the start._
*   \[7\_1\] **Middle Rymakonian** _lek-rymako_ \[lɛkɹʉ̜ˈmakʌ\] – a descendant of Lek-Tsaro. In addition to many of Lek-Tsaro's quirks, Middle Rymakonian boasts phonoruns and an unnecessarily baroque quoting system.
*   \[7\_1\_1\] **Modern Rymakonian** – Currently revising to be more practical to speak, now with ∞% more mutation.

### Domain III

*   [10] **ŊþaċaḤa** [,ŋθatɬa,χa] – a phonorun-based triconsonantal root language that allows permutations of root consonants. A work in progress.

## Other languages

*   [1][2] These are both relexes that are not even worth talking about.
*   [3] **[Mođëp'öso](mp/index.html)** [moðepøso] – has an unusual (in a bad sense) phonology but little else is interesting.
*   [4] **Necarasso Cryssesa** [nekʰaɹaθʰɔː kʰɹiθʰesaː] – literally "forest language". Intended to have an elvish aesthetic, but abandoned due to verbosity and naturalism issues.
*   [4v7] Now called **Ŋarâþ Crîþ** [ŋaɹa̰θ kɹḭθ]; a lot more interesting to work with than its predecessor.
*   \[4v9\] The ninth edition ([website](http://ncv9.flirora.xyz/)).
*   [8.5] A language with many features I dislike. Tense consonants à la Korean, a politeness system and almost everything from Spanish (grammatical number being a notable feature that's absent).
*   [tanapo](tanapo/index.html)
