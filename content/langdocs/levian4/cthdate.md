---
title: "The cþ-date"
---

# The cþ-date

This date format shows the number of days since 2013/04/13, the date when the original Necarasso Cryssesa grammar was created, according to its metadata.

The format is as follows: **cþ** plus the number of days in hexadecimal (with capital letters for higher digits). For dates before cþ0, add 2<sup>32</sup> to the date number.

<script src="cthdate.js"></script>

<label for="western">Date:</label>
<input id="western" type="date" onchange="updateFromDate()">
<button id="today" onclick="setToToday()">Set to today</button>

<label for="cth">As cþ-date: cþ</label>
<input id="cth" type="text" maxlength="8" onchange="updateFromCthNum()">

*Established cþB4F*
