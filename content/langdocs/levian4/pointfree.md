---
title: "Pointed and pointfree glossing"
---

# Pointed and pointfree glossing

In "[The reason of Ŋarâþ Crîþ's existence]", I introduced the concept of "role-markerism", where "the correlation between the syntactic and semantic roles of a verb is explicitly listed in the definition". I now think this isn't the best term for it, since it suggests that there's an ideology behind it. In any case, I think there's a better term for this.

Consider this function written in Haskell[^1]:

~~~ haskell
foo x y = sin (x / y)
~~~

In this definition, the arguments (or the "points") of the function are specified explicitly. We can mechanically refactor this into the following:

~~~ haskell
foo' = (sin .) . (/)
~~~

which doesn't take explicit parameters. It's a bit obscured by the fact that (to oversimplify) `(/)` takes two parameters, but this definition says to "divide then take the sine". Okay, that was a bad use of pointfree programming. It would probably be prettier in something like Forth.

Compare these two to entries in the dictionaries of two of my conlangs with a similar meaning:

* **cîntit** *vt* (S) hits, strikes, collides with (O)
* **cytrynyd** *vt* hit, strike

Note that the former entry explicitly states the arguments of the verb in its definition, while the latter doesn't. It's as if we wrote

~~~ haskell
cîntit subject object = hit subject object
cytrynyd = hit
~~~

The definitions are semantically equivalent; pointed and pointfree glossing is a difference in the documentation of a language, not the language itself.

Before we investigate the merits of each style, let's find what existing conlangs do.

## Existing practice

It seems that pointed glossing is especially prevalent within the Japanese conlanging sphere, given that Arka adopts this convention.

For the following entries, we try to find verbs roughly meaning 

1. "to go", for a basic example, though it seems that some languages have the destination as the direct object or something crazy
2. "to like", because the subject of "to like" in English is the experiencer but the subject of 「好き」 is the stimulus
3. "to buy", because it has many participants

We don't list the full entries here for brevity. We also only cover conlangs here as they tend to have a single canonical lexicon that is usually freely available (compared to all the dictionaries for, say, Spanish).

### Pointed

* [Arka]:
  * **ke** ［動詞］yulに行く、行く、いく。移動動詞。
  * **siina** ［動詞］yulが好き、好む、yulがいい、好き
  * **tau** ［動詞］yulをiからkonでaに買う、買う、輸入する、購入する
* [Shaleian]:
  * **lan** ［動］(zi から) (ca へ)行く, 来る
  * **sâf** ［動］(e'n を)好きになる, 好む
  * **feg** ［動］(e を) (zi から)買う, 購入する
* [Lineparine]:
  * **luni'a** ［自動詞］(-'sは基準, 数値-'cに)達する, 到達する, 行く, 届く
  * **lirf** ［他動詞］((-'sは-'iを))好き, 好む, 愛する
  * **dosyt** ［他動詞］(-'sは-'iを-'c/io/falで)買う, 購入する
* [Lojban]:
  * **klama** *x*<sub>1</sub> comes/goes to destination *x*<sub>2</sub> from origin *x*<sub>3</sub> via route *x*<sub>4</sub> using means/vehicle *x*<sub>5</sub>
  * **nelci** *x*<sub>1</sub> is fond of/likes/has a taste for *x*<sub>2</sub> (object/state).
  * **erve** *x*<sub>1</sub> buys *x*<sub>2</sub> from *x*<sub>3</sub> for price *x*<sub>4</sub>
* [Toaq]:
  * **fa** \_\_\_ goes to \_\_\_ from \_\_\_.
  * **cho** \_\_\_ likes \_\_\_.
  * **jea** \_\_\_ makes a purchase; \_\_\_ buys property \_\_\_; \_\_\_ buys property \_\_\_ from \_\_\_.
* [Okuna]:
  * **hiela** \[v2\] go, move, be propelled (used of vehicles); go/travel by vehicle; take, ride (in) (a vehicle); drive, conduct, propel; row, sail. \<NOM go/move, ERG travel by vehicle, ERG take/propel NOM\>
  * **huata** \[v1\] be liked, appreciated (used esp. of people). \<ALL like NOM\>
  * **talakpa** \[v3\] give/spend money; pay (for); buy, purchase. \<ERG give money to DAT, ERG pay DAT for ALL with NOM (type of money), ERG buy ALL from DAT for NOM (amount of money), ERG spend money on ALL\>

### Pointfree

* [Sambahsa]:
  * **gwah** to go to (vtr)
  * **kam** to like
  * **kurihen** to purchase
* [Verdurian]:
  * **lädan** - vi Ag - go, depart \[LAUDAN\]
  * **šatir** - vt - like
  * **emec** - vt ø - buy [EMEC ‘say’, later ‘haggle’]
* [Vötgil]:
  * **Gof** (V): to go (on a surface); travel; move one's self.
  * **Coy** (V): to enjoy (something); like; prefer.
  * **Prt** (V): to buy; purchase.
* [Ryuuka]:
  * **nyagû-xa** 〈動詞〉 行く
  * (multiple words for "be liked", but they are all adjectives)
  * (no entry for "to buy")
* [Uwu]:
  * **ũwùv** *verb* to go, to move, to travel, to leave
  * **úwuv** *verb* to love, to like
  * (no entry for "to buy")

## Merits of each style

### Sources

For this section, I rely on the following:

* My own thoughts about this matter
* A discussion I started on CDN about this matter:

  > What are your thoughts on explicit role-marking in verb definitions?  
  > \[...\]  
  > And any interesting cases of what you do personally?
* A [Reddit thread] that I also started for more responses

### In favor of pointed glossing

#### Gives precise information for each verb in a concise manner

Pointed glossing can reduce the amount of ambiguity from verb definitions. Matzah comments that it can reduce dependence on the metalanguage in which the target language is documented.

This is particularly advantageous for the following situations:

* The target language tries to "stuff in" as many relevant participants of the action associated with a verb into its core arguments (Ŋarâþ Crîþ; logical languages such as Lojban)
* The target language has a very different role-marking policy than the metalanguage
* The target language has a complex or irregular role-marking policy (Ŋarâþ Crîþ; Okuna)

#### Helps avoid relexing

Matzah argues that pointed glossing can help avoid relexing, as the definitions are less dependent on the particulars of the metalanguage in its role-marking policy.

### In opposition to pointed glossing (in favor of pointfree glossing)

#### Not necessary for most verbs

For most verbs, the part-of-speech tag (showing the valency of the verb) and the pointfree definition should be sufficient to deduce the mapping between syntactic and semantic roles. cancrizans puts it in this way:

> if an English translation exists which employs the roles in the same way, you should use that and not confuse matters. Only introduce explicit notation if strictly necessary

#### Encourages a haphazard role-marking policy

Pointed glossing can encourage the syntactic roles to be assigned haphazardly. If you gloss verbs pointfreely, then you might have more incentive to describe the general patterns that (almost) all verbs then have to follow, such as "experiencers take the dative" and "destinations take the ablative".

#### Ignores certain questions about the "perception of verbs"

Most pointed glosses don't show constraints on things such as the agency of the subject of transitive verbs. That is, they don't show whether something like "the hammer broke the window" or "this medicine will cure you" is idiomatic or whether you have to say "someone broke the window with a hammer" or "you will be cured because of this medicine" instead. If you think of your language's role-marking policy more broadly instead of deciding it piecewise for each verb, then you might have an easier time considering questions like these.

[The reason of Ŋarâþ Crîþ's existence]: /langdocs/reason4v7.html
[Arka]: http://mindsc.ape.jp/klel/
[Shaleian]: https://ziphil.com/conlang/database/1.html
[Lineparine]: http://zpdic.ziphil.com/dictionary/lineparine
[Lojban]: https://la-lojban.github.io/sutysisku/en/#sisku/mi_badna
[Sambahsa]: http://sambahsa.pbworks.com/w/file/117418377/sambahsa-english.txt
[Verdurian]: http://www.zompist.com/verdict.htm
[Vötgil]: http://www.ostracodfiles.com/votgil/guide.html
[Ryuuka]: http://twoc.ever.jp/twoc/conlang.cgi?mode=search&user_id=ryuuka
[Toaq]: http://www.toaq.org/dictionary/
[Uwu]: https://docs.google.com/document/d/1ZV1U0S8qC6yJEi6grFO_Vq5A15lRVCLYeq_udWsC-9Y/edit
[Okuna]: http://pearson.conlang.org/download/okuna_dict_cur.pdf
[have to decide]: /estel/arka/study/147.html
[Reddit thread]: https://www.reddit.com/r/conlangs/comments/krcidu/explicit_rolemarking_in_verb_definitions_yay_or/

[^1]: Not a language I use much nowadays, but it's used here because It Works™.
