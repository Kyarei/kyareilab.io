---
title: "šinteselneri (\"The Dative Case\")"
---

# šinteselneri ("The Dative Case")

cþB9B

it refat gerat eslime łeŋcþor onos desa vesran aranten dommôvesa mîr cajel ħacan caliþ nôras seten dtanin mivon mêven lilos ton cfiþo on isiłit'ce crešiþ selas crešit geðatas lerþir avončis tecsat pente. naðasime vargres movas perþ miłe tovro'ce darniþ nalfaþes onirþa jonos veŋren celcer csano cero.

vinfit cenmiror poras þardaci ton feŋan vjantan pelčac cþîrvasos leron ineþen loras tfôric'ce cilvos nâsmis'ce ton nava viston it maldeþassa amarłit searna. gatrêr enôrimeþos cjamaŋ lê navan'aþ šino cenčanon'ac cereca fose le «\#cfiþalir» nes fenþlaþ.

enjos mîr eleþ glêmas morarþ anon ceen tforen'ce mel'ot atrês neltre.

\#cfiþalir «ela ancêr šimeþ se &{}.» ilotas peŋan ondis cevo vescþa so'moc clačit elþroþna ojorna eflo veła.

meŋco; darnas nelcac jelcosa crînen loras ton nava crîþas reltir mjołes sêna vfelča. ilaten ilotas ceren senar vêdoŋ'ce veðaþ; ondis mennen meŋco menat geðe fose šjonos cþišimine nagre. ea caðjano faras talmaþ; ai folþos tfaþos ecljes cerecaþes leanaraŋ trošis navan mečaþ. as eristat mîr sogoþas ros cerceris nelsiþ menveþ. setes movas perþ lê dtrošis enêl côþês car en eþit coþi irła.

lê neðas'moc coþ enven poðas coþi censa; ai cercerin'aþ vânasos cevoc·oþ'ac gleve.

«cirtel; cercerin cenčanon'pe racra se?» res amarła. ai carðir'moc cenčanon cereca fose le «\#eltes» nes fenþlaþ.

ea lê łeŋcþes seljos \#eltes endin ciron cereca fose searneŋ \#cfiþalin motos jorniłit deneca.

\#cfiþalir «le miþrirþ pedełir searnas; folþos eleþ more.» narva anarir coþin marimeca mjariðen'moc mara.

emtas nel pen dosilvimeca \#eltes gcencra.

\#cfiþalir «ŋarên ancaŋân a cimþrit etor jorniła. ilaten ilotas ŋarâþ gesołaþ. lê ŋarân crîþi roc remame crînþa dtelcþaþ. ondis pocamareþas ŋarâþas as glevit nedo cleþrascos censa.

aniþas senflen aþer el ŋarôn cintesnon melirnepeþ. nafan ŋarên ronon en remaþ ledomsrit nifan melirnepeþ. ai ondis ganelin lê gancâŋ senflen a šimit etor jorniła.»

tê mivon tafon it cerecit \#eltes gesevenra.

\#cfiþalir «ša «neman anljat vandraþ» ner navan ceslaþ os anljat cerje se? ea ša «a gcereþas ganelšit vpentat» nosa «cereþas a ganelšit pentat» nosa'te dêt «cereþas a ganelšit vpentat» ne pen dorefe se? ea ša «ea» nes «ša» nes'ce ton foston minan cþerit pente se? fel geðe so ša vescon pit mjovecta tafon cimþra viþca? ecljame recþetela elet eniin o linelšit aljonela tfełor.»

\#eltes «ea ša linelšit rjotesta?»

\#cfiþalir «ces; fel renda so liinon cþerit penta ai ondis o ŋarên mel gepron ðanhat paðas dovesit šonat łane. mel renas os racrit've roče. arontarna lê nifas os d·oeriłit roc tfałaveþ. ea le melirnen domitriþ ŋarên crîþa miþen paðas sałes.»

\#eltes «ea ša mê ŋôliþi cemi'pe visêrčasta?»

\#cfiþalir «ispjel cemi on eravat've racreþ fose visêrčeþ. meŋco; le cþišiminen eroc cevos elceras.»

a gelcerit \#elteþa reflit gcenmire. enin naðasas roma cfiþas romi aŋaror onos cjerantjas'ce ineþen vernen graþas'ce ondis moŋcehac menat vpente.

a geleþ morat šinen enôrimoþos čil crîna veła. tfaþin tistas ceaþtforen łirlos menat rjote.

escajasos an notat arelan \#eltes narvan gcreša.

«le anjoþas mîr seta cemic menvat ħaran.»

## Gloss and translation

{: .ilgloss}
1. it refat gerat eslime łeŋcþor onos desa vesran aranten dommôvesa mîr cajel ħacan caliþ nôras seten dtanin mivon mêven lilos ton cfiþo on isiłit'ce crešiþ selas crešit geðatas lerþir avončis tecsat pente.
2. ! it ref-at ger-at esl-ime łeŋcþ-or on-os desa vesr-an arant-en dommôv-esa mîr caj-el ħac-an cal-iþ nôr-as set-en d\\tan-in miv-on mêv-en lil-os ton cfiþ-o on isił-it='ce creš-iþ sel-as creš-it geð-atas lerþ-ir avonč-is tecs-at pent-e.
3. @ %inf.%sembl2 true-%inf truly-%inf wide-and clear-%rel.%ter.%nom,%loc sky-%loc.%sg below strong-%rel.%cer.%nom,%acc long_time-%gen.%sg heavy_rain-%abl.%sg after ground-%gen.%sg smell-%acc.%sg smell-%ser small-%rel.%cel.%nom,%dat quiet-%rel.%cel.%nom,%gen bird-%gen.%pl word-%dat.%pl rain-%gen.%sg drop-%dat.%pl %ornative leaf-%nom.%pl %inf.%acc whisper-%inf=and hear-%ser humid-%rel.%cel.%nom,%dat hear-%inf cannot-%rel.%cel.%acc,%dat spring-%gen.%sg breeze-%dat.%sg feel_by_touch-%inf able_to-3%sg
4. One could smell the fragrance of the ground under the vast, clear sky after a long period of heavy rain, hear the chirping of quiet birds and the rustling of the leaves housing tiny raindrops, and feel the moist breeze that could not be heard, as if they were actually real.

{: .ilgloss}
1. naðasime vargres movas perþ miłe tovro'ce darniþ nalfaþes onirþa jonos veŋren celcer csano cero.
2. ! naðas-ime vargr-es mov-as perþ mił-e tovr-o='ce darn-iþ nalf-aþes on-irþa jon-os veŋr-en celc-er csan-o cer-o.
3. @ blue-and not_move-%rel.%cel.%nom,%loc sea-%loc.%sg overlooking grass-%nom.%sg flower-%nom.%pl=and long-%ser grow-%rel.%cel.%acc,%loc tree-%abess.%sg place-%loc.%sg decay-%rel.%cel.%nom,%gen building-%gen.%sg fragment-%nom.%pl remain-3%pl
4. There remained remnants of a decayed building in a clearing where grass and flowers grew long, overlooking the unmoving blue sea.

{: .ilgloss}
1. vinfit cenmiror poras þardaci ton feŋan vjantan pelčac cþîrvasos leron ineþen loras tfôric'ce cilvos nâsmis'ce ton nava viston it maldeþassa amarłit searna.
2. ! vinf-it cenmir-or por-as þard-aci ton feŋ-an vjant-an pelč-ac cþîrv-asos ler-on ineþ-en lor-as tfôr-ic='ce cilv-os nâsm-is='ce ton nav-a vist-on it mald-eþas-sa amarł-it searn-a.
3. @ somber-%inf seem_to-%rel.%ter.%nom,%loc thick-%rel.%cel.%nom,%dat sleeve-%dat.%du %ornative fade_away-%rel.%cel.%nom,%acc vjanta-%acc.%sg wear-%rel.%hum.%nom,%nom thin-%rel.%ter.%nom,%dat smooth-%rel.%ter.%nom,%acc white-%gen.%sg hair-%dat.%sg arm-%dat.%du=and face-%loc.%sg scar-%dat.%sg=and %ornative human-%nom.%sg stone-%acc.%sg %inf.%sembl2 consider_important-%rel.%cel.%dat,%dat-%nrel.%sg consider-%inf sit-3%sg
4. A person with white hair, thin arms, and a scar on their somber-looking face, wearing a *vjanta* with wide sleeves and faded color, was sitting on a smooth rock, as if they were thinking about something important.

{: .ilgloss}
1. gatrêr enôrimeþos cjamaŋ lê navan'aþ šino cenčanon'ac cereca fose le «\#cfiþalir» nes fenþlaþ.
2. ! g\\atr-êr enôrimeþ-os cjam-aŋ lê nav-an='aþ šin-o cenč-anon='ac cerec-a fose le «\#cfiþalir» nes fenþl-aþ.
3. @ color-%gen.%pl landscape-%dat.%sg stare-%rel.%hum.%nom,%acc this.%hum human-%acc=%possr all-%nom.%sg name-%acc.%sg=%poss.3 not_know-3%sg because %imp (name) %quot.%dat call_with_name-1%pl
4. No one knows the name of this person, gazing at the colorful landscape, and so we call them "\#cfiþalir".

{: .ilgloss}
1. enjos mîr eleþ glêmas morarþ anon ceen tforen'ce mel'ot atrês neltre.
2. ! enj-os mîr el-eþ glêmas mor-arþ an-on ce-en tfor-en='ce mel='ot atr-ês neltr-e.
3. @ enja-%dat.%pl after sun-%nom.%sg five.%dat set-%ser sky-%acc.%sg red-%gen.%sg yellow-%gen.%sg=and many=%uniq color-%dat.%pl dye-3%sg
4. In four hours, the sun would set and paint the sky in diverse shades of red and yellow.

{: .ilgloss}
1. \#cfiþalir «ela ancêr šimeþ se &{}.»
2. ! \#cfiþalir «el-a anc-êr šim-eþ se &{}.»
3. @ (name) this.%inanim-%nom.%sg story-%gen.%sg end-%nom.%sg %mirative ...
4. \#cfiþalir: "This seems to be the end of the story..."

{: .ilgloss}
1. ilotas peŋan ondis cevo vescþa so'moc clačit elþroþna ojorna eflo veła.
2. ! ilot-as peŋan ond-is cev-o vescþ-a so='moc clač-it elþr-oþna oj-orna efl-o veł-a.
3. @ distant_past-%loc.%sg %cmp.%eq now-%loc.%sg face-%nom.%sg beautiful-3%sg if=also break-%inf do_easily-%rel.%ter.%acc,%prol bone-%prol.%pl ache-%nom.%sg exist-3%sg
4. Although their face was as beautiful now as long ago, they had an ache through their brittle bones.

{: .ilgloss}
1. meŋco; darnas nelcac jelcosa crînen loras ton nava crîþas reltir mjołes sêna vfelča.
2. ! meŋco; darn-as nelc-ac jelc-osa crîn-en lor-as ton nav-a crîþ-as relt-ir mjoł-es sêna v\\felč-a.
3. @ INTJ long-%rel.%cel.%nom,%dat short_stature-%rel.%hum.%nom,%nom sparse-%rel.%ter.%nom,%abl black-%gen.%sg hair-%dat.%sg %ornative person-%nom.%sg forest-%abl.%sg mist-%gen.%sg grass-%loc.%sg above %pfv\\walk_bristly-3%sg
4. Then a short person with long, black hair briskly approached them on the misty grass from the sparse forest.

{: .ilgloss}
1. ilaten ilotas ceren senar vêdoŋ'ce veðaþ; ondis mennen meŋco menat geðe fose šjonos cþišimine nagre.
2. ! ilat-en ilot-as cer-en sen-ar vêd-oŋ='ce veð-a-þ; ond-is menn-en meŋc-o men-at geð-e fose šjon-os cþišimin-e nagr-e.
3. @ long_ago-%gen.%sg long_ago-%loc.%sg house-%acc.%sg fire-%nom.%sg parent-%acc.%u=and destroy-3%sg-%past now-%loc.%sg left-%gen.%sg eye-%nom.%sg see-%inf cannot-3%sg because all-%loc.%sg bandage-%nom.%sg cover-3%sg
4. Long ago, a fire had destroyed their house and their parents; they were now blind in the left eye and wear a bandage over it.

{: .ilgloss}
1. ea caðjano faras talmaþ; ai folþos tfaþos ecljes cerecaþes leanaraŋ trošis navan mečaþ.
2. ! ea caðjan-o far-as talm-a-þ; ai folþ-os tfaþ-os eclj-es cerec-aþes le-anar-aŋ troš-is nav-an meč-a-þ.
3. @ therefore world-%nom.%sg often-%loc.%sg ridicule-3%sg-%past but soon-%loc.%sg town-%dat.%sg far-%rel.%cel.%nom,%loc forget-%rel.%cel.%acc,%loc 3%sg-considerate-%rel.%hum.%nom,%acc field-%loc.%sg person-%acc.%sg find-3%sg-%past
4. As a result, they were often teased by society; however, they soon found someone more considerate in a forgotten meadow far from town.

{: .ilgloss}
1. as eristat mîr sogoþas ros cerceris nelsiþ menveþ.
2. ! as erist-at mîr sog-oþas ros cercer-is nels-iþ menv-e-þ.
3. @ %inf.%abl meet_for_first_time-%inf after \*week-%loc.%sg each-%loc.%sg stranger-%dat.%sg go-%ser meet_with-3%sg-%past
4. After that initial encounter, they went to meet the stranger every week.

{: .ilgloss}
1. setes movas perþ lê dtrošis enêl côþês car en eþit coþi irła.
2. ! set-es mov-as perþ lê d\\troš-is en-êl côþ-ês car en eþ-it coþ-i irł-a.
3. @ calm-%rel.%cel.%nom,%loc sea-%loc.%sg overlooking this.%cel field-%loc.%sg world-%gen.%sg remainder-%loc.%sg outside %inf.%gen exist-%inf feeling-%dat.%sg feel-3%sg
4. They felt a sense of being separate from the rest of the world whenever they are in this field overlooking the calm sea.

{: .ilgloss}
1. lê neðas'moc coþ enven poðas coþi censa; ai cercerin'aþ vânasos cevoc·oþ'ac gleve.
2. ! lê neð-as='moc coþ-∅ env-en poð-as coþ-i cens-a; ai cercer-in='aþ vân-asos cevoc·-oþ='ac glev-e.
3. @ this.%cel day-%loc.%sg=also feeling-%nom.%sg day-%gen.%sg other-%loc.%sg feeling-%dat.%sg equal-3%sg but stranger-%nom.%sg=%possr norm-%dat.%sg facial_expression-%nom.%sg=%poss.3 differ-3%sg
4. This feeling was the same today as it had been, but the look on the stranger's face was not.

{: .ilgloss}
1. «cirtel; cercerin cenčanon'pe racra se?» res amarła.
2. ! «cirtel; cercer-in cenč-anon='pe racr-a se?» res amarł-a.
3. @ by_the_way stranger-%nom.%sg name-%acc.%sg=%poss.1 know-3%sg %rhetorical %quot.%dat consider-3%sg
4. *"By the way, does the stranger know my name?"* they thought.

{: .ilgloss}
1. ai carðir'moc cenčanon cereca fose le «\#eltes» nes fenþlaþ.
2. ! ai carð-ir='moc cenč-anon cerec-a fose le «\#eltes» nes fenþl-aþ.
3. @ but visitor-%nom.%sg=also name-%acc.%sg not_know-3%sg because %imp (name) %quot.%dat call_with_name-1%pl
4. But even the visitor did not know their own name, and so we call them "\#eltes".

{: .ilgloss}
1. ea lê łeŋcþes seljos \#eltes endin ciron cereca fose searneŋ \#cfiþalin motos jorniłit deneca.
2. ! ea lê łeŋcþ-es selj-os \#elt-es end-in cir-on cerec-a fose searn-eŋ \#cfiþal-in mot-os jornił-it denec-a.
3. @ therefore this.%cel clear-%rel.%cel.%nom,%loc afternoon-%loc.%sg (name)-%nom.%sg now-%gen.%sg action-%acc.%sg not_know-3%sg because sit-%rel.%hum.%nom,%gen (name)-%gen.%sg behind-%loc.%sg stand-%inf continue-3%sg
4. Thus \#eltes, not knowing what to do next, continued to stand behind \#cfiþalir.

{: .ilgloss}
1. \#cfiþalir «le miþrirþ pedełir searnas; folþos eleþ more.»
2. ! \#cfiþalir «le miþr-irþ pe-dełir searn-as; folþ-os el-eþ mor-e.»
3. @ (name) %imp hurry-%ser 1%sg-beside sit-2%sg soon-%loc.%sg sun-%nom.%sg descend-3%sg
4. \#cfiþalir: "Hurry up and sit next to me; the sun won't stay up for long."

{: .ilgloss}
1. narva anarir coþin marimeca mjariðen'moc mara.
2. ! narv-a anar-ir coþ-in mar-imeca mjarið-en='moc mar-a.
3. @ voice-%nom.%sg kindness-%gen.%sg feeling-%acc.%sg say-but inevitability-%acc.%sg=also show-3%sg
4. Although their voice showed a sense of kindness, it also showed one knowing exactly what would happen.

{: .ilgloss}
1. emtas nel pen dosilvimeca \#eltes gcencra.
2. ! emt-as nel pen-∅ do-silv-imeca \#elt-es g\\cencr-a.
3. @ that.%inanim-%dat.%sg regarding what-%nom.%sg %caus-fear-but (name)-%nom.%sg %pfv\\obey-3%sg
4. Something about that worried \#eltes, but they obeyed.

{: .ilgloss}
1. \#cfiþalir «ŋarên ancaŋân a cimþrit etor jorniła.
2. ! \#cfiþalir «ŋar-ên anc-aŋân a cimþr-it etor jornił-a.
3. @ (name) language-%gen.%sg story-%acc.%sg %inf.%loc convey-%inf before stand-1%sg
4. \#cfiþalir: "I am about to tell you a story about a language.

{: .ilgloss}
1. ilaten ilotas ŋarâþ gesołaþ.
2. ! ilat-en ilot-as ŋar-âþ g\\esoła-þ.
3. @ long_ago-%gen.%sg long_ago-%loc.%sg language-%nom.%sg %pfv\\exist.3%sg-%past
4. Long, long ago, a language came to be.

{: .ilgloss}
1. lê ŋarân crîþi roc remame crînþa dtelcþaþ.
2. ! lê ŋar-ân crîþ-i roc rem-ame cr-înþa d\\telcþ-a-þ.
3. @ this.%cel language-%acc.%sg forest-%dat.%sg %benefactive make-and forest-%prol.%sg %pfv\\bloom-3%sg-%past
4. This language was made for the forest and through the forest it flourished.

{: .ilgloss}
1. ondis pocamareþas ŋarâþas as glevit nedo cleþrascos censa.
2. ! ond-is po-ca-mar-eþas ŋar-âþas as glev-it nedo cleþrasc-os cens-a.
3. @ now-%loc.%sg 1%pl-%appl.%inst-speak-%rel.%cel.%dat.%dat language-%dat.%sg %inf.%abl differ-%inf despite ancestor-%dat.%sg equal-3%sg
4. Although it is not the language that we are speaking in right now, it is its ancestor.

{: .ilgloss}
1. aniþas senflen aþer el ŋarôn cintesnon melirnepeþ.
2. ! aniþ-as sen-flen aþer el ŋar-ôn cintesn-on melirn-e-pe-þ.
3. @ year-%abl.%pl two-256 ago this.%ter language-%gen.%pl line_of_succession-%acc.%sg bequeath-3%sg-1%sg-%past
4. Five hundred years ago, I inherited this chain of languages.

{: .ilgloss}
1. nafan ŋarên ronon en remaþ ledomsrit nifan melirnepeþ.
2. ! naf-an ŋar-ên ron-on en rem-aþ le-domsr-it nif-an melirn-e-pe-þ.
3. @ new-%rel.%cel.%nom,%acc language-%gen.%sg form-%acc.%sg %inf.%gen make-%ser 3%sg-rear-%inf task-%acc.%sg bequeath-3%sg-1%sg-%past
4. I was left to create and maintain a new form of the language.

{: .ilgloss}
1. ai ondis ganelin lê gancâŋ senflen a šimit etor jorniła.»
2. ! ai ond-is g\anel-in lê g\anc-âŋ sen-flen a šim-it etor jornił-a.»
3. @ but now-%loc.%sg year-%gen.%pl this.%cel story-%nom.%sg %inf.%loc end-%inf in_front_of stand-3%sg
4. Now this five hundred-year story is about to come to an end."

{: .ilgloss}
1. tê mivon tafon it cerecit \#eltes gesevenra.
2. ! tê miv-on taf-on it cerec-it \#elt-es g\\es-evenr-a.
3. @ that.%cel word-%gen.%pl meaning-%acc.%sg %inf.%sembl2 not_know-%inf (name)-%nom.%sg %pfv\\%inch-open_mouth-3%sg
4. \#eltes opened their mouth as if they did not know what those words meant.

{: .ilgloss}
1. \#cfiþalir «ša «neman anljat vandraþ» ner navan ceslaþ os anljat cerje se?
2. ! \#cfiþalir «ša «nem-an anlj-at vandr-a-þ» ner nav-an ces-laþ os anlj-at cerj-e se?
3. @ (name) %int some-%acc.%sg injure-%inf leave_undone-3%sg-%past %quot.%nom person-%acc.%sg zero-%ctr.%human %inf.%dat injure-%inf imply-3%sg %rhet
4. \#cfiþalir: "Does '*neman anljat vandraþ*' (Some people were left uninjured) imply that no one was injured?

{: .ilgloss}
1. ea ša «a gcereþas ganelšit vpentat» nosa «cereþas a ganelšit pentat» nosa'te dêt «cereþas a ganelšit vpentat» ne pen dorefe se?
2. ! ea ša «a g\\cer-eþas g\\anelš-it v\\pent-at» nosa «cer-eþas a g\\anelš-it pent-at» nosa='te dêt «cer-eþas a g\\anelš-it v\\pent-at» ne p-en do-ref-e se?
3. @ therefore %int %inf.%loc (%inf)\\house-%dat.%sg (%inf)\\return-%inf (%inf)\\able_to-%inf %quot.%abl (%inf)\\house-%dat.%sg %inf.%loc (%inf)\\return-%inf able_to-%inf %quot.%abl=or instead_of (%inf)\\house-%dat.%sg %inf.%loc (%inf)\\return-%inf (%inf)\\able_to-%inf %quot.%acc what-%nom.%sg %caus-correct-3%sg %rhet
4. And why is it 'cereþas a ganelšit vpentat' (*when one is able to return home*) instead of 'a gcereþas ganelšit vpentat' or 'cereþas a ganelšit pentat'?

{: .ilgloss}
1. ea ša «ea» nes «ša» nes'ce ton foston minan cþerit pente se?
2. ! ea ša «ea» nes «ša» nes='ce ton fost-on minan cþer-it pent-e se?
3. @ therefore %int therefore %quot.%dat %int %quot.%dat=and %ornative.%adnom sentence-%acc.%sg one-%acc write-%inf able_to-3%sg %rhet
4. And can 'ea' and 'ša' be used in the same sentence?

{: .ilgloss}
1. fel geðe so ša vescon pit mjovecta tafon cimþra viþca?
2. ! fel geð-e so ša vesc-on p-it mjov-ecta taf-on cimþr-a viþca?
3. @ %ellips cannot-3%sg if %int be_needed-%rel.%ter.%nom,%acc what-%sembl2.%sg word-%inst.%pl meaning-%acc.%sg convey-3%sg %cond
4. If not, how would the necessary meaning be expressed?

{: .ilgloss}
1. ecljame recþetela elet eniin o linelšit aljonela tfełor.»
2. ! eclj-ame recþet-ela el-et eni-in o linelš-it aljon-ela tfeł-or.»
3. @ far-and honorable-%rel.%cel.%nom,%abl this-%sembl2.%pl question-%acc.%pl %inf.%nom answer-%inf goal-%abl.%sg path-%nom.%sg
4. Distant and honorable is the goal to which such questions are the path."

{: .ilgloss}
1. \#eltes «ea ša linelšit rjotesta?»
2. ! \#eltes «ea ša linelš-it rjot-es-ta?»
3. @ (name) therefore %int answer-%inf cannot-2%sg-%past
4. \#eltes: "And you could not answer them?"

{: .ilgloss}
1. \#cfiþalir «ces; fel renda so liinon cþerit penta.
2. ! \#cfiþalir «ces; fel rend-a so liin-on cþer-it pent-a.
3. @ (name) zero %ellips want_to-1%sg if answer-%acc.%pl write-%inf able_to-1%sg
4. \#cfiþalir: "That is not true; I could come up with the answers if I wanted to.

{: .ilgloss}
1. ai ondis o ŋarên mel gepron ðanhat paðas dovesit šonat łane.
2. ! ai ond-is o ŋar-ên mel gepr-on ðanh-at pað-as do-ves-it šon-at łan-e.
3. @ but now-%loc.%sg %inf.%nom language-%gen.%sg many flaw-%acc.%pl fix-%inf another-%dat.%sg %caus-change-%inf do_completely-%inf must-3%sg
4. But now, fixing the many flaws of this language must change it into a different one altogether.

{: .ilgloss}
1. mel renas os racrit've roče.
2. ! mel ren-as os racr-it='ve roč-e.
3. @ many example-%acc.%pl %inf.%dat know-%inf=%poss.2%sg imagine-1%sg
4. I imagine that you know about many of them.

{: .ilgloss}
1. arontarna lê nifas os d·oeriłit roc tfałaveþ.
2. ! aront-arna lê nif-as os d·o-erił-it roc tfał-a-ve-þ.
3. @ long_time-%prol.%sg this.%cel task-%dat.%sg %inf.%dat %caus-prepare-%inf %benefactive teach-1%sg-2%sg-%past
4. Through all those years, I have been training you for this task.

{: .ilgloss}
1. ea le melirnen domitriþ ŋarên crîþa miþen paðas sałes.»
2. ! ea le melirn-en do-mitr-iþ ŋar-ên crîþ-a miþ-en pað-as sał-es.»
3. @ and %imp legacy-%acc.%sg %caus-continue-%ser language-%gen.%sg forest-%gen.%sg ruler-%gen.%sg next-%dat.%sg assume_role-2%sg
4. Therefore, please continue this legacy and take the role of being the next master of Ŋarâþ Crîþ."

{: .ilgloss}
1. \#eltes «ea ša mê ŋôliþi cemi'pe visêrčasta?»
2. ! \#eltes «ea ša mê ŋôl-iþi cem-i='pe visêrč-as-ta?»
3. @ (name) %int which reason-%abl.%sg self-%dat.%sg=%poss.1 prefer-2%sg-%past
4. \#eltes: "And why have you chosen me over someone else?"

{: .ilgloss}
1. \#cfiþalir «ispjel cemi on eravat've racreþ fose visêrčeþ.
2. ! \#cfiþalir «isp-jel cem-i on erav-at='ve racr-e-þ fose visêrč-e-þ.
3. @ (name) life-%gen.%sg self-%dat.%sg %inf.%acc suffer-%inf=%poss.2 know-1%sg-%past because prefer-1%sg
4. \#cfiþalir: "I chose you because I knew that life was difficult for you.

{: .ilgloss}
1. meŋco; le cþišiminen eroc cevos elceras.»
2. ! meŋco; le cþišimin-en e-roc cev-os elcer-as.»
3. @ %intj %imp bandage-%acc.%sg 1%sg-%benefactive face-%dat.%sg remove-2%sg
4. Now take off these bandanges off your face for me."

{: .ilgloss}
1. a gelcerit \#elteþa reflit gcenmire.
2. ! a g\\elcer-it \#elt-eþa refl-it g\\cenmir-e.
3. @ %inf.%loc (%inf)\\remove-%inf (name)-%dat.%sg surprise-%inf seem_to-3%sg
4. When they removed it, \#eltes looked surprised.

{: .ilgloss}
1. enin naðasas roma cfiþas romi aŋaror onos cjerantjas'ce ineþen vernen graþas'ce ondis moŋcehac menat vpente.
2. ! en-in naðas-as rom-a cfiþ-as rom-i aŋar-or on-os cjerant-jas='ce ineþ-en vern-en graþ-as='ce ond-is moŋc-ehac men-at v\\pent-e.
3. @ tree-%gen.%sg blue-%rel.%cel.%nom,%dat each-%gen.%sg leaf-%dat.%sg each-%dat.%sg warm-%rel.%ter.%nom,%loc sky-%loc.%sg butterfly-%dat.%pl=and white-%gen.%sg sand-%gen.%sg shore-%dat.%sg=and now-%loc.%sg eye-%inst.%du see-%inf %pfv\\able_to-3%sg
4. Each green leaf of each tree, the butterflies in the warm sky, and the shore of white sand – now they could see them with both of their eyes.

{: .ilgloss}
1. a geleþ morat šinen enôrimoþos čil crîna veła.
2. ! a g\\el-eþ mor-at šin-en enôrimoþ-os čil crîn-a veła.
3. @ %inf.%loc (%inf)\\sun-%nom.%sg descend-%inf all-%gen landscape-%loc.%sg on black-%nom.%sg exist.3%sg
4. When the sun set, the entire landscape was dark.

{: .ilgloss}
1. tfaþin tistas ceaþtforen łirlos menat rjote.
2. ! tfaþ-in tist-as ceaþ-tfor-en łirl-os men-at rjot-e.
3. @ town-%gen.%pl unstable-%rel.%cel.%nom,%dat red-yellow-%gen.%sg light-%dat.%pl see-%inf cannot-3%sg
4. The flickering orange lights from the towns were nowhere to be seen.

{: .ilgloss}
1. escajasos an notat arelan \#eltes narvan gcreša.
2. ! escaj-asos an not-at arel-an \#elt-es narv-an g\\creš-a.
3. @ inland-%dat.%sg %inf.%prol walk-%inf faint-%rel.%cel.%nom,%acc (name)-%nom.%sg voice-%acc.%sg %pfv\\hear-3%sg
4. As they walked inland, \#eltes heard a faint voice.

{: .ilgloss}
1. «le anjoþas mîr seta cemic menvat ħaran.»
2. ! «le anjoþ-as mîr seta cem-ic menv-at ħar-an.»
3. @ %imp year-%abl.%pl after 2^32 self-%dat.%du meet-%inf do_again-1%du
4. "Let's meet again in four billion years."
