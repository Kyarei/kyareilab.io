---
title: "Fabricking a path toward ŊCv9"
---

# Fabricking a path toward ŊCv9

Because haha funni R-word for `Identifier`

You might be wondering why there's no ŊCv8. That's because 8 is for `sed` users.

Goals of ŊCv9:

* Build on the unique aspects of Ŋarâþ Crîþ
* Sort out the fine details of syntax and semantics
* Naturally take about 200 pages in the grammar
* ***A E S Þ E T I C***

## Phonology

Even though /fʰ/ and /sʰ/ seem appealing, no plans to add new phonemes.

Add some more syllable codas (particularly /ns/ and /st/) that are reduced when not word-final.

## Nouns

### Cases

Ŋarâþ Crîþ definitely doesn't need all 12 of its cases. Some of them are rarely used because of core-packing.

| Case | Verdict |
|-|-|
| Nominative | Definitely necessary. |
| Accusative | Definitely necessary. |
| Genitive | Used extremely often; probably should keep. |
| Dative | Definitely necessary. |
| Locative | Used quite often (for both locations and time); probably keep. |
| Ablative | Not used very often; probably take out. I don't want it to even to show the cause of something, since using the causative voice on the verb is more interesting. Temporal use can be moved to an adposition anyway. |
| Allative | Mostly superseded by core arguments in movement verbs; discard. |
| Prolative | Not worth dedicating an entire case and extra hair-pulling over; discard. |
| Instrumental | Used moderately often; 50/50. |
| Abessive | Used less often than instrumental, but here because ŊC has no single way to negate, so depends on whether the instrumental is kept. |
| Semblative I | Rarely used, especially because it can't be used predicatively (until very recently). Discard and replace with an adposition. |
| Semblative II | Sometimes used on nouns, but it is also used with infinitives for the "as if" construction. Keep. |

Also, add a "generic number" to use nouns to refer to a general idea rather than one or more specific objects.

## Verbs

Adpositions can become a kind of "verb"; call them *relationals* or something. Morphologically, verbs are optimized for predicative use and relationals optimized for attributive use. They are syntactically similar, however.

|| Verbs | Relationals |
|-|-|-|
| Finite form | Finite conjugations | Attached to a "scaffolding" ("copula", if you think of it that way), which is then inflected as a verb |
| Modifying nouns | Participle forms (marked for case of shared noun in both the embedded and the main clause, as well as the gender of the shared noun in the main clause) | Close-to-bare form, perhaps marking for fewer categories than verbs |
| Modifying verbs | Converbal forms | Bare form |
| Case frame | Fixed according to the verb | Second argument is DAT by default, but can be ACC to use motion toward or ABESS to use motion away from |

For verb conjugations:

* Fix ambiguity between 1PL and 3SG-PAST
* Make participle endings not match the head case so much
* Need dedicated affixes for reflexive objects

Comparatives:

* "equal" comparatives rarely truly mean "equal"; usually, it's "greater than or equal to"
* "less than or equal to" is useful, too
