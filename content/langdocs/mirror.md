---
title: "A treatise on the language of mirrors"
---

# A treatise on the language of mirrors

**Note: this article is very work-in-progress-y.**

*Translator's note: here, English examples are in bold, while Ŋarâþ Crîþ terms will be in italics.*

*Glosses will be translated into English for your convenience here. While I appreciate the comedic factor of Ŋarâþ Crîþ glosses in English text, this text isn't about the peculiarities of English.*

In this world, there are many mirrors, reflecting an imperfect image of one object as another. To the mirrored object, the original is its imperfect reflection. The original and the reflection, the reflection and the original, are one by their relation. Zero and one; light and dark; earth and sky; *melco* and *tfoso*. Indeed, sixteen is formed from four mirrors, which in turn arise from two, which are begotten from one.

I have received a relevation from the union of the four gods \[*lenalar*\], comprising of any one of them across two mirrors, that humankind cries for a language of mirrors \[*menþin ŋarâþ*\]. To this, I shall say that I am honoured to answer this call.

The verb is the core of language. Unlike in Ŋarâþ Crîþ where it occurs at the end of a phrase, here it occurs in the middle, separating what is assumed to be known in the past from what is unknown until the future.[^1] In this language, it is often an impediment \[*divro*\] to think about 'before' and 'after'; rather, words are ordered towards and away from the verb.

Let us avoid concerning ourselves with particular words for now, using Ŋarâþ Crîþ *(English in this case)* words in their place. In the sentence '\#saþo chase \#môra', both '\#saþo' and '\#môra' relate to the action of chasing, and '\#saþo' is the entity that is being talked about. '\#môra' is the information that the listener would not be expected to know before hearing this sentence, and thus the sentence is intended to cause the listener to know that \#môra is related to the action of chasing.

To put it another way, the sentence above can be conceived of as a question and answer:

> Q: What relates to chasing that \#saþo is related to?  
> A: It is \#môra.

The exact roles that \#saþo and \#môra take are ambiguous in this sentence. The sentence above could mean '\#saþo is chasing \#môra', or '\#saþo is being chased by \#môra', or (more unusually) '\#saþo is chasing something with \#môra'. It is on the listener to interpret the sentence.

Similarly, the sentence 'chicken eat' could mean 'the chicken is eating' or 'the chicken is being eaten'.

Across the mirror from the presence of an action is its absence. Ŋarâþ Crîþ tries to hide its reality from its speakers, pretending that it does not exist with words such as *garit* and *pečit*, but it is there.

'\#saþo NOT chase \#môra' indicates that act of chasing is absent. Here, the 'NOT' binds to the verb. In this case, it is nonsensical to think of verbward and edgeward directions; we instead conventionally place 'NOT' immediately before the verb.

In 'NOT \#saþo chase \#môra', the act of chasing is not necessarily absent, but the relation between \#saþo and the chasing is. Likewise, in '\#saþo chase NOT \#môra', what is missing is the relation between \#môra and the act of chasing.

[^1]: *That is, the topic and the focus.*
