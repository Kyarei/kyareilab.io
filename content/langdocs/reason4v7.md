---
title: "The reason of Ŋarâþ Crîþ's existence"
---

# The reason of Ŋarâþ Crîþ's existence

cþAFF

With the second anniversary of Ŋarâþ Crîþ (proper) upcoming, I would like to cover the motivations of creating this language and the factors that made it as it is.

## Origins

### In the beginning, let there be names...

>     # Fucking seriously? The terrain generation is fucked up,
>     # and they add HORSES??!!

*­– comment in Necarasso Cryssesa language pack file*

Before Ŋarâþ Crîþ, there was *Necarasso Cryssesa*. This predecessor did not emerge all at once, however. Instead, the first stages of the language arose as a programmatic scheme for creating personal names. [This program](https://gitlab.com/-/snippets/2050500) is one such program, written in Racket for a high school computer programming class. (I also recall writing an earlier program on my TI-84+SE.) Characteristics of this scheme are:

* There is a small set of valid word endings. In the program linked above, they are divided into "feminine" and "masculine" endings.
* The letter \<y\> is often used as a vowel.
* The digraph \<th\> (almost certainly representing /θ/)
* The clusters \<cth\> and \<cv\>
* No /b/! Fuck /b/!

The aesthetics that I was aiming for was meant to be some sort of "elvish" aesthetic. However, all this program did was generate names; there was not yet any grammar or vocabulary set in stone.

The next step in the language's development was the creation of a [language pack] for the game *Minecraft*, starting in March 2013 and resulting in its initial publication in April. I found the process of creating one enjoyable, although manually installing them before 1.6 added resource packs was not. This stage established some basic vocabulary and grammar. In this stage, I decided to use \<ss\> instead of \<th\> for /θ/, because I considered the latter to be trite. The language was originally called *Cressia*, but it was later called *Necarasso Cryssesa*, the "forest language".

Noticing that a centralized documentation was necessary, I later created the first standard grammar of the language, *VE¹ENCS*. This initial version was bare-bones: it had two grammatical numbers, two grammatical genders, and no case marking. Verbs were marked for person and number as well as one of two moods, with tense being marked using auxiliaries. All verbs were regular, including *essyd*, glossed as 'to be' at the time. And of course, there was a section of *tabelvortoj* to wash it off. A lot of the grammatical ideas were picked from Spanish, the one foreign language that I had studied at that time.

Not long after, I became interested in Quenya because it was *literally* an elf language. This can be seen through my translation of [*Namárië*] on April 22 into my newly created conlang:

> *A ress lyre erlleva carso iss cvyssalyrys  
> ress eneryseas relcorarys cetallyroros re an pertena vencead ci  
> Iss elsserys atri nealygo ress tancarys mytro  
> eas iss esmalarys mesto es esadarys eradiona ceor iss Merseda  
> desor iss setrorys nasatesa eas Varda  
> yvores iss nesmeriarys enenero es iss narva eas er ress anoraseni
temtera*
>
> *Endyr geve cronayd nepsa venor iss tanca nas e*
>
> *Anastyr endyr iss Senadyr Varda Nesmeriaryseas Temtera  
> eas Sysonarcyn atre esasenago mortoros eas er ress rotrosarys  
> Ner metellorys syno esforo es gasydarys  
> Ner eas pestrys elecssa acrynos rylssa simios creterys yrnessa  
> Ner ecssesos nagare iss yngarorys eas Calaciria sysono  
> Endyr alyrynssygo alyrynssygo aer emtarys eas iss Elsse essa Valimar*
>
> *Tormecyn essa nagtor re refetesas Valimar  
> Essa nagtor re eo acra refetesas er tormecyn*[^1]

After completing this translation, I noticed that Necarasso Cryssesa lacked many features that Quenya had. In order to make my conlang be more like Quenya, I made some changes to its grammar and published it as *VE²ENCS* on May 29:

* the verb *essyd* became irregular
* dual number was added

The same simple rule for pluralization that existed in VE¹ENCS was retained; another rule for dual number was added but changed in *VE³ENCS* on July 7.

*VE⁴ENCS*, which was released in 2014, made the following changes:

* introduction of the verb *ollyd* (which takes an adjectival complement and is glossed as 'to be'), while *essyd* was narrowed to 'exist'
* introduction of synthetic passive verb forms (probably taken from Latin, not Quenya)
* changes to pluralization rules, now an ending change instead of a simple suffix to the singular form. Almost definitely done to make it become more similar to Quenya.

When I look at the changes now, it seems that most of the changes made to Necarasso Cryssesa as a result of Quenya envy were superficial, such as dual number but no 'partitive plural', or introducing a few irregular verbs instead of refining the entire inflection system).

### Magic letters

> runes are ugly

*– probably not many people*

The script of Necarasso Cryssesa at the time was *Cenvos*, named after its first six letters.

![A description of Cenvos as it was before ŊCv7.](cenvos_old.png){:width="100%"}

The capital letters were taken from an earlier personal script for English with runelike characters. The glyph for \<o\> was once a square with corners in orthogonal directions, but it was changed for aesthetic purposes. The glyph for \<g\> was changed because I had trouble remembering how to write it. In addition, the writing direction was changed from left-to-right to right-to-left. The lowercase versions were added later.

This script eventually inspired the modern Cenvos script used in ŊCv7.

### Exiting Spain and entering Japan

> at least your Spanish teacher is the least bit likeable right?

*– literally me on CDN*

Meanwhile, during my junior and senior years of high school[^2] from 2013 to 2015, I started struggling with Spanish classes. Some of it came from the actual material (consistently remembering to respect noun-adjective agreement and clitic doubling; dealing with false friends; idiomatic phrases), but this difficulty was exacerbated by the teacher I had for those classes. These factors, combined with the perception of Spanish as the 'illegal immigrant language', made me abhor not only Spanish class but the Spanish language itself.

Later during that period, I started gaining interest in Japanese[^3]. I found this interest quite awkward at that time, because of my Korean ancestry. Eventually, however, I gave into the temptation and started casually studying Japanese. Better orthography than Spanish *or* Korean! None of that agreement nonsense! Less rounded high back vowel!

To be honest, some of that obsession wasn't in the best taste. The end result of this, however, was the "Reform of Necarasso Cryssesa" document I wrote on 2014/10/28. According to the document, its goals were the following:

> 1. Enhance the aesthetics of the language by modifying syntax and increasing synthesis
> 2. Remove most of the Indo-European influence from the language
> 3. Streamline the orthography, making it more intuitive
> 4. Enhance borrowing

The last point highlighted a secondary weakness of the old Necarasso Cryssesa, which lacked certain common phonemes such as /b/ and /u/ and had allophonic vowel length rules that made it awkward to borrow words from languages with phonemic vowel length such as Japanese.

Of course, by "Indo-European", I meant "English and the Romance family"; I certainly didn't mean to purge anything that reminded me of – say – Indo-Iranian or Celtic. When this document was written, the model language for Necarasso Cryssesa was Japanese.

Eventually, the following changes managed to enter *NCS5* (2014/12/26):

* The romanization added three new letters: \<j\>, \<š\>, and \<č\>.
* Unvoiced consonants were explicitly specified to be aspirated. This is probably less pro-Japanese influence and more anti-Spanish influence, as Spanish doesn't aspirate any consonants.
* Most importantly, Necarasso Cryssesa was changed into an SOV/AN/Post language, as it is in Japanese.
* Nouns now had cases (nominative and accusative). In addition, *constructs*, which could optionally replace postpositional phrases with synthetic forms, were added.
* An explicit interrogative head particle was added.
* Gender (both natural and grammatical) was eradicated, to the extent that there was no way to convey gender at all (that is, no words for 'mother' or 'father', or even 'male' or 'female'!)
* Tense became morphologically marked, and there was only a past/nonpast distinction.
* The 'short forms' of numerals supplanted the previous 'long forms', and (optional) counters were added.
* A distinction was added between *nominal*, *verbal*, and *clausal* conjunctions.

*NCS6* (2016/2/19) was then a more refined form of NCS5.

### Downfall

> Fuck off uruwi.

*– in CDN's Discord server*

When I [published NCS6 in 2016], I had become increasingly unsatisfied with Necarasso Cryssesa, primarily for the following reasons:

* Necarasso Cryssesa was not evolved from a protolang, making it "completely unnaturalistic".
* it was too verbose. The situation somewhat improved with NCS5, but this failed to address the fact that the *roots themselves* were verbose. Many of them were three or more syllables long, and the fact that many vowels ended up long exacerbated the problem.

Initially, I planned for a seventh revision to the language to get the lexicon on a solid diachronic footing. That, however, failed to work out for obvious reasons[^4].

I left the language construction scene for a while until I started working on *Ḋraħýl Rase* in 2017. I see this language as a refinement of Necarasso Cryssesa, being head-final and light on redundancy, but with heavier, more agglutinative morphology. Later that year, I followed with *Varta Avina*, a relatively analytic language to contrast with Ḋraħýl Rase.

In September 2017, I started working on *Lek-Tsaro*, starting my experimental phase inspired by Isoraķatheð Zorethan's works. However, during that period, I started many projects that ended up in dead ends and became unsatisfied with what I produced. Perhaps I had made these languages so alien that I could not relate to them, or perhaps I was too busy chasing new ideas that I had not enough time to focus on any one language.

Eventually, I had the idea of revisiting the language of the forest, leading to a new chapter in my conlanging journey.

## @crîþjen ŋarên ŋarâþ crîþ

> The languages that come out of `mklang` can look distinctly unnaturalistic, but that's its charm; it's never really meant to produce a naturalistic language because I find them frustrating to make. Nevertheless, it's possible to make a naturalistic language using `mklang`, and in any case a "almost-but-not-quite-human" language is something that is largely unexplored in language creation, so it's a nice niche to fill.

*– Isoraķatheð, [description of mklang]*

*Ŋarâþ Crîþ* (*ŊCv7*), which began development on 2018/12/30, is semi-naturalistic artistic language. By 'semi-naturalistic', I mean the "almost-but-not-quite-human" quality that Isoraķatheð describes as "largely unexplored".

I don't like subscribing to naturalism completely for similar reasons as Isoraķatheð:

* Naturalistic conlangs have been done a lot. This is not a bad thing, but I don't really have an interest in staying in explored territory.
* In addition, truly making a conlang naturalistic requires evolving it from an ancestor, and none of my conlangs that were derived that way went very far.
* To me, conlanging is an escapist activity, so I would find complete naturalism unattractive. Isoraķatheð makes a related point that "it's easier to make facts up than keep up with real ones".
* Deviating from naturalism supports looking at language from a more mathematical perspective.

But for other reasons, abandoning naturalism entirely is also unattractive:

* It's harder to relate to a language that is too unnaturalistic.
* It's more tedious and error-prone to compose (or, usually, translate from English) texts in such a language, especially when inflection involves heavy number-crunching.

Thus Ŋarâþ Crîþ can be seen as a cross between human and machine.

In its world, Ŋarâþ Crîþ is situated in a different setting than its predecessor: many centuries after the *Venesos Gating Event* in 2561, in which a variety of demonic beasts called *corjam* (sg. *corim*) razed the land and caused civilization to collapse.

### Guiding principles

To make an aesthetically pleasing language was an important goal throughout the history of Ŋarâþ Crîþ. By "aesthetically pleasing", I mean that it should align with my personal tastes. This applies not only to phonaesthetics but also "grammaesthetics", the aesthetic quality of grmamatical features, and "graphaesthetics", the aesthetic quality of writing systems. What motivates my tastes for certain features over others?

In part, they are influenced by other languages: mainly Japanese, Welsh, and Irish, which I have studied at one point or another, as well as other languages such as Latin or Finnish. I must admit that most of my 'model languages' originate in Europe. And of course, Necarasso Cryssesa has an influence on Ŋarâþ Crîþ. While Ŋarâþ Crîþ might not be a systematic descendant of Necarasso Cryssesa, evolved through the diachronic method, it still acts as its spiritual successor, and therefore ought to be at least somewhat similar to it.

As for other conlangs, I have little interest in most of them to be honest. However, I have a lot of it in Isoraķatheð's conlangs, particularly Drsk, as they inspired many of my earlier conlangs. In fact, I adopted one of its features for Ŋarâþ Crîþ. Another language that has garnered my interest is Arka. In terms of its influence on my conlang, there isn't much obvious (aside from the all-lowercase romanization), but I think it made me think more about the lexicon of my conlangs.

My personal philosophy also influences the languages that I create. For instance, because I was skeptical of authority when I was younger and I started considering myself egalitarian, few of my conlangs have so much as a T-V distinction, let alone full-blown honorifics, and this is true for Ŋarâþ Crîþ as well[^5].

Related to the influence of personal philosophy is the desire to fix perceived flaws in the languages with which I am familiar. In fact, this has been a guiding principle since the earliest versions of Necarasso Cryssesa. Examples in English include the lack of a number distinction in second-person pronouns, the lack of a "proper" animate singular third-person pronoun of unspecified gender, and the lack of a good way to answer "A" to the question "not A?".

An underlying theme of Ŋarâþ Crîþ is *asymmetry*, a phenomenon closely related to "irregularity". A property of something in the language (e.g. words of a certain part of speech, or phonemes, or inflectional paradigms) is asymmetric when it applies to some members thereof, but not to other related members to which it should logically apply from the former. Here are some examples:

* *menat* 'to see' is semitransitive but *crešit* 'to hear' is transitive
* *anhit* 'to enjoy' has the experiencer in the dative case, but *vretat* 'to dislike' has the experiencer in the nominative case
* some color terms are nouns, others are verbs, and two colors have terms in both parts of speech

Another manifestation of asymmetry is the presence of seemingly redundant features. For instance, it seems redundant to have adpositions, serial verb constructions, and auxiliary verbs at once. One could create a language with only one of the above. The real fun comes when one mechanism is used for some purposes and another mechanism for others.

To me, asymmetry is what makes a language feel organic. Of course, my approach to it is probably far from naturalistic, and I do not mind that.

### Phonology

> if uru spoke swedish he'd pronounce ɧ as x͡θ

*– mareck*

The phonology of Ŋarâþ Crîþ is a continuation of that of Necarasso Cryssesa. Most phonemes in the latter are in the former, but Ŋarâþ Crîþ adds /ð/, /ŋ/ and /ʕ/ for consonants, and /ʃ/ and /tʃ/ (which were allophones of /s/ and /t/, respectively, in Necarasso Cryssesa) are now phonemes. A set of creaky-voiced vowels was added to its inventory, as well as /u̜/ for loanword use. There is no more length contrast in any vowels.

Instead of a small list of allowed word endings, Ŋarâþ Crîþ forms valid rimes from an optional \<j\> plus a vowel and an optional coda. The restrictions on rimes now applies to each syllable instead of only the last. That is, a syllable could end with /d/ (as long as it was not the final one) in Necarasso Cryssesa, but Ŋarâþ Crîþ forbids this.

Necarasso Cryssesa had /kv/ clusters, but Ŋarâþ Crîþ replaces them with /kf/ and allows a more complete set of stop+fricative clusters.

/x͡θ/ existed as an allophone of the cluster /kθ/ in Necarasso Cryssesa, and it also exists in Ŋarâþ Crîþ similarly. I decided to include this sound, which exists in no natural language, because I thought it was beautiful.

The combination of /n/ plus a velar consonant is not resolved in the archetypical way of backing /n/ to /ŋ/; instead, both consonants are assimilated to palatals. I wanted /ŋ/ to have a reason to exist; this is also the reason that /ŋ/ is permitted as an onset.

One characteristic feature of Ŋarâþ Crîþ phonology is the presence of consonant mutations, but this feature was absent from the earliest versions of the language. In fact, this feature was grafted onto the language on May 20. The set of mutations in this language was taken from Irish, which I was studying then.

Orthographically, Ŋarâþ Crîþ adds *markers*, which are considered letters by speakers but more closely resembles punctuation. Four of them mark various types of names, and one is reserved for foreign words.

### The noun

Ŋarâþ Crîþ has twelve cases. In general, I seem to gravitate toward large case systems, perhaps because I prefer conciseness. Perhaps the languages that I've learned to love or hate hold an influence over this as well: Japanese and Finnish have case declensions, while Spanish doesn't. The singular-dual-plural distinction is inherited from Necarasso Cryssesa; it was a good system that I decided to continue using.

Ŋarâþ Crîþ reintroduces gender, but I considered a sex-based system to be unacceptable. Instead, there is a semantic 'human' gender and two formal ones labeled 'celestial' and 'terrestrial'. Terrestrial nouns usually end in \<-os\>, \<-or\>, \<-om\>, \<-on\>, \<-oŋ\>, \<-or\>, or \<-el\>, while celestial nouns dominate the other endings[^6].

The most substantial change from Necarasso Cryssesa is that Ŋarâþ Crîþ is much richer in agreement. This change is used to support *hyperbaton*, in which modifiers move away from their heads such that they are separated by other words. I wanted to include this feature because Latin and Ancient Greek had it; after all, those two languages are an important part of Western culture.

In a way, the noun is the center of Ŋarâþ Crîþ morphology, as it has the most complex declension paradigm, taking over ten pages to describe. Moreover, certain forms of verbs and numerals also depend on properties of the noun, most frequently case.

#### Pronouns

Personal pronouns *in the strictest sense* lack any forms for the core cases (nominative, accusative, genitive, and dative), or rather, the forms that exist are identical to the emphatic forms but prefer not to manifest. Often, there are more synthetic ways to express something that would otherwise need core-case personal pronouns.

Of course, there are other types of pronouns, some of which overlap with the personal pronouns in the semantic space.

### The verb

Verbs inflect not only for person, number, and tense, but also for aspect (at least as long as eclipsis does anything). In contrast, they no longer inflect for mood.

There is no separate class of adjectives, since verbs take up most of their function. They can take on synthetic *relative clause forms* (or 'participles', if you want to use that term). Nor is there a separate class of adverbs – one must use adverbial cases of nouns, serial verb constructions, or auxiliary verbs.

The valency system of Ŋarâþ Crîþ has been refined further, yielding *intransitive* (no O or I), *semitransitive* (I but no O), *transitive* (O but no I), and *ditransitive* (both O and I) verbs, with the last category also containing *twisted* (secundative) verbs, giving the language a complex 'split-P' system. Whether a bivalent verb will take its second argument in the accusative or the dative case depends partly on its meaning, although there is also a "random" element to the question.

In any case, Ŋarâþ Crîþ tries to fit in common participants of an action into the core argument slots. For instance, the entry for *nelsit* is '(S) goes to (I)'. The destination is in the dative case, not the ablative case as someone familiar with English would predict. The expectation is that speakers would often specify the destination when using *nelsit*, so it is granted first-class status. Likewise, the entry for *navetat* is '(S) carves (D) into (I)', with the anticipation that the medium on which something is carved will often be specified. Even if not, there was a free argument slot lying around to put to good use. An unforeseen consequence of this decision is that the non-core cases are not used as often as one would expect. Indeed, it would not have done much damage to start with a smaller case inventory, but because the noun system was created before the verb system, it was already too late for verb valency to affect noun cases.

A notable feature of the language is its lack of negation, or rather, the lack of only *one* way to express negation. Instead, there are several verbs that have a "negative" meaning (*garit* 'refrain from', *pečit* 'avoid doing', *amðat* 'not yet'), plus suppletive forms for the most common verbs (*racrit* 'know' vs. *cerecit* 'not know', *neħrit* 'have the conscience to be able to' vs. *velrjotat* 'not ~'). This feature was inspired by Pkalho-Kölo's similar lack of negation.

### Other parts of speech

Ŋarâþ Crîþ has postpositions like its predecessor, although they are used less frequently because of its many cases. Like other parts of speech, postpositions interact extensively with case: each postposition governs a certain case. Postpositions governing the core cases have inflected forms.

As was the case since the beginning of its history, Ŋarâþ Crîþ has a base-16 system, probably because of my interest in programming. Interestingly, it inherits both the pre-NCS5 "long" numerals and the post-NCS5 "short" numerals. The "long numerals" now go up to only sixteen but mark for case. The "short numerals" have a far higher limit and do not mark for case, but they have obligatory counters in most circumstances. Neither type of numeral can completely replace the other.

Additionally, there are derivational numeral prefixes that work on a prime-factorization system.

### Syntax

In short, Ŋarâþ Crîþ's syntax feels superficially natural but uncanny on a deeper level, though perhaps the way it is described contributes to the latter feeling.

Overall, its syntax is mostly head-final like its predecessor, although by the time Ŋarâþ Crîþ was created, this was an arbitrary decision that was not motivated by a love or hate for any particular natlang.

As mentioned previously, Ŋarâþ Crîþ relies on agreement toward noun phrases more extensively than Necarasso Cryssesa, allowing for certain modifiers to become detached from their heads. Other less intense types of syntactic discontinuities occur in Ŋarâþ Crîþ: for instance, the infinitive particle of an infinitive clause can jump up to one word in advance:

**«a g\\an-asor varmen-at='pe inor-as mov-aþa p-en menvað-a-pe-þ» reþ racr-e.**  
INF.LOC (INF\\)sky-DAT.SG observe-INF=1SG void-LOC.SG float-REL.CEL.ACC,NOM what-NOM be_noticed_by-3SG-1SG-PAST QUOT.INDIRECT.ACC know-1SG  
*I know that when I looked at the sky, I noticed something floating in the void.*

Before this movement, the original phrase would have started with *anasor a varmenat'pe*, but the *a* jumps one word in advance in this case.

At the same time, there are situations when the particular word order is important:

**šin-o nem-an racr-a.**  
all-NOM.SG any-ACC.SG know-3SG  
For all *x*, there is a *y* such that *x* knows *y*.

**nem-an šin-o racr-a.**  
any-ACC.SG all-NOM.SG know-3SG  
There exists a *y* such that for all *x*, *x* knows *y*.

Arguably an intuitive feature, but one that complicates analysis. It would be difficult to devise a theory that would elegantly describe Ŋarâþ Crîþ while being compatible with this feature, especially since quantifiers can occur "deep down in the tree", such as in a postpositional phrase or in an embedded clause.

The syntax of Ŋarâþ Crîþ has so far defied any attempts at a formal analysis, although this might just arise from my total lack of experience in theoretical syntax.

### Lexicon

In contrast to Necarasso Cryssesa, the lexicon of Ŋarâþ Crîþ has been constructed more carefully. Ŋarâþ Crîþ's dictionary, in contrast to those of earlier languages, is role-markerist. That is, the correlation between the syntactic and semantic roles of a verb is explicitly listed in the definition. For instance, take this Ŋarâþ Crîþ entry:

**nelsit** *vs* (S) goes to (I)

In this definition, (S) refers to the nominative argument and (I) to the dative argument. That is, for *nelsit*, the agent is in the nominative case and the destination is in the dative case. Contrast this with the corresponding Necarasso Cryssesa entry:

**nelšyd** *vt* go to, attend

Also compare entries for similar words in two conlangs:

* Arka (by Seren Arbazard; partial entry): **ke** ［動詞］yulに行く、行く、いく。移動動詞。 "\[verb\]: go to (yul), go to. A movement verb."
* Mácuet (by Mareck): **dake** (TRA): go; move to(ward) a locus

Thus Ŋarâþ Crîþ's lexicon, as well as Arka's, is classified as "role-markerist", while Necarasso Cryssesa's and Mácuet's lexicons are classified as "non-role-markerist". Note that role-markerism is a property of a language's documentation, not of a language itself. For instance, one could theoretically create an Arka dictionary where the entry for *ke* included the definition "to go to", or a Mácuet dictionary where the entry for *dake* had the definition of "(S) goes to, moves to(ward) (O)".

It is interesting to see that there is a divide between these two types of conventions. I'd wager that the main argument for role-markerism is that it gives precise information for each verb in a concise manner. This point is especially important for a language such as Ŋarâþ Crîþ that tries to pack frequent arguments in the core cases. On the other hand, I'd imagine that some conlangers avoid this practice because it encourages the syntactic roles to be assigned haphazardly instead of taking the work to describe the general patterns and ensuring that new verbs adhere to them. In the case of Ŋarâþ Crîþ, however, I embrace this drawback, having pairs of verbs that are semantically related but syntactically dissimilar.

In theory, role-markerism could be extended to non-verb entries. In fact, Ŋarâþ Crîþ does so for a small number of nouns:

**ceran** *nc* key (strictly the traditional kind used to open a mechanical lock) for (ALL)

Of course, one could argue that non-role-markerist lexicons are actually role-markerist in a more implicit manner; for example, the entry *go to* implies that the direct object is the destination, and *be seen by* implies that the subject is an experiencer. However, while one could use the part-of-speech field to indicate "semitransitive" (dative-complement) verbs, it is not clear how one would extend this convention to ditransitive verbs.

But the above are my own thoughts. I'd like to hear what other people in the conlang community think about such a convention.

## Conclusion

Ŋarâþ Crîþ has become my primary conlang over the last two years, and part of it can perhaps be attributed to its long history in my life. It has marked another stage of my growth as a conlanger, marking the transition from being a rebel constantly in the chase for more alien ideas to a more cautious conlanger who pays attention to aesthetics and, to some extent, naturalism, while adopting unconventional ideas when I feel that they would fit into the language. In short, the purpose of Ŋarâþ Crîþ's existence is a place for my imagination to blossom. Now, however, it is time to look toward the future.

Where does Ŋarâþ Crîþ go from here? There will still be room for a bigger lexicon for longer than one can imagine, but in other areas, such as phonology or grammar, the language has already grown close to full capacity, at least barring a change that makes current texts obsolete. I feel that I've reached the point where I more often discover new grammatical rules than inventing them.

Or perhaps I haven't looked hard enough. Perhaps room for expansion in those respects is hiding right in front of my eyes. After all, for instance, the whole area of politeness and respect has hardly been touched. Even then, however, it seems that such expansions will become increasingly peripheral.

The other alternative is to move on from Ŋarâþ Crîþ and start working on a new conlang or one I've made earlier but shelved. Such a conlang would have to be worth working on in the long term instead of being a toy that I just throw away after a few weeks or months.

As for the existing conlangs, there are good reasons I abandoned most of them after a few months at most:

* I'm no longer attached to the conworld of the "Domains"; they were built for a game that never became fruitful because of its nature.
* Some conlangs were doomed to fail from their start because of their hyper-experimental nature.
* Those evolved from another language are tied to other languages, and working on them inevitably ends up in spamming words into a ztš script.

Thus, oddly enough, making Yet Another New Conlang™ looks like the best option if I stop working on Ŋarâþ Crîþ. What would this YANC look like? The short answer is that it would have similar goals as Ŋarâþ Crîþ yet achieve them in a different way.

In terms of phonology, it would need a sound (even as an allophone) that could beat \[x͡θ\]. Part of its allure is probably that its components are relatively common but the combination is nowhere to be found in natlangs; furthermore, \[x\] and \[θ\] do not frequently cluster cross-linguistically.

Ḋraħýl Rase has obstruent + lateral fricative clusters, which I'd imagine to be quite rare at the least. These could plausibly be smooshed into a coarticulated consonant, but what's the fun in building a glorified Ḋraħýl Rase phonology? Another option is an approximant of intermediate laterality, \[ɹ͡l\].

Aside from the "killer sound", I'd like to have some aspirated fricatives (contrasting phonemically with plain voiceless ones). I'm not sure if I'd like an aspiration distinction in stops, as I'm not a fan of tenuis voiceless stops. Phonotactically, I'm thinking of more complex syllable structures albeit not completely free-form as with some of my earlier conlangs.

Syntactically, any basic word order that tends to place (S) before (O) is fine[^7]. Even SVO is still the second-most common order, and I have no reason to avoid it just because my working language does too. Then head directionality and adposition directionality mostly falls from there. Of course, syntax consists of more than saying "haha SOV, AN, Post" (see [this list of questions] for starters). Recall that most of the interesting parts of Ŋarâþ Crîþ grammar comes from putting more structural burden on morphology, allowing syntactic discontinuities in cases where it would "make sense", but at this point in time, I'm hard-pressed to find another interesting niche.

Come to think of it, consider that marking arguments positionally is the spoken-language counterpart of calling a function in a programming language with the arguments passed positionally: compare "John loves Mary" with `memcpy(&foo_buffer, baz_info->foo1, sizeof(Foo))`. "Mary loves John" means something different than the previous, as does `memcpy(baz_info->foo1, &foo_buffer, sizeof(Foo))`. Marking arguments with cases is reminescent of calling a function with named arguments (which doesn't exist in C, but might look like `memcpy(.dest = &foo_buffer, .src = baz_info->foo1, .size = sizeof(Foo))` in a dream world). However, the way Ŋarâþ Crîþ does the latter corresponds to limiting yourself to three argument names for most of the time[^8]. Since using only three different parameter names sounds like a surefire way to unemployment, what if we imagine taking human language to the extreme that is the ideal way to name parameters? Perhaps there would be many "role names" (corresponding to cases), some of which are used for only a handful of verbs. Of course, this idea itself is alien enough to warrant sticking to mundane ideas for the rest of the syntax, and it's quite risky for a conlang that should last for years.

Even without that, however, there are plenty of syntactic quirks, even (especially?) from English, to draw inspiration from.

Of course, morphology is the other half of the grammatical puzzle, though I have even fewer ideas set in stone about what to do. It probably won't be as complex as Ḋraħýl Rase's. I want to at least avoid dedicating morphology for extremely situational things.

What about the setting of this hypothetical language? To look back on earlier languages, Necarasso Cryssesa didn't seem to know what kind of setting it was in. Ŋarâþ Crîþ at least has a standard of "Rennaisance-era technology"; of course, this makes translating anything such as "computer" awkward. If I make another conlang, I don't plan to have a setting for it, at least not at first.

Regardless of which path I take from here (or even both to some extent), Ŋarâþ Crîþ has had a long life so far, and it probably won't go away any time soon.

[^1]: Note that I also completed translations for VE²ENCS and VE³ENCS.
[^2]: That's the last two years of secondary education in the U.S. for those of you in foreign countries.
[^3]: In short, it went as follows: *oh, Black MIDI is cool!* → *oh, Touhou looks cool!* → *but I can't buy Touhou games* (remember: this was before I turned 18) → *guess I'll jump into the Japanese internet culture rabbit hole* → *ok I'm gonna learn Japanese*.
[^4]: The prophecy was eventually fulfilled, however: ŊCv7 was an even more substantial change than NCS5, just not in the way I anticipated.
[^5]: Necarasso Cryssesa (since NCS5) had an "honorific genitive", but this was abandoned in Ŋarâþ Crîþ.
[^6]: While this is the same boundary as the one for the old pre-NC5 gender system, I don't find a problem with this, as long as the labels are different. The presence of a human gender means that human beings don't have to be assigned to either of the formal genders.
[^7]: Those that are not, on the other hand, are not, not because they are unnaturalistic but because they are *superficially* unnaturalistic.
[^8]: Or rather, two, if you consider the "subject" to be an object that a method is called on à la Java and friends.

[language pack]: https://www.minecraftforum.net/forums/archive/language-packs/933120-1-6-wip-necarasso-cryssesa
[*Namárië*]: https://folk.uib.no/hnohf/namarie.htm
[published NCS6 in 2016]: https://www.reddit.com/r/conlangs/comments/46jikk/necarasso_cryssesa_6th_revision_of_the_grammar/
[description of mklang]: https://www.reddit.com/r/conlangs/comments/7de032/mklang_the_way_i_make_languages/
[this list of questions]: https://www.eva.mpg.de/lingua/tools-at-lingboard/questionnaire/linguaQ.php
