---
title: Enough with the fake 'conlangs'!
---

# Enough with the fake 'conlangs'!

**This article is still under construction.**

While movies have [started embracing the quest for authenticity in fictional languages](https://www.vox.com/culture/2019/4/2/18267808/pop-culture-conlangs-constructed-languages-dothraki-navi-klingon-elvish), many music producers who tout such languages have yet to accept that message. It is far too common to encounter a song featuring a language that is a relexification – or worse, a cypher or plain gibberish.

## How to find out if a 'conlang' is real

As usual, these are just general trends instead of hard-and-fast rules.

### Documentation

Perhaps the most straightforward way to check is to consult the documentation for the language – if it is available. Otherwise, you can try asking for documentation; if an actual conlang is used, then the creator of the video probably won't mind giving some to you. That is, given that they see your comment.

### Video title

If you see 架空言語 ('fictional language') in the video title, then that's a sign that you're encountering something other than a *bona fide* conlang. 人工言語 ('constructed language') is a better sign, though.

### Orthography

Using all of the ISO basic Latin alphabet (minus possibly one or two letters) is also a bad sign. Bonus points if the pronunciation of &lt;c&gt; or &lt;q&gt; follows the same rules as any Romance language, or if &lt;x&gt; is pronounced /ks/.

If you see random Cyrillic letters peppered into a Latin orthography, and especially if &lt;Я&gt; isn't pronounced as anything nearing /ja/, then *get the hell out!*

### Analysis

Finally, if some natlang translation is available, then you can try analysing the lyrics linguistically. Things to watch for:

* does a given sound or syllable in the X lyrics always occur with the same sound or syllable in the translation?
* in the case of a cover, do the X lyrics happen to fit perfectly with the metre?
* does a given morpheme or word in the X lyrics always occur with the same morpheme or word in the translation? Things to especially watch for: morphemes or words that sound the same in the natlang in question, but have different functions (e.g. Japanese に either as the postposition meaning 'in' or an adjective-to-adverb converter).
* Barring those, it's often difficult to further analyse the grammar or the lexicon, since most words will occur only once among the unique lines of the lyrics.

## List of recommended and unrecommended musicians

Visit [this page](https://gitlab.com/Kyarei/kyarei.gitlab.io/issues) to complain about this list.

### Recommended

| Channel | Notes |
|-|-|
| [+merlan \#flirora](https://www.youtube.com/channel/UCGKwZGiP57GepW6TbNxNzGA) | It's me, \#flirora! |
| [Калигула Армянская](https://www.youtube.com/channel/UCZlnXigCybdgJcE9mO-3f5Q) | Avoid `ｗａｔｃｈ？ｖ＝ｌｄＩＴｉＫｚＤｔｕＹ`. |
| <s><a href="https://www.youtube.com/user/shishix83">Ritchan Aios-Ciao</a></s> | Not very active in terms of music. |
| [炭酸ソーダ](https://www.youtube.com/channel/UCraVQnYfjZuJg10vYiivn7g) | Not very active in general. |
| [Herman Miller](https://www.youtube.com/user/tirelat) | \<3! |
| [Con WorkShop](https://www.youtube.com/channel/UCJMjntBcDJWrnwBXWot4HRQ) | Publishes multi-conlang covers of songs. Their site isn't very good, but the music is fine. |
| [John Quijada](https://www.youtube.com/channel/UCJ6BcuwscfmMl1pQp0gEX-g) | The one known for Ithkuil. |
| [Lunias Legitleins](https://www.youtube.com/channel/UCBr8itaoMI_U-CacY8D6Mmg) | At least I believe the conlang they use is *legit*imate. |
| [Luni Vanoneeme](https://www.youtube.com/user/kakiserlsax) | AKA Kakis Erl Sax. Very old videos of a very old (~2008) version of Arka. Hasn't uploaded anything for 8 years. |
| [leviantarka](https://www.youtube.com/user/leviantarka) | Official Arka channel. Won't be active again until at least 2025. |
| [minerva scientia](https://www.youtube.com/channel/UClsyp848PT_kJ0VSR0zvWFg) | Does some conlang stuff. |
| [藤原 安眞](https://www.youtube.com/user/Kadsuwo) | Not many videos. |
| [人工言語・柳霞の文化活動](https://www.youtube.com/channel/UCMXADn2og6AN6nNWmspZvDw) | Not the best quality voice- and video-wise but at least doesn't pass off sed output as conlangs. |
| [Ikku@人工言語制作者](https://www.youtube.com/channel/UCBqCm6t1YorXsMI9L5ilGWg) | Videos in Akyou; the language's orthography looks bad IMO but also legit. |
| [anotterkiro](https://www.youtube.com/channel/UCdHHAn8KuYHFu_G1Y3o84uA) | Only one conlang song so far, but promising. Has [another channel](https://www.youtube.com/channel/UCtzy-6udpuo19KMMqhUhNmA) dedicated to conlangs. |
| [Avlönskt](https://www.youtube.com/channel/UCOxag8oVb70iDjCarWhuzbw) | Has some covers in his conlang. |

### Unrecommended

The channel IDs are listed, converted to fullwidth characters. In case you want to view those channels, prefix `ｈｔｔｐｓ：／／ｗｗｗ．ｙｏｕｔｕｂｅ．ｃｏｍ／ｃｈａｎｎｅｌ／`.

* ＵＣＬｔＡ７ｌｇＨ０Ｊｕｋ１ｕＢａＱｒＸｓＱＤｇ (Cyphers.)
* ＵＣｓｋｊｗＸＵ２９Ｕｕ４ｌｙ＿７ｎ０ｑＫＲｖＱ (Relexes.)
* ＵＣｙＳＴｇＤｏＹＫＴ－ｔｄＣｂｘＭ－ｌＫｄｌｇ (Cyphers.)
* ＵＣｓ３０Ｇｓ７ＦｒｋＨＭＪＬＵＲＯＲｘ１９ＤＱ (Gibberish.)
