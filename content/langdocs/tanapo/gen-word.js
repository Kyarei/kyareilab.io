const loneC = ["p", "t", "k", "f", "s", "h", "m", "n", "l", "y", "w"];
const nLoneC = loneC.length;
const nGlideC = nLoneC - 2;
const lowV = ["a", "e", "i", "o", "u"];
const highV = ["á", "é", "í", "ó", "ú"];

function roll(n) {
  return Math.floor(Math.random() * n);
}

function prob(p) {
  return Math.random() < p;
}

function generateBody() {
  while (true) {
    let s = "";
    if (prob(0.1)) {
      s += loneC[roll(nGlideC)] + "y";
    } else if (prob(0.95)) {
      s += loneC[roll(nLoneC)];
    }
    if (prob(0.33)) {
      s += highV[roll(5)];
    } else {
      s += lowV[roll(5)];
    }
    if (!s.match(/y[ií]|w[uú]/)) return s;
  }
}

function generateSyllable() {
  let s = generateBody();
  switch (roll(15)) {
    case 0: s += "n"; break;
    case 1: s += "l"; break;
    case 2: s += "k"; break;
  }
  return s;
}

function resolveTone(match) {
  let s = match[0];
  let nextHigh = lowV.includes(s);
  for (let i = 1; i < match.length; ++i) {
    let k = lowV.indexOf(match[i]);
    if (k == -1) k = highV.indexOf(match[i]);
    s += (nextHigh ? highV : lowV)[k];
    nextHigh = !nextHigh;
  }
  return s;
}

function generateWord(len) {
  let s = "";
  for (let i = 0; i < len; ++i) {
    s += generateSyllable();
  }
  // Correct forbidden sequences:
  // duplicate consonants
  s = s.replace(/(n|l|k)\1/g, "$1");
  // nm
  s = s.replace(/nm/g, "m");
  // adjacent vowels of the same tone
  s = s.replace(/[aeiou]{2,}|[áéíóú]{2,}/, resolveTone);
  return s;
}
