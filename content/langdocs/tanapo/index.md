---
title: "A new language"
---

<script src="./gen-word.js"></script>

<script>

function displayWord() {
  let lengthInput = document.getElementById("wordLen");
  let wordOutput = document.getElementById("word");
  let len = lengthInput.valueAsNumber;
  wordOutput.textContent = generateWord(len);
}

</script>

# A new language

Tentatively called **tanapo**.

This conlang is not like others that I have created, and thus it is not numbered unlike the rest of my conlangs. It's still incomplete.

## Philosophy of tanapo

* Prefer simplicity
* Prefer unity
* but take practicality into account
* most importantly, be wholesome

Non-goals: not anti-goals but not goals either

* ease of learning
* minimalism
* experimenting with weird features
* avoiding ambiguity altogether
* coercing the language on people

## "Phonology"

tanapo has eleven "consonants":

**p t k f s h m n l y w**

and ten vowels:

**i e a o u í é á ó ú**

**i e a o u** have *low tone* while **í é á ó ú** have *high tone*.

(note that capital letters are not used in tanapo.)

a syllable has:

* an optional onset: either a single consonant or a glide consisting of a consonant other than **y** or **w** plus **y**
* a vowel
* an optional coda: one of **n, l, k**

other restrictions:

* two adjacent vowels (i.e. without an intervening consonant) must have different tones. (otherwise, the second vowel changes tone to fit this requirement)
* the same consonant cannot appear twice in a row (e.g. **\*akke** is invalid), and **n** can't occur before **m**. (otherwise, the first consonant is deleted)
* the sequences **\*yi, yí, wu, wú** are also invalid.
* syllabification occurs on a maximal-onset principle: **kikyalu** is syllabified as **ki.kya.lu**.

there is not yet any canonical pronunciation of tanapo, and I'm not sure if there will ever be.

## Outline of words

by syllable count:

| \# syllables | contents |
|-:|-|
| 1 | mostly function words, maybe a few very frequently used content words |
| 2 | most common content words, plus some function words |
| 3 | more content words, some more advanced function words |
| 4 | even more content words; some of these are words that you might expect to be shorter, but often, they're meant to be this long |
| 5+ | even more content words |

## Nouns

nouns are what most people expect them to be. however, they have no case, number or gender (for any sense thereof). **nafa** 'human' is such a word. nouns always end in vowels.

the personal pronouns are **na** 'I, we', **ti** 'you' (both singular and plural), **la** 'this thing or person', and **le** 'that thing or person'. they act exactly like regular nouns (can take modifiers, &c.)

by themselves, nouns aren't that interesting.

## Verbs

there are only a few verbs:

* **ke**: the copula ('to be'). can take either a noun, an adjective or an ideophone as a complement.
* **keke**, **kete** and **keté**: refinements of the above, meaning 'equals', 'is a member of' and 'is a subset of', respectively.
* **so**: 'to do'. can combine with nouns to form other actions.
* **tó**: 'to have'. also can combine with nouns to form other actions.
* **fo**: 'to go to'. no adposition needed!

in a sentence, the subject comes first, followed by the verb, then possibly the complement:

**na ke kikyalu.**  
I <sc>cop</sc> key  
*I am a key.*

in the case of verbs that can combine with nouns (such as **so**), the object of the action can be separated from the noun describing the action itself with **a**:

**na so káto (a) púlu.**  
I do eat (<sc>bleh</sc>) fish  
*I eat fish.*

some particles that can be placed before verbs:

* **hó**: interrogation: **ti hó tó lépile?** *do you have flowers?*
* **sal**: negation: **la sal ke talo** *this is not a bean*
* **ak**: past: **ti ak so kikyalu hyulepe** *you unlocked the door*
* **lá**: future: **na lá fo kénpi fa** *I will go to my house*

## Adjectives

adjectives are, like other parts of speech, not inflected at all. an adjective falls after the noun it modifies: **lépile húpu** *a red flower*.

adjectives can act as adverbs with no change.

an adjective can be preceded by the particle **nin** 'state of being': **nin húpu** *redness*.

there are possessive forms of the personal pronouns: **fa** 'my, our', **fi** 'your', **lya** 'this one's', **lye** 'that one's'.

the demonstrative adjectives are **wa** 'this' and **we** 'that'.

## Prepositions

you know what they are. prepositional phrases can modify either nouns or verbs.

the particle **sal** can also modify prepositions to negate them.

## Numerals

uses base 12:

| word | # (12) | # (10) |
|-|-:|-:|
| wi | 0 | 0 |
| pyú | 1 | 1 |
| no | 2 | 2 |
| ta | 3 | 3 |
| hu | 4 | 4 |
| kék | 5 | 5 |
| humá | 6 | 6 |
| nemú | 7 | 7 |
| pane | 8 | 8 |
| kyelo | 9 | 9 |
| yúko | A | 10 |
| leta | B | 11 |
| mofe | 10 | 12 |
| mofe pyú | 11 | 13 |
| mofe no | 12 | 14 |
| no mofe | 20 | 24 |
| no mofe pyú | 21 | 25 |
| nifawó | 100 | 144 |

## Ideophones

ideophones have high tone in the first syllable and low tone in all others. they can modify nouns or verbs, optionally with the particle **u** preceding it, as well as participate in compounding. an ideophone can be reduplicated; in that case, its first copy has the low tone in all syllables.

## Word formation

two nouns or an ideophone plus a noun can be compounded head-finally.

## Lexicon

<form id="wordGenerator">
  <button id="generateWord" type="button" onclick="displayWord()">Generate</button> a word of <input type="number" id="wordLen" maxlength="3"> syllables: <br>

  <b><span id="word"></span></b>
</form>

| Entry | PoS | Gloss |
|-|-|-|
| ak | partv | (past) |
| fa | adj | my, our |
| fi | adj | your |
| fo | v | to go to |
| hó | partv | (interrogative) |
| hu | num | 4 |
| humá | num | 6 |
| húpu | adj | red |
| hyulepe | n | door |
| kato | n | food |
| káto | n | eating, to eat |
| ke | v | to be |
| kék | num | 5 |
| keke | v | to equal |
| kénpi | n | house |
| kete | v | to be a member of |
| keté | v | to be a subset of |
| kikyalu | n | key, to unlock |
| kyelo | num | 9 |
| ko | n | thing, person; usually used with adjectives |
| la | n | this thing or person |
| lá | partv | (future) |
| le | n | that thing or person |
| lépile | n | flower, to bloom |
| leta | num | 11 |
| lya | adj | this one's |
| lye | adj | that one's |
| mofe | num | 12 |
| na | n | I, we |
| nemú | num | 7 |
| nifawó | num | 144 |
| no | num | 2 |
| nohú | n | knowledge, (with tó) to know |
| nuta | n | want, desire, (with tó) to want |
| pane | num | 8 |
| púlu | n | fish |
| pyú | num | 1 |
| sal | partv | not (negation) |
| se | n | someone, (in compounds) one who |
| so | v | to do |
| talo | n | bean, legume |
| tanapo | n | language, to speak |
| ta | num | 3 |
| ti | n | you |
| tó | v | to have |
| u | partid | (may be used before an ideophone to modify a noun or adjective) |
| wa | adj | this |
| we | adj | that (demonstrative) |
| wi | num | 0 |
| wokú | n | tool, to use |
| yúko | num | 10 |
