const REPLACEMENTS = {
  'ŋ': 'ng',
  'þ': 'th',
  'š': 'sh',
  'ł': 'lh',
  'č': 'ch',
  'ð': 'dh',
  'ħ': 'gh',
  'h': 'kh',
  'j': 'y',
  'c': 'k',
};

const LENITION = {
  'p': 'f',
  't': 'þ',
  'd': 'ð',
  'č': 'š',
  'c': '',
  'g': 'ħ',
  'm': 'v',
  'f': '',
  'v': '',
  'ð': '',
};

function normiefyWord(original, rinStack, ucFlag) {
  'use strict';
  let foundMarker = false;
  for (let i = rinStack.length - 1; i >= 0; --i) {
    if (rinStack[i][1]) {
      foundMarker = true;
      break;
    }
  }
  let hasEclipsis = original.match(/^(vp|mp|dt|gc|nd|ŋg|vf|ðþ|lł)/);
  let withoutEclipsis =
      hasEclipsis ? original[0] + original.slice(2) : original;
  let withoutLenitionDot =
      withoutEclipsis.replace(/[ptdčcgmfvð]·/g, (x) => LENITION[x[0]]);
  let newWord =
      withoutLenitionDot.split('').map((x) => REPLACEMENTS[x] || x).join('');
  if (foundMarker || ucFlag)
    newWord = newWord[0].toUpperCase() + newWord.slice(1);
  return newWord;
}

function normiefy(original, upper) {
  'use strict';
  const REGEX =
      /(\*?)(\+\*?|#|@)?((?:[A-Za-zªºÀ-ÖØ-öø-ʸˠ-ˤᴀ-ᴥᴬ-ᵜᵢ-ᵥᵫ-ᵷᵹ-ᶾḀ-ỿⁱⁿₐ-ₜKÅℲⅎⅠ-ↈⱠ-ⱿꜢ-ꞇꞋ-ꞹꟷ-ꟿꬰ-ꭚꭜ-ꭤﬀ-ﬆＡ-Ｚａ-ｚ]|·)+|{)/;
  let rinStack = [];
  let len = original.length;  // No Vocaloid jokes, okay?
  let out = '';
  let ucFlag = upper;
  for (let index = 0; index < len; ++index) {
    // Match (markers) '{' or (markers) 'word'
    let match = REGEX.exec(original.slice(index));
    if (match !== null && match.index == 0) {
      let nem = match[1];
      let marker = match[2];
      let word = match[3];
      rinStack.push([nem, marker]);
      if (word != '{') {
        out += normiefyWord(word, rinStack, ucFlag);
        rinStack.pop();
      }
      ucFlag = false;
      index += match[0].length - 1;
    } else {
      let c = original.charAt(index);
      if (c == '}') {
        rinStack.pop();
      } else {
        out += c;
      }
      if (c == '.' || c == '?' || c == '!') {
        ucFlag = true;
      }
    }
  }
  return out;
}

function setNormiefy() {
  'use strict';
  let normiefied = document.getElementById('normiefy').checked;
  let elementsToUpdate = document.getElementsByClassName('l4v7');
  if (normiefied) {
    for (let element of elementsToUpdate) {
      let upper = element.hasAttribute('upper');
      element.setAttribute('original', element.innerText);
      element.innerText = normiefy(element.innerText, upper);
    }
  } else {
    for (let element of elementsToUpdate) {
      element.innerText = element.getAttribute('original');
    }
  }
}

window.addEventListener('load', (_e) => {
  document.getElementById('normiefy').checked = false;
});
