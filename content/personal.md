---
title: "Personal projects"
---

# My personal projects

Things I do on my own. Also see [AGC's projects](https://nagakawa.gitlab.io/projects/).

## ztš

[ztš](https://gitlab.com/Kyarei/zt) was created before the great Modern Rymakonian do-over. It is a sound change applier for weird languages like mine, complete with Lua scripting.

## nmrqr

[nmrqr](https://gitlab.com/Kyarei/nmrqr), named after a randomly-selected five-letter string, is a Rust program to procedurally generate music by generating bad music and making random changes to make it better.

## halen 
[halen](https://gitlab.com/Kyarei/halen) is an esoteric programming language that I made after getting disappointed by the F/GO gacha. Its syntax consists of curse words and the names of Fate/Grand Order servants. Despite having five letters, *halen* was not selected randomly but rather named after the Welsh word for 'salt'.

Now also in a less vulgar `siwgr` flavour!

## mel to :mel:

[A Firefox extension](https://addons.mozilla.org/en-US/firefox/addon/mel-to-mel/) ([GitLab](https://gitlab.com/Kyarei/mel2melff)) that replaces every occurrence of 'mel' with a weary-faced bread emoji. Also see the older [Chrome version](https://github.com/bluebear94/mel2mel).

## kozet-gdb-stuff

[kozet-gdb-stuff](https://gitlab.com/Kyarei/kozet-gdb-stuff) is a collection of files to customise GDB. No longer will you have to type `set disassembly-flavor intel` every time you want to look at some assembly without wanting to rip your eyes out! (Oh yeah, and glm and kfp pretty-printers as well.)

## kraphyko

[kraphyko](https://gitlab.com/Kyarei/kraphyko) is an image editor for the TI-89. I wrote most of it a long, long time ago, but made some revisions relatively recently.

## bag

[bag](https://github.com/bluebear94/bag) is a very slow programming language. Time to relive the experience of programming graphing calculators!

## VS Code stuff

* [purple-night](https://marketplace.visualstudio.com/items?itemName=kozet.purple-night) ([GitLab](https://gitlab.com/Kyarei/vscode-purple-night)) – theme based on purple.
* [zt-vscode-syntax](https://marketplace.visualstudio.com/items?itemName=kozet.zt--vscode-syntax) ([GitLab](https://gitlab.com/Kyarei/zt-vscode-syntax)) – syntax highlighter for ztš scripts.

## Failures at bullet hell engines

* [ssf](https://github.com/bluebear94/ssf)
* [proj-youmu](https://github.com/bluebear94/proj-youmu)
