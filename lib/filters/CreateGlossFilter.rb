require 'set'
require "nokogumbo"

class CreateGlossFilter < Nanoc::Filter
  identifier :create_gloss

  def is_separator(c)
    return (c == '-' or c == '=' or c == '.' or c == '%')
  end

  def render_line_2(node, s)
    s.scan(/%[^-=.%\\]*|[^%]*/).each do |fragment|
      if fragment.start_with?("%")
        node.add_child(node.document.create_element("span", fragment[1..], :class => "smallcaps"))
      else
        node.add_child(node.document.create_text_node(fragment))
      end
    end
  end

  def run(content, params = {})
    if @item.path.include?("Mopso.html") # Fuck it
      return content
    end
    parsed_content = Nokogiri::HTML5.parse(content)
    parsed_content.css("ol.ilgloss").each do |raw_gloss|
      raw_lines_before = []
      raw_lines_after = []
      line1 = nil
      line2 = nil
      raw_gloss.children.each do |line|
        line_text = line.inner_text
        if line_text.start_with?("!")
          if line1 != nil
            throw "Only one \"!\" line allowed per gloss"
          end
          stripped = line_text[1..].strip
          line1 = stripped.split(" ")
        elsif line_text.start_with?("@")
          if line2 != nil
            throw "Only one \"@\" line allowed per gloss"
          end
          stripped = line_text[1..].strip
          line2 = stripped.split(" ")
        else
          line.name = "div"
          if line1 != nil
            line["class"] = "freetext-after"
            raw_lines_after.push(line)
          else
            line["class"] = "freetext-before"
            raw_lines_before.push(line)
          end
        end
      end
      if line1 == nil
        throw "You forgot the \"!\" line!"
      elsif line2 == nil
        throw "You forgot the \"@\" line!"
      end
      cooked_gloss = parsed_content.create_element "div",
          :class => "sentence"
      raw_lines_before.each {|line| cooked_gloss.add_child(line)}
      line1.zip(line2).each do |word_equiv|
        word_node = parsed_content.create_element "dl",
            :class => "word"
        word1_node = parsed_content.create_element "dt",
            word_equiv[0] || "",
            :class => "one"
        word2_node = parsed_content.create_element "dd",
            :class => "two" do |node|
          render_line_2(node, word_equiv[1] || "")
        end
        word_node.add_child(word1_node)
        word_node.add_child(word2_node)
        cooked_gloss.add_child(word_node)
      end
      raw_lines_after.each {|line| cooked_gloss.add_child(line)}
      raw_gloss.replace(cooked_gloss)
    end
    parsed_content.to_s
  end
end
