require 'set'
require "nokogumbo"

class RepositionFootnotesFilter < Nanoc::Filter
  identifier :reposition_footnotes

  @@stop_tags = Set["body", "p", "div"]

  def run(content, params = {})
    if @item.path.include?("Mopso.html") # Fuck it
      return content
    end
    parsed_content = Nokogiri::HTML5.parse(content)
    # A list of all footnotes. The 'id' attribute holds the ID.
    footnotes_by_id = {}
    parsed_content.css(".footnotes > ol > li").each {|element|
      id = element.attribute("id")
      element2 = element.clone
      element2.css("a.reversefootnote").each{|backlink|
        parent = backlink.parent
        backlink.remove()
        last_child = parent.children[-1]
        if last_child.text?
          if last_child.content.match?(/^[[:blank:]]*$/)
            last_child.remove()
          else
            last_child.content = last_child.content.rstrip
          end
        end
      }
      footnotes_by_id['#' + id.value] = element2
    }
    # Get the corresponding list of markers. The 'href' attribute is '#' plus the ID, hence why we prepended '#' earlier.
    markers = parsed_content.xpath("/html//a[@class='footnote']/..")
    markers.each {|marker|
      id = marker.children[0].attribute("href").to_s
      wrapped_footnote = footnotes_by_id[id].clone
      wrapped_footnote.name = "aside"
      wrapped_footnote.set_attribute("class", "sidenote")
      wrapped_footnote.set_attribute("role", "presentation")
      # Hiding from screen reader because it's redundant with the
      # "real" footnotes (and only one of the aside or the footnote
      # entry is meant to be shown at a time). Nothing nefarious here.
      wrapped_footnote.set_attribute("aria-hidden", "true")
      # Now find where to put the sidenote (hint: right before a block-level tag)
      elem = marker
      while elem.parent != nil && elem.parent.name != "body" && !@@stop_tags.include?(elem.name)
        elem = elem.parent
      end
      elem.before(wrapped_footnote)
    }
    parsed_content.to_s
  end
end