require 'json'

HTML_ESCAPES = {
  '&' => '&amp;',
  '<' => '&lt;',
  '>' => '&gt;',
  '"' => '&quot;',
  "'" => '&#039;',
  '#' => ' ',
}

module RenderGlossHelper
  def escape_html(text)
    return text.gsub(/[&<>"'#]/) {|c| HTML_ESCAPES[c]}
  end
  def get_classes_one(props)
    a = ['one']
    if props[:script] == "hacm"
      a.push("hacm")
    end
    return a.join(" ")
  end
  def is_separator(c)
    return (c == '-' or c == '=' or c == '.' or c == '%')
  end
  # This is a literal translation of the original JavaScript code, so it
  # doesn't look very nice...
  def render_line_two(s)
    out = ''
    i = 0
    while i < s.bytesize
      c = s.byteslice(i)
      if HTML_ESCAPES.has_key?(c)
        out += HTML_ESCAPES[c]
      elsif c == '%'
        i += 1
        j = i
        while j < s.bytesize and not is_separator(s.byteslice(j))
          j += 1
        end
        out += '<sc>'
        out += escape_html(s.byteslice(i, j - i))
        out += '</sc>'
        i = j - 1
      else
        out += c
      end
      i += 1
    end
    return out
  end
  def make_gloss_raw(raw, props)
    lines = raw.strip.split(/\n/)
    if lines.length % 2 != 0
      throw "make_gloss_raw: the number of lines should be even"
    end
    html = ''
    for i in (0 .. (lines.length / 2 - 1))
      html += '<div class="sentence">';
      words1 = lines[2 * i].split(/\s+/);
      words2 = lines[2 * i + 1].split(/\s+/);
      if words1.length != words2.length
        throw "make_gloss_raw: the number of words on lines should match: " +
          "line 1:\n" +
          lines[2 * i] + " (" + words1.length.to_s + " words)\n" +
          "line 2:\n" +
          lines[2 * i + 1] + " (" + words2.length.to_s + " words)\n"
      end
      words1.each_index {|i|
        html += '<div class="word">'
        html += '<div class="' + get_classes_one(props) + '">'
        html += escape_html(words1[i])
        html += '</div>'
        html += '<div class="two">'
        html += render_line_two(words2[i])
        html += '</div>'
        html += '</div>'
      }
      html += '</div>'
    end
    return html
  end
  def make_gloss(item, props)
    content = item.compiled_content
    return make_gloss_raw(content, props)
  end
end
