require 'json'

module RenderHrSitemapHelper
  def make_hr_sitemap()
    linked_items = []
    @items.each {|item|
      if item.path.end_with?(".html") || item.path.end_with?("/")
        linked_items.push(item)
      end
    }
    linked_items.sort_by! {|item| item.path}
    links = linked_items.map {|item|
      title = item[:title] || "(untitled)"
      if item == @item
        title = "Tá tú anseo"
      end
      row = Element.new("tr")
      cell1 = Element.new("td")
      link = Element.new("a")
      link.add_text(title)
      link.add_attribute("href", item.path)
      cell1.add(link)
      cell2 = Element.new("td")
      cell2.add(Text.new(item.path))
      row.add(cell1)
      row.add(cell2)
      row.to_s
    }
    return "<table><thead><tr><th>Item</th><th>Path</th></tr></thead>" + links.join("\n") + "</table>"
  end
end
