require 'json'

module RenderSidebarHelper
  def make_sidebar(item)
    content = item.compiled_content
    toc = JSON.parse(content)
    sections = toc["items"].map { |sec|
      header = '<h2>%s</h2>' % [sec["header"]]
      links = sec["links"].map { |link|
        '<li><a href="%s">%s</a></li>' % [link["url"], link["text"]]
      }
      "%s\n<ul>%s</ul>" % [header, links.join("\n")]
    }
    sections.join("\n")
  end
end
