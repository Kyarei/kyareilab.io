use_helper MyBreadcrumbHelper
require "rexml/document"
require "rexml/element"
include REXML

module RenderTrailHelper
  def render_breadcrumbs
    trail = breadcrumbs_trail
    links = trail.filter {|entry|
      entry != nil
    }.map {|entry|
      if entry == @item
        entry[:title]
      else
        elem = Element.new("a")
        elem.add_text(entry[:title])
        elem.add_attribute("href", entry.identifier.without_ext + ".html")
        elem.add_attribute("class", "back")
        elem.to_s
      end
    }
    return links.join(" » ")
  end
end